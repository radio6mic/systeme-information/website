# -*- coding: utf-8 -*-
"""
Django settings for website project.

For more information on this file, see
https://docs.djangoproject.com/fr/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/fr/2.0/ref/settings/
"""

import os
import datetime
import audiotools

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'
DEFAULT_FROM_EMAIL = 'gregory.david@ac-nantes.fr'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/fr/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '$*t-va+2fy*ehx@vp7eqvmn9+k1+#t*crb$0-c@eykie^37s_a'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'gestion_de_la_diffusion': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
        },
        'gestion_des_emissions': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
        },
        'gestion_des_musiques': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
        },
    },
}

ALLOWED_HOSTS = ['dev.radio6mic.net']


# Application definition

INSTALLED_APPS = [
    'gestion_de_la_diffusion.apps.GestionDeLaDiffusionConfig',
    'gestion_des_musiques.apps.GestionDesMusiquesConfig',
    'gestion_des_emissions.apps.GestionDesEmissionsConfig',
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'website.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'website.wsgi.application'


# Database
# https://docs.djangoproject.com/fr/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'radio6mic',
        'USER': 'radio6mic_dev',
        'HOST': 'postgresql.radio6mic.net',
        'PORT': '5432',
        'TEST': {
            'NAME': 'radio6mic-test',
            'TEMPLATE': 'radio6mic-test-template',
        },
    }
}


# Password validation
# https://docs.djangoproject.com/fr/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/fr/2.0/topics/i18n/

LANGUAGE_CODE = 'fr-fr'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/fr/2.0/howto/static-files/
STATIC_ROOT = '/radio/website-dev/static'
STATIC_URL = "/static/"

FIXTURE_DIRS = (os.path.join(BASE_DIR, "auth", "fixtures"),)

# Racine du stockage des fichiers
MEDIA_ROOT = "/radio"
MEDIA_URL = "/radio/"

# Répertoire de stockage pour les fichiers téléchargés
UPLOAD_DIR = "uploads"

# Répertoires intermédiaires définissant la base audio
# et la base image des `Diffusables`
AUDIO_DIR = "base-audio-diffusion"
IMAGE_DIR = "base_image"

# Configuration des paramètres généraux de conformation des fichiers audio
AUDIO_CONFIG = {
    "sample_rate": 44100,
    "bits_per_sample": 16,
    "channels": 2,
    "channel_mask": int(audiotools.ChannelMask.from_fields(front_left=True,
                                                           front_right=True)),
    "type": "mp3",
    }

# Configuration des paramètres généraux de conformation des fichiers image
IMAGE_CONFIG = {
    "format": "JPEG",
    "extension": "jpg",
    "mode": "RGB",
    "max_size": (1024, 1024),
    }


# Nom à utiliser dans le cadre de la production d'`Episode`
# Le cas général étant qu'un `Episode` est produit par l'organisation,
# par conséquent le `Medium` de `Provenance` de l'`Episode` est toujours
# le même et son nom est défini ici
PROVENANCE_INTERNE = "Production interne"
EDITEUR_INTERNE = "Lycée André Malraux"

# Nombre maximal de répétitions autorisé pour une `Programmation`
MAX_REPETITIONS = 15

# Lors de sa création, une `Programmation` ne peut pas débuter
# avant now() + DELAI_AVANT_DIFFUSION
DELAI_AVANT_DIFFUSION = datetime.timedelta(hours=12)

# Durée entre le 1 juillet et le 31 décembre
DUREE_JUILLET_DECEMBRE = datetime.timedelta(days=184)

# Format d'affichage des datetime ex : "Lu 06 avr. 2018 18:27:13"
DATETIME_FORMAT = "%a %d %b %Y %H:%M:%S"

# Durée nulle
DUREE_NULLE = datetime.timedelta()
