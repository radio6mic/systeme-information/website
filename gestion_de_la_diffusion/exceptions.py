# -*- coding: utf-8 -*-

"""
Définitions des exceptions de l'application.
"""


class LockedError(Exception):
    """
    Exception lancée lorsqu'on essaye de modifier ou de supprimer
    un objet verrouillé par une `Programmation`.
    message : message d'erreur
    objects : objet ou liste des objets verrouillés
    related_objects : programmations qui verrouillent le ou les objets
    """
    def __init__(self, message, objects=None, related_objects=None):
        super().__init__(message)
        self.message = message
        self.objects = objects
        self.related_objects = related_objects
