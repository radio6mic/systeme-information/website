# -*- coding: utf-8 -*-

"""
Module de test des modèles de la gestion de la diffusion.
"""

import datetime
import django.test
import tempfile
from django.utils import timezone
from django.db import transaction
from django.db.utils import IntegrityError
from django.db.models import (DateTimeField,
                              ExpressionWrapper,
                              F)
from django.db.models.deletion import ProtectedError
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from .models import (Diffusable,
                     Medium,
                     Provenance,
                     Licence,
                     Editeur,
                     Playlist,
                     Ordonner,
                     Frequence,
                     Programmation)
from gestion_des_emissions.models import (Emission,
                                          Episode)
from gestion_des_musiques.models import Piste
from website import settings
from .utils import Occurrence
from .exceptions import LockedError

# Nombre d'insertions et de suppressions d'objets à réaliser
# lors des tests 'test_objet_insertion_suppression'
NB_INSERTIONS = 5


class LicenceTest(django.test.TestCase):
    """
    Test des objets de type `Licence`.
    """
    fixtures = ["Licence"]
    prefixeTest = "LicenceTest" + next(tempfile._get_candidate_names())

    def test_Licence_doublon_creation(self):
        """
        S'assure que l'on ne peut pas ajouter une `Licence` avec
        des champs acronyme, nom ou texte déjà existants.
        """
        licence = Licence.objects.first()
        with transaction.atomic():
            with self.assertRaises(IntegrityError):
                Licence.objects.create(
                        acronyme=licence.acronyme,
                        nom="nom-{prefixe}"
                            .format(prefixe=self.prefixeTest),
                        texte="texte-{prefixe}"
                              .format(prefixe=self.prefixeTest))
        with transaction.atomic():
            with self.assertRaises(IntegrityError):
                Licence.objects.create(
                        acronyme="acronyme-{prefixe}"
                                 .format(prefixe=self.prefixeTest),
                        nom=licence.nom,
                        texte="texte-{prefixe}"
                              .format(prefixe=self.prefixeTest))

    def test_Licence_doublon_modification(self):
        """
        S'assure que l'on ne peut pas modifier une `Licence`
        avec un acronyme, nom ou texte de `Licence` existants.
        """
        licence = Licence.objects.first()
        licenceTest = Licence.objects.last()
        with transaction.atomic():
            with self.assertRaises(IntegrityError):
                licenceTest.acronyme = licence.acronyme
                licenceTest.save()
        with transaction.atomic():
            with self.assertRaises(IntegrityError):
                licenceTest.nom = licence.nom
                licenceTest.save()
        with transaction.atomic():
            with self.assertRaises(IntegrityError):
                licenceTest.texte = licence.texte
                licenceTest.save()

    def test_Licence_existence(self):
        """
        S'assure que les `Licence` fournies existent dans la base
        et sont des `Licence`.
        """
        licences = Licence.objects.all()
        for licenceTest in licences:
            with self.subTest(licence=licenceTest):
                self.assertIsInstance(
                        Licence.objects.get(acronyme=licenceTest.acronyme),
                        Licence)

    def test_Licence_inexistence(self):
        """
        S'assure qu'une exception est soulevée lorsqu'une
        `Licence` n'existe pas.
        """
        with self.assertRaises(Licence.DoesNotExist):
            Licence.objects.get(acronyme=self.prefixeTest)

    def test_Licence_insertion_suppression(self):
        """
        S'assure qu'insertion et suppression de `Licence` fonctionne.
        Réalise l'insertion de NB_INSERTIONS `Licence`.
        Réalise la suppression des NB_INSERTIONS `Licence` insérées.
        """
        nb_licences = Licence.objects.all().count()
        licences = [Licence(acronyme="acronyme-{nom}-{suffixe}"
                                     .format(nom=self.prefixeTest,
                                             suffixe=str(i)))
                    for i in range(NB_INSERTIONS)]
        for licenceTest in licences:
            with self.subTest(licence=licenceTest):
                licenceTest.save()
                licenceTest.refresh_from_db()
                self.assertIsNotNone(licenceTest.pk)
                licenceTest.delete()
        self.assertEqual(Licence.objects.all().count(),
                         nb_licences)


class DiffusableTest(django.test.TestCase):
    """
    Test des objets de type `Diffusable`.
    """
    fixtures = ["Medium",
                "Provenance",
                "Album",
                "Artiste",
                "Editeur",
                "Genre",
                "Licence",
                "Emission",
                "SourceExterne",
                "Diffusable",
                "ObjetSonore",
                "Piste",
                "Episode",
                "Playlist"]
    prefixeTest = "DiffusableTest" + next(tempfile._get_candidate_names())
    # `Diffusable` utilisant la licenceTest et l'editeurTest, de nature "piste"
    PisteTest_titre = "vapeur"
    # `Licence` utilisée pour le diffusablePisteTest et pour 6 `Diffusables`
    licenceTest_acronyme = "LAL 1.3"
    nb_diffusables_licenceTest = 6
    # `Licence` présente mais inutilisée dans les fixtures
    licence_inutile_acronyme = "CC BY-NC-ND"
    # `Editeur` utilisé pour le diffusablePisteTest et pour 6 `Diffusables`
    editeurTest_nom = "Association Groolot"
    nb_diffusables_editeurTest = 6
    editeur_inutile_nom = "Lycée Polyvalent Le Mans Sud"
    # `Diffusable` de nature "episode"
    episodeTest_titre = "Conseils aux STS1 pour PPE3"
    episodeTest_emission_nom = "Transmission"
    # `Diffusable` de nature "playlist"
    playlistTest_titre = "Matin tranquille"

    def setUp(self):
        """
        Definit et réinitialise à chaque test les licenceTest et
        diffusableTest utilisés dans chaque test.
        """
        self.licenceTest = Licence.objects.get(
                                    acronyme=self.licenceTest_acronyme)
        self.editeurTest = Editeur.objects.get(nom=self.editeurTest_nom)
        self.diffusableTest = Diffusable.objects.get(
                                    titre=self.PisteTest_titre)

    def test_Diffusable_existence(self):
        """
        S'assure que les `Diffusable` fournis existent dans
        la base et sont des `Diffusable`.
        """
        diffusables = Diffusable.objects.all()
        for diffusableTest in diffusables:
            with self.subTest(diffusable=diffusableTest):
                self.assertIsInstance(
                        Diffusable.objects
                        .filter(titre=diffusableTest.titre).first(),
                        Diffusable)

    def test_Diffusable_inexistence(self):
        """
        S'assure qu'une exception est soulevée
        lorsqu'un `Diffusable` n'existe pas.
        """
        with self.assertRaises(Diffusable.DoesNotExist):
            Diffusable.objects.get(titre=self.prefixeTest)

    def test_Diffusable_Licence_related(self):
        """
        S'assure que le `Diffusable` fait partie de l'ensemble des
        `Diffusable` de la `Licence` auquel il est associé.
        """
        self.assertEqual(self.licenceTest.diffusable_set.all().count(),
                         self.nb_diffusables_licenceTest)
        self.assertIn(self.diffusableTest,
                      self.licenceTest.diffusable_set.all())

    def test_Licence_suppression(self):
        """
        S'assure qu'on peut supprimer une `Licence` non utilisée,
        mais qu'on ne peut pas supprimer une `Licence` utilisée
        par un `Diffusable`.
        """
        nb_licences_avant = Licence.objects.all().count()
        nb_diffusables_avant = Diffusable.objects.all().count()
        # Supprime une `Licence` non utilisée
        Licence.objects.get(acronyme=self.licence_inutile_acronyme).delete()
        # Vérifie qu'on a bien 1 `Licence` de moins
        # et autant de `Diffusable` qu'au début
        nb_licences_apres = Licence.objects.all().count()
        self.assertEqual(nb_licences_avant,
                         nb_licences_apres + 1)
        nb_diffusables_apres = Diffusable.objects.all().count()
        self.assertEqual(nb_diffusables_avant,
                         nb_diffusables_apres)
        with self.assertRaises(Licence.DoesNotExist):
            Licence.objects.get(acronyme=self.licence_inutile_acronyme)
        # Supprime une `Licence` utilisée par des `Diffusable`
        with self.assertRaises(ProtectedError):
            Licence.objects.get(acronyme=self.licenceTest_acronyme).delete()
        # Vérifie qu'on a bien toujours 1 `Licence` de moins
        # et autant de `Diffusable` qu'au début
        nb_licences_apres = Licence.objects.all().count()
        self.assertEqual(nb_licences_avant,
                         nb_licences_apres + 1)
        nb_diffusables_apres = Diffusable.objects.all().count()
        self.assertEqual(nb_diffusables_avant,
                         nb_diffusables_apres)

    def test_Diffusable_Editeur_related(self):
        """
        S'assure que le `Diffusable` fait partie de l'ensemble des
        `Diffusable` de l'`Editeur` auquel il est associé.
        """
        self.assertEqual(self.editeurTest.diffusable_set.all().count(),
                         self.nb_diffusables_editeurTest)
        self.assertIn(self.diffusableTest,
                      self.editeurTest.diffusable_set.all())

    def test_Editeur_suppression(self):
        """
        S'assure qu'on peut supprimer un `Editeur` non utilisé,
        mais qu'on ne peut pas supprimer un `Editeur` utilisé
        par un `Diffusable`.
        """
        nb_editeurs_avant = Editeur.objects.all().count()
        nb_diffusables_avant = Diffusable.objects.all().count()
        # Supprime un `Editeur` non utilisé
        Editeur.objects.get(nom=self.editeur_inutile_nom).delete()
        # Vérifie qu'on a bien 1 `Editeur` de moins
        # et autant de `Diffusable` qu'au début
        nb_editeurs_apres = Editeur.objects.all().count()
        self.assertEqual(nb_editeurs_avant,
                         nb_editeurs_apres + 1)
        nb_diffusables_apres = Diffusable.objects.all().count()
        self.assertEqual(nb_diffusables_avant,
                         nb_diffusables_apres)
        with self.assertRaises(Editeur.DoesNotExist):
            Editeur.objects.get(pk=99999)
        # Supprime un `Editeur` utilisé par des `Diffusable`
        with self.assertRaises(ProtectedError):
            Editeur.objects.get(pk=1).delete()
        # Vérifie qu'on a toujours bien 1 `Editeur` de moins
        # et autant de `Diffusable` qu'au début
        nb_editeurs_apres = Editeur.objects.all().count()
        self.assertEqual(nb_editeurs_avant,
                         nb_editeurs_apres + 1)
        nb_diffusables_apres = Diffusable.objects.all().count()
        self.assertEqual(nb_diffusables_avant,
                         nb_diffusables_apres)

    def test_Diffusable_nature(self):
        """
        Test de la propriété nature de `Diffusable`.
        """
        # Teste la nature "piste"
        self.assertEqual(self.diffusableTest.nature,
                         "piste")
        # Teste la nature "épisode"
        emissionTest = Emission.objects.get(nom=self.episodeTest_emission_nom)
        episodeTest = Episode.objects.get(
                            titre=self.episodeTest_titre,
                            emission=emissionTest)
        diffusableTest = Diffusable.objects.get(pk=episodeTest.pk)
        self.assertEqual(diffusableTest.nature,
                         "épisode")
        # Teste la nature "liste de lecture"
        diffusableTest = Diffusable.objects.get(titre=self.playlistTest_titre)
        self.assertEqual(diffusableTest.nature,
                         "liste de lecture")
        # Teste la nature "inconnue"
        diffusableTest = Diffusable.objects.create(
                titre=self.prefixeTest,
                date_publication=datetime.date.today(),
                editeur=Editeur.objects.first())
        self.assertEqual(diffusableTest.nature,
                         "inconnue")


class MediumTest(django.test.TestCase):
    """
    Test des objets de type `Medium`.
    """
    fixtures = ["Medium"]
    prefixeTest = "MediumTest" + next(tempfile._get_candidate_names())

    def test_Medium_doublon(self):
        """
        S'assure que l'on ne peut pas ajouter un `Medium` déjà existant.
        """
        mediumTest = Medium.objects.first()
        with self.assertRaises(IntegrityError):
            Medium.objects.create(nom=mediumTest.nom)

    def test_Medium_existence(self):
        """
        S'assure que les `Medium` fournis existent dans
        la base et sont des `Medium`.
        """
        media = Medium.objects.all()
        for mediumTest in media:
            with self.subTest(medium=mediumTest):
                self.assertIsInstance(Medium.objects.get(nom=mediumTest.nom),
                                      Medium)

    def test_Medium_inexistence(self):
        """
        S'assure qu'une exception est soulevée
        lorsqu'un `Medium` n'existe pas.
        """
        with self.assertRaises(Medium.DoesNotExist):
            Medium.objects.get(nom=self.prefixeTest)

    def test_Medium_insertion_suppression(self):
        """
        S'assure qu'insertions et suppressions de `Medium` fonctionnent.
        Réalise l'insertion de NB_INSERTIONS `Medium`.
        Réalise la suppression des NB_INSERTIONS `Medium` insérés.
        """
        nb_media = Medium.objects.all().count()
        media = [Medium(nom="{nom}-{suffixe}"
                            .format(nom=self.prefixeTest,
                                    suffixe=str(i)))
                 for i in range(NB_INSERTIONS)]
        for medium in media:
            with self.subTest(medium=medium):
                medium.save()
                medium.refresh_from_db()
                self.assertIsNotNone(medium.pk)
        for medium in media:
            with self.subTest(medium=medium):
                medium.delete()
        self.assertEqual(Medium.objects.all().count(),
                         nb_media)


class ProvenanceTest(django.test.TestCase):
    """
    Test des objets de type `Provenance`.
    """
    fixtures = ["Medium",
                "Provenance"]
    prefixeTest = "ProvenanceTest" + next(tempfile._get_candidate_names())
    mediumInternet_nom = "Internet"
    mediumCD_nom = "CD audio"
    medium_inutile_nom = "TV"
    # `Provenance`  utilisant le mediumCD. Le `Medium` mediumCD est utilisé
    # dans 1 `Provenance`
    provenanceTest_description = "Aven CD"
    nb_provenances_mediumCD = 1

    @classmethod
    def setUpTestData(cls):
        """
        Ajoute le `Medium` PROVENANCE_INTERNE s'il n'existe pas.
        """
        # Si la PROVENANCE_INTERNE n'est pas dans la fixture
        # on l'ajoute
        medium = Medium.objects.filter(nom=settings.PROVENANCE_INTERNE)
        if not medium.exists():
            Medium.objects.create(nom=settings.PROVENANCE_INTERNE)

    def setUp(self):
        """
        Definit et réinitialise à chaque test les mediumInterne,
        mediumInternet, mediumCD utilisés dans les tests.
        """
        self.mediumInterne = Medium.objects.get(
                                    nom=settings.PROVENANCE_INTERNE)
        self.mediumInternet = Medium.objects.get(nom=self.mediumInternet_nom)
        self.mediumCD = Medium.objects.get(nom=self.mediumCD_nom)
        self.provenanceTest = Provenance.objects.get(
                                description=self.provenanceTest_description)

    def test_Provenance_str(self):
        """
        S'assure que la méthode str() de `Provenance` fonctionne comme voulu.
        """
        # `Provenance` avec description et avec url
        provenanceTest1 = Provenance.objects.get(pk=1)
        # `Provenance` sans description et avec url
        provenanceTest2 = Provenance.objects.get(pk=2)
        # `Provenance` avec description et sans url
        provenanceTest3 = Provenance.objects.get(pk=3)
        self.assertEqual(str(provenanceTest1),
                         provenanceTest1.description)
        self.assertEqual(str(provenanceTest2),
                         provenanceTest2.url)
        self.assertEqual(str(provenanceTest3),
                         provenanceTest3.description)

    def test_Provenance_doublon(self):
        """
        S'assure que l'on ne peut pas ajouter une `Provenance` déjà existante.
        """
        # Tests avec 3 `Provenance` avec ou sans description
        # et avec ou sans url
        for i in range(1, 4):
            provenanceTest = Provenance.objects.get(pk=i)
            with self.assertRaises(ValidationError):
                Provenance.objects.create(
                        description=provenanceTest.description,
                        url=provenanceTest.url,
                        medium=provenanceTest.medium)

    def test_Provenance_existence(self):
        """
        S'assure que les `Provenance` fournies existent dans
        la base et sont des `Provenance`.
        """
        provenances = Provenance.objects.all()
        for provenanceTest in provenances:
            self.assertIsInstance(
                    Provenance.objects.get(
                            description=provenanceTest.description,
                            url=provenanceTest.url),
                    Provenance)

    def test_Provenance_inexistence(self):
        """
        S'assure qu'une exception est soulevée lorsqu'une
        `Provenance` n'existe pas.
        """
        with self.assertRaisesMessage(
                Provenance.DoesNotExist,
                "Provenance matching query does not exist"):
            Provenance.objects.get(description=self.prefixeTest)

    def test_Provenance_insertion_suppression(self):
        """
        S'assure qu'insertions et suppressions de `Provenance` fonctionnent.
        Réalise l'insertion de NB_INSERTIONS `Provenance`.
        Réalise une suppression des NB_INSERTIONS `Provenance` insérées.
        """
        nb_provenances = Provenance.objects.all().count()
        media = Medium.objects.all()
        nb_media = media.count()
        provenances = [Provenance(description="description-{nom}-{suffixe}"
                                              .format(nom=self.prefixeTest,
                                                      suffixe=str(i)),
                                  url="url-{nom}-{suffixe}".format(
                                                      nom=self.prefixeTest,
                                                      suffixe=str(i)),
                                  medium=media[i % nb_media])
                       for i in range(NB_INSERTIONS)]
        for provenance in provenances:
            with self.subTest(provenance=provenance):
                provenance.save()
                provenance.refresh_from_db()
                self.assertIsNotNone(provenance.pk)
        for provenance in provenances:
            with self.subTest(provenance=provenance):
                provenance.delete()
        self.assertEqual(Provenance.objects.all().count(),
                         nb_provenances)

    def test_Provenance_CreationSansUrl(self):
        """
        S'assure qu'une PROVENANCE_INTERNE est toujours associée à une url.
        """
        with self.assertRaises(ValidationError):
            Provenance.objects.create(
                    description="Création sans url",
                    medium=self.mediumInterne)

    def test_Provenance_InternetSansUrl(self):
        """
        S'assure que le `Medium` Internet est
        toujours associée à une url.
        """
        with self.assertRaises(ValidationError):
            Provenance.objects.create(
                    description="Internet sans url",
                    medium=self.mediumInternet)

    def test_Provenance_SansDescriptionSansUrl(self):
        """
        S'assure qu'on ne peut pas enregistrer un `Medium`
        sans descrition ni url.
        """
        with self.assertRaises(ValidationError):
            Provenance.objects.create(
                    medium=Medium.objects.get(nom=self.mediumCD_nom))

    def test_Provenance_Medium_related(self):
        """
        S'assure que la `Provenance` fait partie de l'ensemble des `Provenance`
        du `Medium` auquel il est associé.
        """
        self.assertEqual(self.mediumCD.provenance_set.all().count(),
                         self.nb_provenances_mediumCD)
        self.assertIn(self.provenanceTest,
                      self.mediumCD.provenance_set.all())

    def test_Medium_suppression(self):
        """
        S'assure qu'on peut supprimer un `Medium` non utilisé,
        mais qu'on ne peut pas supprimer un `Medium` utilisé
        par une `Provenance`.
        """
        nb_media_avant = Medium.objects.all().count()
        nb_provenances_avant = Provenance.objects.all().count()
        # Supprime un `Medium` non utilisé
        Medium.objects.get(nom=self.medium_inutile_nom).delete()
        # Vérifie qu'on a bien 1 `Medium` de moins
        # et autant de `Provenance` qu'au début
        nb_media_apres = Medium.objects.all().count()
        self.assertEqual(nb_media_avant,
                         nb_media_apres + 1)
        nb_provenances_apres = Provenance.objects.all().count()
        self.assertEqual(nb_provenances_avant,
                         nb_provenances_apres)
        with self.assertRaises(Medium.DoesNotExist):
            Medium.objects.get(nom=self.medium_inutile_nom)
        # Supprime un `Medium` utilisé par des `Provenance`
        with self.assertRaises(ProtectedError):
            self.mediumInternet.delete()
        # Vérifie qu'on a bien 1 `Medium` de moins
        # et autant de `Provenance` qu'au début
        nb_media_apres = Medium.objects.all().count()
        self.assertEqual(nb_media_avant,
                         nb_media_apres + 1)
        nb_provenances_apres = Provenance.objects.all().count()
        self.assertEqual(nb_provenances_avant,
                         nb_provenances_apres)


class PlaylistTest(django.test.TestCase):
    """
    Test des objets de type `Playlist`.
    """
    fixtures = ["Playlist",
                "Diffusable",
                "ObjetSonore",
                "Editeur",
                "Licence",
                "Ordonner",
                "Episode",
                "Emission",
                "SourceExterne"]
    prefixeTest = "PlaylistTest" + next(tempfile._get_candidate_names())
    # "Conseils aux STS1 pour PPE3" fait partie seulement
    # de la `Playlist` "Soirée studieuse" qui contient 3 `Diffusable`
    episodeTest_titre = "Conseils aux STS1 pour PPE3"
    episodeTest_emission_nom = "Transmission"
    playlistTest_titre = "Soirée studieuse"
    nb_playlist_episodeTest = 1
    nb_diffusables_playlistTest = 3

    def test_Playlist_doublon_creation(self):
        """
        S'assure que l'on ne peut pas ajouter une `Playlist` déjà existante.
        """
        playlistTest1 = Playlist.objects.first()
        with self.assertRaises(ValidationError):
            Playlist.objects.create(
                    titre=playlistTest1.titre,
                    description=self.prefixeTest,
                    date_publication=datetime.date.today(),
                    editeur=Editeur.objects.first())

    def test_Playlist_doublon_modification(self):
        """
        S'assure que l'on ne peut pas modifier une `Playlist`
        avec un titre déjà existant.
        """
        playlistTest1 = Playlist.objects.first()
        playlistTest2 = Playlist.objects.last()
        playlistTest2.titre = playlistTest1.titre
        with self.assertRaises(ValidationError):
            playlistTest2.save()

    def test_Playlist_existence(self):
        """
        S'assure que les `Playlist` fournies existent dans
        la base et sont des `Playlist`.
        """
        playlists = Playlist.objects.all()
        for playlistTest in playlists:
            with self.subTest(playlist=playlistTest):
                self.assertIsInstance(
                        Playlist.objects.get(titre=playlistTest.titre),
                        Playlist)

    def test_Playlist_inexistence(self):
        """
        S'assure qu'une exception est soulevée
        lorsqu'une `Playlist` n'existe pas.
        """
        with self.assertRaises(Playlist.DoesNotExist):
            Playlist.objects.get(titre=self.prefixeTest)

    def test_Playlist_insertion_suppression(self):
        """
        S'assure qu'insertions et suppressions de `Playlist` fonctionnent.
        Réalise l'insertion de NB_INSERTIONS `Playlist`.
        Réalise une suppression des NB_INSERTIONS `Playlist` insérées.
        """
        licences = Licence.objects.all()
        nb_licences = licences.count()
        editeurs = Editeur.objects.all()
        nb_editeurs = editeurs.count()
        nb_playlists = Playlist.objects.all().count()
        playlists = [Playlist(titre="{nom}-{suffixe}"
                                    .format(nom=self.prefixeTest,
                                            suffixe=str(i)),
                              date_publication=datetime.date.today(),
                              licence=licences[i % nb_licences],
                              editeur=editeurs[i % nb_editeurs],
                              description=self.prefixeTest)
                     for i in range(NB_INSERTIONS)]
        for playlistTest in playlists:
            with self.subTest(playlist=playlistTest):
                playlistTest.save()
                playlistTest.refresh_from_db()
                self.assertIsNotNone(playlistTest.pk)
        for playlistTest in playlists:
            with self.subTest(playlist=playlistTest):
                playlistTest.delete()
        self.assertEqual(Playlist.objects.all().count(),
                         nb_playlists)

    def test_Playlist_Diffusable_related(self):
        """
        S'assure que la `Playlist` fait partie de l'ensemble des `Playlist`
        du `Diffusable` auquel il est associé.
        Et qu'un `Diffusable` fait bien partie de l'ensemble des `Diffusable`
        de la `Playlist` qui le contient.
        """
        emissionTest = Emission.objects.get(nom=self.episodeTest_emission_nom)
        episodeTest = Episode.objects.get(
                            titre=self.episodeTest_titre,
                            emission=emissionTest)
        diffusableTest = episodeTest.diffusable_ptr
        playlistTest = Playlist.objects.get(titre=self.playlistTest_titre)
        self.assertEqual(diffusableTest.playlist_set_liste.all().count(),
                         self.nb_playlist_episodeTest)
        self.assertIn(playlistTest,
                      diffusableTest.playlist_set_liste.all())
        self.assertEqual(playlistTest.liste.all().count(),
                         self.nb_diffusables_playlistTest)
        self.assertIn(diffusableTest,
                      playlistTest.liste.all())


class OrdonnerTest(django.test.TestCase):
    """
    Test des objets de type `Ordonner`.
    """
    fixtures = ["Playlist",
                "Diffusable",
                "ObjetSonore",
                "Editeur",
                "Licence",
                "Ordonner",
                "Episode",
                "Emission",
                "SourceExterne",
                "Medium",
                "Provenance",
                "Genre",
                "Artiste",
                "Album",
                "Piste"]
    prefixeTest = "OrdonnerTest" + next(tempfile._get_candidate_names())
    # La playlistTest1 contient 5 `Diffusable` dont la playlistTest2
    # La `Piste` "Sigmund Freud" est à la position 1 de playlistTest1 et
    # à la position 2 de playlistTest2
    playlistTest1_titre = "Matin tranquille"
    position_libre_playlistTest1 = 6
    pisteTest_titre = "Sigmund Freud"
    nb_ordonners_pisteTest = 2
    position_pisteTest_playlisTest1 = 1
    # La playlistTest2 contient 3 `Diffusable` dont la playlistTest3
    # en position 3
    playlistTest2_titre = "Soirée studieuse"
    nb_diffusables_playlistTest2 = 3
    position_playlistTest3_playlistTest2 = 3
    position_libre_playlistTest2 = 4
    position_pisteTest_playlistTest2 = 2
    # position occupée dans playlistTest2 par un autre
    # `Diffusable` que pisteTest
    position_occupee = 1
    # La playlistTest3 contient 1 `Diffusable`
    playlistTest3_titre = "Pour le plaisir"
    position_libre_playlistTest3 = 2

    @classmethod
    def setUpTestData(cls):
        """
        Définit les playlistTest1, playlistTest2 et playlistTest3
        utilisés dans les tests.
        """
        cls.playlistTest1 = Playlist.objects.get(titre=cls.playlistTest1_titre)
        cls.playlistTest2 = Playlist.objects.get(titre=cls.playlistTest2_titre)
        cls.playlistTest3 = Playlist.objects.get(titre=cls.playlistTest3_titre)

    def setUp(self):
        """
        Définit la pisteTest utilisés dans les tests.
        """
        self.pisteTest = Piste.objects.get(titre=self.pisteTest_titre)

    def test_Ordonner_str(self):
        """
        S'assure que la méthode str() de `Ordonner` fonctionne comme voulu.
        """
        self.assertEqual(str(Ordonner.objects.get(pk=1)),
                         "1 - Sigmund Freud dans la playlist Matin tranquille")

    def test_Ordonner_doublon_creation(self):
        """
        S'assure qu'on ne peut pas insérer un `Diffusable` dans une `Playlist`
        à la même position qu'un autre,
        mais qu'on peut bien l'insérer à une position non existante
        ou dans une autre `Playlist` à la même position.
        """
        # pisteTest est dans la `Playlist` playlistTest2
        # à la position position_pisteTest
        with transaction.atomic():
            with self.assertRaises(IntegrityError):
                # Insertion à la position occupée position_occupee
                Ordonner.objects.create(playlist=self.playlistTest2,
                                        diffusable=self.pisteTest,
                                        position=self.position_occupee)
        # Insertion à une position libre
        Ordonner.objects.create(playlist=self.playlistTest2,
                                diffusable=self.pisteTest,
                                position=self.position_libre_playlistTest2)
        # Une `Playlist` contient des `Diffusable` et non des `Episode`
        # Il faut donc faire référence à l'objet parent de la `Piste` pour la
        # recherche dans la `Playlist`
        self.assertIn(self.pisteTest.diffusable_ptr,
                      self.playlistTest2.liste.all())
        # Insertion à la position libre position_libre_playlistTest3
        # de la playlisteTest3
        Ordonner.objects.create(playlist=self.playlistTest3,
                                diffusable=self.pisteTest,
                                position=self.position_libre_playlistTest3)
        self.assertIn(self.pisteTest.diffusable_ptr,
                      self.playlistTest3.liste.all())

    def test_Ordonner_doublon_modification(self):
        """
        S'assure qu'on ne peut modifier la place d'un `Diffusable` dans une
        `Playlist` pour essayer de l'insérer à une position existante,
        mais qu'on peut bien l'insérer à une position non existante
        ou dans une autre `Playlist` à la même position.
        """
        # pisteTest est en position position_pisteTest dans la
        # `Playlist` playlistTest2
        ordonnerTest = Ordonner.objects.get(
                            playlist=self.playlistTest2,
                            diffusable=self.pisteTest,
                            position=self.position_pisteTest_playlistTest2)
        # Modification pour la position occupée position_occupee
        ordonnerTest.position = self.position_occupee
        with transaction.atomic():
            with self.assertRaises(IntegrityError):
                ordonnerTest.save()
        # Modification pour la position libre position_libre_playlistTest2
        ordonnerTest.position = self.position_libre_playlistTest2
        ordonnerTest.save()
        # Une `Playlist` contient des `Diffusable` et non des `Episode`
        # Il faut donc faire référence à l'objet parent de l'`Episode`
        # pour la recherche dans la `Playlist`
        self.assertIn(self.pisteTest.diffusable_ptr,
                      self.playlistTest2.liste.all())
        # Modification de la `Playlist` en playlistTest3
        # avec la position libre position_libre_playlistTest3
        ordonnerTest.playlist = self.playlistTest3
        ordonnerTest.position = self.position_libre_playlistTest3
        ordonnerTest.save()
        self.assertIn(self.pisteTest.diffusable_ptr,
                      self.playlistTest3.liste.all())

    def test_Playlist_Se_Contenant_Creation(self):
        """
        S'assure qu'une `Playlist` ne peut être insérée dans elle-même.
        playlistTest1 existe.
        On vérifie que playlistTest1 ne peut pas diffuser playlistTest1
        lors de la création d'un `Ordonner`.
        """
        with self.assertRaises(ValidationError):
            Ordonner.objects.create(playlist=self.playlistTest1,
                                    diffusable=self.playlistTest1,
                                    position=self.position_libre_playlistTest1)

    def test_Playlist_Se_Contenant_Modification(self):
        """
        S'assure qu'on ne peut pas modifier une `Playlist` contenue dans une
        `Playlist` de façon à ce qu'elle se contienne elle-même.
        playlistTest1 diffuse playlistTest2.
        On vérifie que playlistTest2 ne peut pas diffuser playlistTest2
        lors de la modification d'un `Ordonner`.
        """
        ordonnerTest = Ordonner.objects.get(
                            playlist=self.playlistTest2,
                            diffusable=self.playlistTest3,
                            position=self.position_playlistTest3_playlistTest2)
        ordonnerTest.diffusable = self.playlistTest2
        with self.assertRaises(ValidationError):
            ordonnerTest.save()

    def test_Playlist_Reference_Circulaire_Creation(self):
        """
        S'assure qu'on ne peut pas insérer une `Playlist` dans une autre
        si cela crée une référence circulaire.
        playlistTest1 diffuse playlistTest2,
        playlistTest2 diffuse playlistTest3.
        On vérifie que playlistTest3 ne peut pas diffuser
        playlistTest1 par création.
        """
        # Insère la playlistTest1 dans la playlistTest3
        with self.assertRaises(ValidationError):
            Ordonner.objects.create(playlist=self.playlistTest3,
                                    diffusable=self.playlistTest1,
                                    position=self.position_libre_playlistTest3)

    def test_Playlist_Reference_Circulaire_Modification(self):
        """
        S'assure qu'on ne peut pas modifier une `Playlist` contenue dans une
        `Playlist` de façon à ce que cela crée une référence circulaire.
        playlistTest1 diffuse playlistTest2,
        playlistTest2 diffuse playlistTest3.
        On vérifie que playlistTest2 ne peut pas diffuser playlistTest1.
        """
        ordonnerTest = Ordonner.objects.get(
                            playlist=self.playlistTest2,
                            diffusable=self.playlistTest3,
                            position=self.position_playlistTest3_playlistTest2)
        ordonnerTest.diffusable = self.playlistTest1
        with self.assertRaises(ValidationError):
            ordonnerTest.save()

    def test_Ordonner_insertion_suppression(self):
        """
        S'assure qu'insertions et suppressions de `Diffusable` (avec
        des `Episode`) dans une `Playlist` fonctionnent.
        Réalise l'insertion de NB_INSERTIONS `Episode` dans une `Playlist`.
        Réalise la suppression des NB_INSERTIONS `Episode` insérés
        dans la `Playlist`.
        Vérifie que les `Diffusable` existent toujours en temps que
        `Diffusable` ainsi que les `Episode`.
        """
        playlistTest = Playlist.objects.first()
        nb_playlists = playlistTest.liste.all().count()
        nb_diffusables_avant = Diffusable.objects.all().count()
        nb_episodes_avant = Episode.objects.all().count()
        emissionTest = Emission.objects.first()
        editeurTest = Editeur.objects.first()
        licenceTest = Licence.objects.first()
        episodes = [Episode(titre="{nom}-{suffixe}"
                                  .format(nom=self.prefixeTest,
                                          suffixe=str(i)),
                            date_publication=datetime.date.today(),
                            emission=emissionTest,
                            editeur=editeurTest,
                            licence=licenceTest)
                    for i in range(NB_INSERTIONS)]
        for i in range(NB_INSERTIONS):
            episodeTest = episodes[i]
            episodeTest.save()
            episodeTest.refresh_from_db()
            Ordonner.objects.create(playlist=playlistTest,
                                    diffusable=episodeTest,
                                    position=i + 100)
        self.assertEqual(playlistTest.liste.all().count(),
                         nb_playlists + NB_INSERTIONS)
        self.assertEqual(Diffusable.objects.all().count(),
                         nb_diffusables_avant + NB_INSERTIONS)
        self.assertEqual(Episode.objects.all().count(),
                         nb_episodes_avant + NB_INSERTIONS)
        for episodeTest in episodes:
            Ordonner.objects.filter(playlist=playlistTest,
                                    diffusable=episodeTest).delete()
        self.assertEqual(playlistTest.liste.all().count(),
                         nb_playlists)
        self.assertEqual(Diffusable.objects.all().count(),
                         nb_diffusables_avant + NB_INSERTIONS)
        self.assertEqual(Episode.objects.all().count(),
                         nb_episodes_avant + NB_INSERTIONS)

    def test_duree_Playlist_Ordonner_insertion(self):
        """
        Vérifie que la durée d'une `Playlist` est bien mise à jour
        lors de la création d'un `Ordonner`.
        """
        playlistTest = Playlist.objects.first()
        duree_playlist = playlistTest.duree
        episodeTest = Episode.objects.first()
        duree_episode = episodeTest.duree
        # Ajoute un `Episode` à la `Playlist`
        Ordonner.objects.create(playlist=playlistTest,
                                diffusable=episodeTest,
                                position=100)
        self.assertEqual(playlistTest.duree,
                         duree_playlist + duree_episode)

    def test_duree_Playlist_Ordonner_suppression(self):
        """
        Vérifie que la durée d'une `Playlist` est bien mise à jour
        lors de la suppression d'un ou plusieurs `Ordonner`.
        """
        playlistTest = Playlist.objects.first()
        duree_playlist = playlistTest.duree
        ordonners = Ordonner.objects.filter(playlist=playlistTest)
        # Supprime un `Ordonner` de la `Playlist`
        ordonnerTest = ordonners.first()
        duree_diffusable = ordonnerTest.diffusable.duree
        ordonnerTest.delete()
        self.assertEqual(playlistTest.duree,
                         duree_playlist - duree_diffusable)
        # Supprime tous les `Ordonner` de la `Playlist`
        ordonners.delete()
        self.assertEqual(playlistTest.duree,
                         settings.DUREE_NULLE)

    def test_Playlist_suppression(self):
        """
        S'assure que si on supprime une `Playlist`, tous les enregistrements
        de `Ordonner` concernant cette `Playlist` sont supprimés (en temps
        que `Playlist` et en temps que `Diffusable`).
        """
        # La `Playlist` playlistTest2 contient nb_diffusables_playlistTest2
        # `Diffusable` et fait partie de la `Playlist` playlistTest1
        nb_playlists_avant = Playlist.objects.all().count()
        nb_diffusables_avant = Diffusable.objects.all().count()
        nb_ordonners_avant = Ordonner.objects.all().count()
        self.playlistTest2.delete()
        # Vérification qu'il y a bien 1 `Playlist` de moins
        nb_playlists_apres = Playlist.objects.all().count()
        self.assertEqual(nb_playlists_avant,
                         nb_playlists_apres + 1)
        # Vérification qu'il y a bien 1 `Diffusable` de moins
        nb_diffusables_apres = Diffusable.objects.all().count()
        self.assertEqual(nb_diffusables_avant,
                         nb_diffusables_apres + 1)
        # Vérification qu'il y a bien nb_diffusables_playlistTest2 + 1
        # `Ordonner` de moins (nombre de `Diffusable` de la playlistTest2 +
        # la playlistTest2 faisant partie de la playlistTest1)
        nb_ordonners_apres = Ordonner.objects.all().count()
        self.assertEqual(
                nb_ordonners_avant,
                nb_ordonners_apres + self.nb_diffusables_playlistTest2 + 1)
        # Vérification qu'il n'y a plus de `Ordonner` faisant référence à la
        # playlistTest2
        recherche = Ordonner.objects.filter(
                                playlist__titre=self.playlistTest2_titre)
        # Recherche toutes les références en temps que `Playlist`
        self.assertEqual(recherche.count(),
                         0)
        recherche = Ordonner.objects.filter(
                                diffusable__titre=self.playlistTest2_titre)
        # Recherche toutes les références en temps que `Diffusable`
        self.assertEqual(recherche.count(),
                         0)

    def test_Diffusable_suppression(self):
        """
        S'assure que si on supprime un `Diffusable`, tous les enregistrements
        de `Ordonner` concernant ce `Diffusable` sont supprimés.
        """
        # La `Piste` pisteTest est dans 2 `Playlist`
        # playlistTest1 et playlistTest2
        duree_initiale1 = self.playlistTest1.duree
        duree_initiale2 = self.playlistTest2.duree
        duree_Piste = self.pisteTest.duree
        nb_diffusables_avant = Diffusable.objects.all().count()
        nb_ordonners_avant = Ordonner.objects.all().count()
        self.pisteTest.delete(keep_ObjetSonore=True)
        # Vérifie qu'on a bien un `Diffusable` de moins
        nb_diffusables_apres = Diffusable.objects.all().count()
        self.assertEqual(nb_diffusables_avant,
                         nb_diffusables_apres + 1)
        # Vérifie qu'on a bien nb_ordonners_pisteTest `Ordonner` de moins
        # (la pisteTest était dans nb_ordonners_pisteTest `Playlist`)
        nb_ordonners_apres = Ordonner.objects.all().count()
        self.assertEqual(nb_ordonners_avant,
                         nb_ordonners_apres + self.nb_ordonners_pisteTest)
        # Vérifie qu'il n'y a plus de référence à ce `Diffusable`
        recherche = Ordonner.objects.filter(
                                diffusable__titre=self.pisteTest_titre)
        self.assertEqual(recherche.count(),
                         0)
        # Vérification que la durée des `Playlist` a été mise à jour
        # Attention : pisteTest fait partie de playlistTest1 et de
        # playlistTest2 qui fait aussi partie de playlistTest1 :
        # pisteTest a donc été supprimé nb_ordonners_pisteTest fois
        self.assertEqual(
                self.playlistTest1.duree,
                duree_initiale1 - duree_Piste*self.nb_ordonners_pisteTest)
        self.assertEqual(self.playlistTest2.duree,
                         duree_initiale2 - duree_Piste)


class ProgrammationTest(django.test.TestCase):
    """
    Test des objets de type `Programmation`.
    """
    fixtures = ["Diffusable",
                "Editeur",
                "Licence",
                "Programmation",
                "Frequence",
                "Group",
                "User",
                "Playlist",
                "Ordonner"]

    @classmethod
    def setUpTestData(cls):
        """
        Définit le programmateurTest utilisé dans les tests.
        Modifie les dates des données de `Programmation` de façon à ce
        qu'elles soient adaptées à la date du test : il doit y avoir certaines
        dates dans le passé et d'autres dans le futur.
        instant0 est la date de référence des fixtures qui doit correspondre
        à l'instant des tests.
        (Dans les fixtures, instant0 est le 22 mars 2018 à 18h00:00.)
        """
        # programmateur utilisé dans tous les tests
        cls.programmateurTest = User.objects.get(pk=15)
        instant0 = Programmation.objects.get(pk=1).debut
        maintenant = timezone.now().replace(second=0, microsecond=0)
        duree_MaJ = maintenant - instant0
        Programmation.objects.all().update(
                debut=ExpressionWrapper(F("debut") + duree_MaJ,
                                        output_field=DateTimeField()))
        Programmation.objects.all().update(
                fin=ExpressionWrapper(F("fin") + duree_MaJ,
                                      output_field=DateTimeField()))

    def test_fixtures(self):
        """
        Vérifie que les fixtures ont bien étées mises à jour par setUpTestData.
        Teste si début de la `Programmation` (pk=3) est dans le passé
        et fin dans le futur.
        Teste si début et fin de la `Programmation` (pk=4) sont
        bien dans le passé.
        Teste si début et fin de la `Programmation` (pk=5) sont
        bien dans le futur.
        """
        maintenant = timezone.now()
        programmationTest = Programmation.objects.get(pk=3)
        self.assertLess(programmationTest.debut,
                        maintenant)
        self.assertGreater(programmationTest.fin,
                           maintenant)
        programmationTest = Programmation.objects.get(pk=4)
        self.assertLess(programmationTest.debut,
                        maintenant)
        self.assertLess(programmationTest.fin,
                        maintenant)
        programmationTest = Programmation.objects.get(pk=5)
        self.assertGreater(programmationTest.debut,
                           maintenant)
        self.assertGreater(programmationTest.fin,
                           maintenant)

    def test_Programmation_str(self):
        """
        S'assure que la méthode str() de `Programmation`
        fonctionne comme voulu.
        """
        programmationTest = Programmation.objects.get(pk=1)
        debut_attendu = programmationTest.debut.strftime(
                                            settings.DATETIME_FORMAT)
        fin_attendue = programmationTest.fin.strftime(
                                            settings.DATETIME_FORMAT)
        str_attendue = "vapeur programmé une seule fois du {debut} au {fin}" \
                       .format(debut=debut_attendu,
                               fin=fin_attendue)
        self.assertEqual(str(programmationTest),
                         str_attendue)

    def test_ProgrammationManager_occurrences_set(self):
        """
        Teste la méthode get_occurrence() de `ProgrammationManager`.
        Vérifie que debut et fin sont bien initialisés.
        Vérifie que toutes les `Occurrence` dans la plage sont bien renvoyées
        avec ou sans le paramètre pk_exclude.
        """
        maintenant = timezone.now()
        # Teste que, si debut=None, alors debut est
        # bien initialisé à maintenant
        occ_setTest = Programmation.objects.occurrences_set(
                                fin=maintenant + datetime.timedelta(hours=1))
        nb_attendu = len(Programmation.objects.occurrences_set(
                            debut=maintenant,
                            fin=maintenant + datetime.timedelta(hours=1)))
        self.assertEqual(len(occ_setTest),
                         nb_attendu)
        # Teste les 4 `Occurence`
        # Première `Occurence`
        programmationTest = Programmation.objects.get(pk=1)
        debut = programmationTest.debut
        duree = programmationTest.diffusable.duree
        occTest = Occurrence(debut, debut + duree)
        self.assertIn(occTest,
                      occ_setTest)
        # Deuxième `Occurence`
        programmationTest = Programmation.objects.get(pk=3)
        debut = programmationTest.debut + 3*programmationTest.frequence.delta
        duree = programmationTest.diffusable.duree
        occTest = Occurrence(debut, debut + duree)
        self.assertIn(occTest,
                      occ_setTest)
        # Troisième `Occurence`
        programmationTest = Programmation.objects.get(pk=2)
        debut = programmationTest.debut + 4*programmationTest.frequence.delta
        duree = programmationTest.diffusable.duree
        occTest = Occurrence(debut, debut + duree)
        self.assertIn(occTest,
                      occ_setTest)
        # Quatrième `Occurence`
        programmationTest = Programmation.objects.get(pk=8)
        debut = programmationTest.debut + 3*programmationTest.frequence.delta
        duree = programmationTest.diffusable.duree
        occTest = Occurrence(debut, debut + duree)
        self.assertIn(occTest,
                      occ_setTest)
        # Teste que, si fin=None alors fin est bien initialisé
        # au 31/08 de la fin de l'année scolaire
        occ_setTest = Programmation.objects.occurrences_set(
                            debut=maintenant + datetime.timedelta(days=4))
        annee_fin = (maintenant + settings.DUREE_JUILLET_DECEMBRE).year
        datetime_fin = timezone.make_aware(datetime.datetime(day=1,
                                                             month=9,
                                                             year=annee_fin))
        nb_attendu = len(Programmation.objects.occurrences_set(
                            debut=maintenant + datetime.timedelta(days=4),
                            fin=datetime_fin))
        self.assertEqual(len(occ_setTest),
                         nb_attendu)
        # Teste que toutes `Occurence` d'une `Programmation` sont bien
        # renvoyées avec exclude=None (22 `Occurence`)
        occ_setTest = Programmation.objects.occurrences_set(
                            debut=maintenant - datetime.timedelta(hours=3),
                            fin=maintenant + datetime.timedelta(days=1,
                                                                minutes=10))
        nb_attendu = 22
        self.assertEqual(len(occ_setTest),
                         nb_attendu)
        # Teste que toutes `Occurence` d'une `Programmation` sont bien
        # renvoyées avec exclude=une valeur non valide (22 `Occurence`)
        occ_setTest = Programmation.objects.occurrences_set(
                            debut=maintenant - datetime.timedelta(hours=3),
                            fin=maintenant + datetime.timedelta(days=1,
                                                                minutes=10),
                            pk_exclude=999)
        self.assertEqual(len(occ_setTest),
                         nb_attendu)
        # Teste que toutes `Occurence` d'une `Programmation` sont bien
        # renvoyées avec exclude=3 (10 `Occurence`)
        occ_setTest = Programmation.objects.occurrences_set(
                            debut=maintenant - datetime.timedelta(hours=3),
                            fin=maintenant + datetime.timedelta(days=1,
                                                                minutes=10),
                            pk_exclude=3)
        nb_attendu = 10
        self.assertEqual(len(occ_setTest),
                         nb_attendu)
        # Teste que s'il n'y a pas d'`Occurence`, une liste vide est renvoyée
        occ_setTest = Programmation.objects.occurrences_set(
                            debut=maintenant + datetime.timedelta(days=1,
                                                                  hours=1),
                            fin=maintenant + datetime.timedelta(days=1,
                                                                hours=1,
                                                                minutes=30))
        nb_attendu = 0
        self.assertEqual(len(occ_setTest),
                         nb_attendu)

    def test_Programmation_get_occurrences(self):
        """
        Teste la fonction get_occurrence de `Programmation`.
        La `Programmation` (pk=1) a une seule `Occurence`.
        La `Programmation` (pk=2) a 3 `Occurence`
        (`Fréquence` : tous les jours).
        La `Programmation` (pk=3) a 11 `Occurence`
        (`Fréquence` : toutes les heures).
        """
        # Première `Programmation`
        programmationTest = Programmation.objects.get(pk=1)
        duree = Diffusable.objects.get(pk=1).duree
        occTest = Occurrence(programmationTest.debut,
                             programmationTest.debut + duree)
        occurrences = programmationTest.get_occurrences()
        nb_occs_attendu = 1
        # Vérifie la première `Occurence` et le nombre d'`Occurence`
        self.assertEqual(occurrences[0],
                         occTest)
        self.assertEqual(len(occurrences),
                         nb_occs_attendu)
        # Deuxième `Programmation`
        programmationTest = Programmation.objects.get(pk=2)
        duree = Diffusable.objects.get(pk=2).duree
        frequence = Frequence.objects.get(pk=4).delta
        occurrences = programmationTest.get_occurrences()
        occTest1 = Occurrence(programmationTest.debut,
                              programmationTest.debut + duree)
        occTest2 = Occurrence(programmationTest.debut + frequence,
                              programmationTest.debut + frequence + duree)
        nb_occs_attendu = 9
        # Vérifie les deux premières `Occurence` et le nombre d'`Occurence`
        self.assertEqual(occurrences[0],
                         occTest1)
        self.assertEqual(occurrences[1],
                         occTest2)
        self.assertEqual(len(occurrences),
                         nb_occs_attendu)
        # Troisième `Programmation`
        programmationTest = Programmation.objects.get(pk=3)
        duree = Diffusable.objects.get(pk=3).duree
        frequence = Frequence.objects.get(pk=1).delta
        occTest1 = Occurrence(programmationTest.debut,
                              programmationTest.debut + duree)
        occTest2 = Occurrence(programmationTest.debut + frequence,
                              programmationTest.debut + frequence + duree)
        occurrences = programmationTest.get_occurrences()
        nb_occs_attendu = 12
        # Vérifie les deux premières `Occurence` et le nombre d'`Occurence`
        self.assertEqual(occurrences[0],
                         occTest1)
        self.assertEqual(occurrences[1],
                         occTest2)
        self.assertEqual(len(occurrences),
                         nb_occs_attendu)

    def test_Programmation_fin_reelle(self):
        """
        Vérifie que la fonction fin_reelle de `Programmation`
        fonctionne correctement.
        """
        # `Programmation` de `Frequence` "une seule fois"
        # fin_attendue correspond à la fin de la dernière `Occurence`
        programmationTest = Programmation.objects.get(pk=7)
        duree_diffusable = programmationTest.diffusable.duree
        fin_attendue = programmationTest.fin + duree_diffusable
        # modification de fin de 1 heure
        # Rien ne doit changer pour la fin_attendue
        programmationTest.fin += datetime.timedelta(hours=1)
        self.assertEqual(programmationTest.fin_reelle(),
                         fin_attendue)
        # `Programmation` de `Frequence` "tous les jours"
        programmationTest = Programmation.objects.get(pk=2)
        duree_diffusable = programmationTest.diffusable.duree
        fin_attendue = programmationTest.fin + duree_diffusable
        self.assertEqual(programmationTest.fin_reelle(),
                         fin_attendue)
        # modification de fin de 12 heures
        # Rien ne doit changer pour la fin_attendue
        programmationTest.fin += datetime.timedelta(hours=12)
        self.assertEqual(programmationTest.fin_reelle(),
                         fin_attendue)

    def test_Programmation_debut_passe_creation(self):
        """
        S'assure que le début d'une nouvelle `Programmation`
        ne peut pas être dans le passé.
        """
        maintenant = timezone.now()
        message = "Une programmation ne peut pas être créée avant le"
        with self.assertRaisesMessage(ValidationError, message):
            Programmation.objects.create(
                    debut=maintenant - datetime.timedelta(hours=1),
                    fin=maintenant + datetime.timedelta(hours=1),
                    frequence=Frequence.objects.get(pk=2),
                    diffusable=Diffusable.objects.get(pk=1),
                    programmateur=self.programmateurTest)

    def test_Programmation_debut_passe_modification(self):
        """
        S'assure que le début d'une `Programmation`
        ne peut pas être modifiée dans le passé.
        """
        # `Programmation` dont debut est dans le passé
        programmationTest = Programmation.objects.get(pk=2)
        programmationTest.debut += datetime.timedelta(hours=1)
        message = "Impossible de modifier le début " \
                  "d'une programmation commençant avant le"
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()
        # `Programmation` dont debut est dans le futur
        programmationTest = Programmation.objects.get(pk=5)
        programmationTest.debut -= datetime.timedelta(days=1)
        message = "La date limite pour modifier le début " \
                  "d'une programmation est"
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()

    def test_Programmation_fin_modification(self):
        """
        S'assure que les règles de modification de la fin
        d'une `Programmation` sont respectées :
         - Si la fin intiale était dans le passé,
           on ne peut pas la modifier;
         - Si la fin initiale était dans le futur,
           on ne peut pas la modifier dans le passé.
        """
        maintenant = timezone.now()
        # Modification de la fin d'une `Programmation`
        # avec debut dans le passé et fin dans le futur
        programmationTest = Programmation.objects.get(pk=2)
        # Modification de la fin dans le futur
        programmationTest.fin -= datetime.timedelta(days=1)
        programmationTest.save()
        # Modification de la fin dans le passé
        programmationTest.fin = maintenant - datetime.timedelta(hours=1)
        message = "La date limite pour modifier la fin " \
                  "d'une programmation déja commencée est"
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()
        # Modification de la fin d'une `Programmation`
        # avec debut dans le passé et fin dans le passé
        programmationTest = Programmation.objects.get(pk=6)
        # Modification de la fin dans le futur
        programmationTest.fin += datetime.timedelta(hours=2)
        message = "Impossible de modifier la fin " \
                  "d'une programmation terminant avant le"
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()
        # Modification de la fin dans le passé
        programmationTest.fin = maintenant + datetime.timedelta(days=1)
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()

    def test_Programmation_modification(self):
        """
        S'assure qu'on ne peut pas modifier le `Diffusable`, la `Frequence`,
        le programmateur d'une `Programmation` débutant dans le passé
        mais que c'est possible si le début est dans le futur.
        """
        # `Programmation` avec debut dans le passe
        # (rien ne doit être modifiable)
        programmationTest = Programmation.objects.get(pk=2)
        diffusable = programmationTest.diffusable
        frequence = programmationTest.frequence
        # Modification du `Diffusable`
        message = "Impossible de modifier le diffusable " \
                  "d'une programmation commençant avant le"
        programmationTest.diffusable = Diffusable.objects.get(pk=1)
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()
        programmationTest.diffusable = diffusable
        # Modification de la `Frequence`
        message = "Impossible de modifier la fréquence " \
                  "d'une programmation commençant avant le"
        programmationTest.frequence = Frequence.objects.get(pk=2)
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()
        programmationTest.frequence = frequence
        # Modification du programmateur
        message = "Impossible de modifier le programmateur " \
                  "d'une programmation commençant avant le"
        programmationTest.programmateur = User.objects.get(pk=1)
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()
        # `Programmation` avec debut dans le futur
        # (tout doit être modifiable)
        programmationTest = Programmation.objects.get(pk=5)
        # Modification du `Diffusable`
        programmationTest.diffusable = Diffusable.objects.get(pk=1)
        programmationTest.save()
        # Modification de la `Frequence`
        programmationTest.frequence = Frequence.objects.get(pk=4)
        programmationTest.save()
        # Modification du programmateur
        programmationTest.programmateur = User.objects.get(pk=1)
        programmationTest.save()

    def test_Programmation_debut_apres_fin_creation(self):
        """
        S'assure que le début d'une `Programmation` ne peut
        pas être postérieur à sa fin lors d'une création de `Programmation`.
        """
        maintenant = timezone.now()
        message = "La fin de la programmation ne peut pas être " \
                  "antérieure à son début."
        with self.assertRaisesMessage(ValidationError, message):
            Programmation.objects.create(
                    debut=maintenant + datetime.timedelta(days=2, hours=2),
                    fin=maintenant + datetime.timedelta(days=2, hours=1),
                    frequence=Frequence.objects.get(pk=2),
                    diffusable=Diffusable.objects.get(pk=1),
                    programmateur=self.programmateurTest)

    def test_Programmation_debut_apres_fin_modification(self):
        """
        S'assure que le début d'une `Programmation` ne peut
        pas être postérieur à sa fin lors d'une modification de `Programmation`
        """
        programmationTest = Programmation.objects.get(pk=7)
        debut = programmationTest.debut
        programmationTest.fin = debut - datetime.timedelta(hours=1)
        message = "La fin de la programmation ne peut pas être " \
                  "antérieure à son début."
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()

    def test_Programmation_fin_apres_fin_annee_scolaire_creation(self):
        """
        S'assure que la fin d'une `Programmation` ne peut
        pas être postérieure à la fin de l'année scolaire
        lors de la création d'une `Programmation`.
        """
        maintenant = timezone.now()
        annee_fin = (maintenant + settings.DUREE_JUILLET_DECEMBRE).year
        # On se place au 1 août de l'année scolaire
        debut_prog = timezone.make_aware(datetime.datetime(day=1,
                                                           month=8,
                                                           year=annee_fin))
        # Si on fait le test entre le 1 juillet et le 31 aout
        # On se place au 1 août de l'année suivante
        if maintenant > debut_prog:
            debut_prog = timezone.make_aware(
                                datetime.datetime(day=1,
                                                  month=8,
                                                  year=annee_fin + 1))
        message = "La fin de la programmation ne peut pas être " \
                  "postérieure à la fin de l'année scolaire,"
        # On crée une `Programmation` toutes les semaines sur 40 jours
        # (dépasse donc la date limite du 1 septembre)
        with self.assertRaisesMessage(ValidationError, message):
            Programmation.objects.create(
                    debut=debut_prog,
                    fin=debut_prog + datetime.timedelta(days=40),
                    frequence=Frequence.objects.get(pk=3),
                    diffusable=Diffusable.objects.get(pk=1),
                    programmateur=self.programmateurTest)

    def test_Programmation_fin_apres_fin_annee_scolaire_modification(self):
        """
        S'assure que la fin d'une `Programmation` ne peut
        pas être postérieure à la fin de l'année scolaire
        lors de la modification d'une `Programmation`.
        """
        maintenant = timezone.now()
        annee_fin = (maintenant + settings.DUREE_JUILLET_DECEMBRE).year
        # On se place au 1 août de l'année scolaire
        debut_prog = timezone.make_aware(datetime.datetime(day=1,
                                                           month=8,
                                                           year=annee_fin))
        # Si on fait le test entre le 1 juillet et le 31 aout
        # On se place au 1 août de l'année suivante
        if maintenant > debut_prog:
            debut_prog = timezone.make_aware(
                                datetime.datetime(day=1,
                                                  month=8,
                                                  year=annee_fin + 1))
        # On crée une `Programmation` toutes les semaines sur 10 jours
        # (ne dépasse donc pas la date limite du 1 septembre)
        programmationTest = Programmation.objects.create(
                            debut=debut_prog,
                            fin=debut_prog + datetime.timedelta(days=10),
                            frequence=Frequence.objects.get(pk=3),
                            diffusable=Diffusable.objects.get(pk=1),
                            programmateur=self.programmateurTest)
        # Augmente le fin de 30 jours (dépasse la date limite du 1 septembre)
        programmationTest.fin += datetime.timedelta(days=30)
        message = "La fin de la programmation ne peut pas être " \
                  "postérieure à la fin de l'année scolaire,"
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()

    def test_Programmation_max_repetitions_creation(self):
        """
        S'assure qu'une `Programmation` ne peut pas comporter
        plus de MAX_REPETITIONS répétitions d'`Occurence`.
        Création d'une `Programmation` toutes les heures sur
        MAX_REPETITIONS heures dans une plage horaire libre.
        """
        maintenant = timezone.now()
        message = "Cette programmation comporte {nb_occ} occurrences. " \
                  "Le nombre maximum d'occurrences autorisé " \
                  "est {nb_max}.".format(nb_occ=settings.MAX_REPETITIONS + 1,
                                         nb_max=settings.MAX_REPETITIONS)
        with self.assertRaisesMessage(ValidationError, message):
            Programmation.objects.create(
                    debut=maintenant + datetime.timedelta(days=15),
                    fin=maintenant + datetime.timedelta(
                                            days=15,
                                            hours=settings.MAX_REPETITIONS),
                    frequence=Frequence.objects.get(pk=1),
                    diffusable=Diffusable.objects.get(pk=1),
                    programmateur=self.programmateurTest)

    def test_Programmation_max_repetitions_modification(self):
        """
        S'assure qu'une `Programmation` ne peut pas comporter
        plus de MAX_REPETITIONS répétitions de l'`Occurence`.
        Modification d'une `Programmation` programmée toutes les heures
        en mettant la fin MAX_REPETITIONS heures plus tard.
        """
        programmationTest = Programmation.objects.get(pk=2)
        nb_occs = len(programmationTest.get_occurrences())
        programmationTest.fin += datetime.timedelta(
                                            days=settings.MAX_REPETITIONS)
        message = "Cette programmation comporte {nb_occ} " \
                  "occurrences. Le nombre maximum d'occurrences " \
                  "autorisé est {nb_max}." \
                  .format(nb_occ=settings.MAX_REPETITIONS + nb_occs,
                          nb_max=settings.MAX_REPETITIONS)
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()

    def test_Programmation_duree_sup_frequence_creation(self):
        """
        S'assure qu'on ne peut pas créer une `Programmation` dont la
        durée du `Diffusable` est supérieure à la `Frequence` choisie.
        """
        maintenant = timezone.now()
        playlistTest = Playlist.objects.first()
        # On cherche un `Diffusable` de durée non nulle
        diffusables = Diffusable.objects.filter(
                                    _duree__gt=settings.DUREE_NULLE)
        # Vérification qu'il y a bien un `Diffusable` de durée non nulle
        self.assertGreater(diffusables.count(),
                           0)
        diffusableTest = diffusables.first()
        # `Frequence` d'une durée de 1 heure
        frequenceTest = Frequence.objects.get(pk=1)
        nb_insertions = frequenceTest.delta // diffusableTest.duree + 1
        # On ajoute nb_insertions fois le `Diffusable` à la `Playlist`
        for i in range(nb_insertions):
            Ordonner.objects.create(playlist=playlistTest,
                                    diffusable=diffusableTest,
                                    position=100 + i)
        message = "La durée du diffusable ne peut pas être " \
                  "supérieure à la fréquence de programmation."
        # On essaye de programmer toutes les heures
        with self.assertRaisesMessage(ValidationError, message):
            Programmation.objects.create(
                    debut=maintenant + datetime.timedelta(days=2),
                    fin=maintenant + datetime.timedelta(days=2,
                                                        hours=2),
                    frequence=Frequence.objects.get(pk=1),
                    diffusable=playlistTest,
                    programmateur=self.programmateurTest)

    def test_Programmation_duree_sup_frequence_modification(self):
        """
        S'assure qu'on ne peut pas modifier une `Programmation` de telle façon
        que la durée du `Diffusable` devienne supérieure à la `Frequence`.
        """
        # `Programmation` programmée toutes les heures
        programmationTest = Programmation.objects.get(pk=5)
        # Cherche une `Playlist` en excluant éventuellement celle programmée
        playlists = Playlist.objects.exclude(
                                pk=programmationTest.diffusable.pk)
        playlistTest = playlists.first()
        # On cherche un `Diffusable` de durée non nulle
        diffusables = Diffusable.objects.filter(
                                _duree__gt=settings.DUREE_NULLE)
        # Vérification qu'il y a bien un `Diffusable` de durée non nulle
        self.assertGreater(diffusables.count(),
                           0)
        diffusableTest = diffusables.first()
        # `Frequence` d'une durée de 1 heure
        frequenceTest = Frequence.objects.get(pk=1)
        nb_insertions = frequenceTest.delta // diffusableTest.duree + 1
        # On ajoute nb_insertions fois le `Diffusable` à la `Playlist`
        for i in range(nb_insertions):
            Ordonner.objects.create(playlist=playlistTest,
                                    diffusable=diffusableTest,
                                    position=100 + i)
        message = "La durée du diffusable ne peut pas être " \
                  "supérieure à la fréquence de programmation."
        # Modifie le `Diffusable` de la `Programmation`
        programmationTest.diffusable = playlistTest
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()

    def test_Programmation_creneaux_libres_creation(self):
        """
        S'assure qu'on ne puisse pas créer une `Programmation`
        dont les `Occurrence` entrent en conflit avec celles
        des `Programmation` déja enregistrées.
        """
        # `Frequence` "toutes les heures"
        frequenceTest = Frequence.objects.get(pk=1)
        diffusableTest = Diffusable.objects.get(pk=1)
        # Vérification que le `Diffusable` utilisé a
        # une durée supérieure à 30s
        self.assertGreater(diffusableTest.duree,
                           datetime.timedelta(seconds=30))
        # `Programmation` ayant une seule `Occurrence`
        programmationTest = Programmation.objects.get(pk=7)
        message = "Une programmation déja enregistrée utilise tout " \
                  "ou une partie de la plage horaire"
        # On crée une `Programmation` dont une  `Occurence`
        # débute un peu avant le debut de programmationTest
        with self.assertRaisesMessage(ValidationError, message):
            Programmation.objects.create(
                    debut=programmationTest.debut - datetime.timedelta(
                                                                hours=2,
                                                                seconds=30),
                    fin=programmationTest.debut + datetime.timedelta(hours=2),
                    frequence=frequenceTest,
                    diffusable=diffusableTest,
                    programmateur=self.programmateurTest)
        # On crée une `Programmation` un peu après le
        # debut de programmationTest
        with self.assertRaisesMessage(ValidationError, message):
            Programmation.objects.create(
                    debut=programmationTest.debut - datetime.timedelta(
                                                                hours=1,
                                                                minutes=59,
                                                                seconds=30),
                    fin=programmationTest.debut + datetime.timedelta(hours=2),
                    frequence=frequenceTest,
                    diffusable=diffusableTest,
                    programmateur=self.programmateurTest)
        # `Programmation` ayant plusieurs `Occurrence`
        programmationTest = Programmation.objects.get(pk=5)
        # On crée une `Programmation` un peu avant le debut
        # d'une `Occurrence` de programmationTest
        with self.assertRaisesMessage(ValidationError, message):
            Programmation.objects.create(
                    debut=programmationTest.debut + datetime.timedelta(
                                                                hours=1,
                                                                minutes=59,
                                                                seconds=30),
                    fin=programmationTest.debut + datetime.timedelta(hours=4),
                    frequence=frequenceTest,
                    diffusable=diffusableTest,
                    programmateur=self.programmateurTest)
        # On crée une `Programmation` un peu après le debut
        # d'une `Occurrence` de programmationTest
        with self.assertRaisesMessage(ValidationError, message):
            Programmation.objects.create(
                    debut=programmationTest.debut + datetime.timedelta(
                                                                hours=2,
                                                                seconds=30),
                    fin=programmationTest.debut + datetime.timedelta(hours=4),
                    frequence=frequenceTest,
                    diffusable=diffusableTest,
                    programmateur=self.programmateurTest)

    def test_Programmation_creneaux_libres_modification(self):
        """
        S'assure qu'on ne puisse pas modifier une `Programmation` de telle
        façon que les `Occurrence` entrent en conflit avec celles
        des autres `Programmation` déja enregistrées.
        """
        # `Programmation` dans le futur, on change le debut qui vient
        # en concurrence avec la `Programmation` (pk=7)
        message = "Une programmation déja enregistrée utilise tout " \
                  "ou une partie de la plage horaire"
        programmationTest = Programmation.objects.get(pk=5)
        programmationTest.debut += datetime.timedelta(hours=10)
        programmationTest.fin += datetime.timedelta(hours=10)
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()
        # change le debut, la fin et la fréquence de la `Programmation` (pk=5)
        # qui rentre alors en concurrence avec la `Programmation` (pk=8)
        programmationTest = Programmation.objects.get(pk=5)
        programmationTest.debut += datetime.timedelta(hours=9,
                                                      minutes=20)
        programmationTest.frequence = Frequence.objects.get(pk=4)
        programmationTest.fin += datetime.timedelta(days=10)
        with self.assertRaisesMessage(ValidationError, message):
            programmationTest.save()

    def test_Programmation_date_creation(self):
        """
        Vérifie que l'option auto_now_add=True
        du champ date_creation fonctionne comme voulu.
        """
        # Vérifie que le champ date_creation est bien
        # rempli lors de la création
        maintenant1 = timezone.now()
        programmationTest = Programmation.objects.create(
                debut=maintenant1 + datetime.timedelta(days=2),
                fin=maintenant1 + datetime.timedelta(days=2),
                frequence=Frequence.objects.get(pk=2),
                diffusable=Diffusable.objects.get(pk=1),
                programmateur=self.programmateurTest)
        maintenant2 = timezone.now()
        self.assertGreaterEqual(programmationTest.date_creation,
                                maintenant1)
        self.assertLessEqual(programmationTest.date_creation,
                             maintenant2)
        # Vérifie que le champ  date_creation n'est pas
        # modifié lors d'une modification
        programmationTest = Programmation.objects.get(pk=7)
        date_creation_ini = programmationTest.date_creation
        programmationTest.debut += datetime.timedelta(days=1)
        programmationTest.fin += datetime.timedelta(days=1)
        programmationTest.save()
        self.assertEqual(date_creation_ini,
                         programmationTest.date_creation)

    def test_Programmation_date_modification(self):
        """
        Vérifie que l'option auto_now=True du champ
        date_modification fonctionne comme voulu.
        """
        # Vérifie que le champ date_modification est bien
        # rempli lors de la création
        maintenant1 = timezone.now()
        programmationTest = Programmation.objects.create(
                debut=maintenant1 + datetime.timedelta(days=2),
                fin=maintenant1 + datetime.timedelta(days=2),
                frequence=Frequence.objects.get(pk=2),
                diffusable=Diffusable.objects.get(pk=1),
                programmateur=self.programmateurTest)
        maintenant2 = timezone.now()
        self.assertGreaterEqual(programmationTest.date_modification,
                                maintenant1)
        self.assertLessEqual(programmationTest.date_modification,
                             maintenant2)
        # Vérifie que le champ date_modification est
        # modifié lors d'un enregistrement sans modification
        programmationTest = Programmation.objects.get(pk=7)
        date_modification_ini = programmationTest.date_modification
        programmationTest.save()
        self.assertNotEqual(date_modification_ini,
                            programmationTest.date_modification)
        # Vérifie que le champ  date_modification est
        # modifié lors d'un enregistrement avec modification
        maintenant1 = timezone.now()
        programmationTest = Programmation.objects.get(pk=7)
        programmationTest.fin += datetime.timedelta(days=2)
        programmationTest.save()
        maintenant2 = timezone.now()
        self.assertGreaterEqual(programmationTest.date_modification,
                                maintenant1)
        self.assertLessEqual(programmationTest.date_modification,
                             maintenant2)

    def test_Programmation_suppression(self):
        """
        Vérifie qu'on ne peut pas supprimer une `Programmation` débutant
        dans le passé.
        """
        # `Programmation` débutant dans le futur
        # On doit pouvoir la supprimer
        nb_programmations = Programmation.objects.all().count()
        programmationTest = Programmation.objects.get(pk=7)
        programmationTest.delete()
        self.assertEqual(Programmation.objects.all().count(),
                         nb_programmations - 1)
        # `Programmation` débutant dans le passé et avec la fin dans le passé
        # On ne doit pas pouvoir la supprimer
        programmationTest = Programmation.objects.get(pk=4)
        message = "Une programmation ne peut pas être supprimée si " \
                  "elle a déjà commencé ou si elle va bientôt commencer. " \
                  "La suppression est autorisée pour " \
                  "les programmations débutant après "
        with transaction.atomic():
            with self.assertRaisesMessage(LockedError, message):
                programmationTest.delete()
        # `Progammation` débutant dans le passé et fin dans le futur
        # On ne doit pas pouvoir la supprimer
        # Teste la deuxième partie du message d'erreur
        programmationTest = Programmation.objects.get(pk=2)
        message = "Pour annuler les occurences pas encore diffusées, " \
                  "il faut modifier la date de fin de la programmation."
        with transaction.atomic():
            with self.assertRaisesMessage(LockedError, message):
                programmationTest.delete()
        # QuerySet de `Programmation` dont certaines débutent dans le passé
        # On ne doit pas pouvoir faire la suppression par lot
        limite_debut = timezone.now() - datetime.timedelta(hours=12)
        programmations = Programmation.objects.filter(
                debut__gte=limite_debut).order_by("-debut")
        # Vérification qu'il y a bien des `Programmation` dans le passé
        # et dans le futur
        self.assertLess(programmations.last().debut,
                        timezone.now())
        self.assertGreater(programmations.first().debut,
                           timezone.now())
        with transaction.atomic():
            with self.assertRaises(LockedError):
                programmations.delete()
        self.assertEqual(Programmation.objects.all().count(),
                         nb_programmations - 1)

    def test_Diffusable_est_programme(self):
        """
        Test de la méthode est_programme() de `Diffusable`.
        """
        # `Diffusable` faisant partie d'une `Programmation`
        diffusableTest1 = Diffusable.objects.get(pk=1)
        self.assertTrue(diffusableTest1.est_programme())
        # `Diffusable` ne faisant pas partie d'une `Programmation`
        diffusableTest2 = Diffusable.objects.get(pk=9)
        self.assertFalse(diffusableTest2.est_programme())
        # `Diffusable` non directement programmé (pk=9)
        # dans une `Playlist` non programmée (pk=8)
        diffusableTest3 = Diffusable.objects.get(pk=9)
        self.assertFalse(diffusableTest3.est_programme())
        # `Diffusable` non directement programmé (pk=12)
        # faisant partie d'une `Playlist` (pk=10) qui contient la
        # `Playlist` (pk=9) qui va être programmée
        diffusableTest4 = Diffusable.objects.get(pk=12)
        playlistTest4 = Playlist.objects.get(pk=9)
        Ordonner.objects.create(playlist=playlistTest4,
                                diffusable=diffusableTest4,
                                position=4)
        playlist_prog4 = Playlist.objects.get(pk=8)
        maintenant = timezone.now()
        Programmation.objects.create(
                debut=maintenant + datetime.timedelta(days=2,
                                                      hours=12),
                fin=maintenant + datetime.timedelta(days=2,
                                                    hours=12),
                frequence=Frequence.objects.get(pk=2),
                diffusable=playlist_prog4,
                programmateur=self.programmateurTest)
        self.assertTrue(diffusableTest4.est_programme())

    def test_Diffusable_programmations(self):
        """
        Test de la méthode programmations() de `Diffusable`.
        """
        # `Diffusable` faisant partie d'une `Programmation`
        diffusableTest1 = Diffusable.objects.get(pk=1)
        nb_attendu1 = 3
        self.assertEqual(len(diffusableTest1.programmations()),
                         nb_attendu1)
        # `Diffusable` ne faisant pas partie d'une `Programmation`
        diffusableTest2 = Diffusable.objects.get(pk=9)
        nb_attendu2 = 0
        self.assertEqual(len(diffusableTest2.programmations()),
                         nb_attendu2)
        # `Diffusable` non directement programmé (pk=9)
        # dans une `Playlist` non programmée (pk=8)
        diffusableTest3 = Diffusable.objects.get(pk=9)
        nb_attendu3 = 0
        self.assertEqual(len(diffusableTest3.programmations()),
                         nb_attendu3)
        # `Diffusable` non directement programmé (pk=12)
        # faisant partie d'une `Playlist` (pk=10) qui contient la
        # `Playlist` (pk=9) qui va être programmée
        diffusableTest4 = Diffusable.objects.get(pk=12)
        playlistTest4 = Playlist.objects.get(pk=9)
        Ordonner.objects.create(playlist=playlistTest4,
                                diffusable=diffusableTest4,
                                position=4)
        playlist_prog4 = Playlist.objects.get(pk=8)
        maintenant = timezone.now()
        Programmation.objects.create(
                debut=maintenant + datetime.timedelta(days=2,
                                                      hours=12),
                fin=maintenant + datetime.timedelta(days=2,
                                                    hours=12),
                frequence=Frequence.objects.get(pk=2),
                diffusable=playlist_prog4,
                programmateur=self.programmateurTest)
        nb_attendu4 = 1
        self.assertEqual(len(diffusableTest4.programmations()),
                         nb_attendu4)

    def test_Diffusable_programme_modification(self):
        """
        S'assure qu'on ne peut pas modifier un `Diffusable` faisant
        partie d'une `Programmation`.
        """
        # Modification d'un `Diffusable` faisant partie d'une `Programmation`
        diffusableTest = Diffusable.objects.get(pk=1)
        message = "ne peut plus être modifié car il est verrouillé par"
        diffusableTest.titre = "titreTest"
        with self.assertRaisesMessage(LockedError, message):
            diffusableTest.save()
        # Modification d'une `Playlist` faisant partie d'une `Programmation`
        # On vérifie qu'on ne peut pas ajouter de `Diffusable`
        # dans la `Playlist`
        playlistTest = Playlist.objects.get(pk=10)
        nb_ordonners = playlistTest.liste.count()
        with self.assertRaisesMessage(LockedError, message):
            Ordonner.objects.create(playlist=playlistTest,
                                    diffusable=diffusableTest,
                                    position=4)
        # Vérification que le nombre de `Diffusable` dans la `Playlist`
        # n'a pas changé
        self.assertEqual(playlistTest.liste.count(),
                         nb_ordonners)
        # On vérifie qu'on ne peut pas modifier un `Ordonner` de la `Playlist`
        ordonnerTest = Ordonner.objects.filter(playlist=playlistTest).first()
        ordonnerTest.position = 999
        with self.assertRaisesMessage(LockedError, message):
            ordonnerTest.save()
        # Vérification que le nombre de `Diffusable` dans la `Playlist`
        # n'a pas changé
        self.assertEqual(playlistTest.liste.count(),
                         nb_ordonners)

    def test_Diffusable_programme_suppression(self):
        """
        S'assure qu'on ne peut pas supprimer un `Diffusable` faisant
        partie d'une `Programmation`.
        """
        # Suppression d'un `Diffusable` faisant partie d'une `Programmation`
        diffusableTest = Diffusable.objects.get(pk=1)
        message = "ne peut plus être modifié car il est verrouillé par"
        diffusableTest.titre = "titreTest"
        with self.assertRaises(ProtectedError):
            diffusableTest.delete()
        # Modification d'une `Playlist` faisant partie d'une `Programmation`
        # On vérifie qu'on ne peut pas supprimer de `Diffusable`
        # dans la `Playlist`
        playlistTest = Playlist.objects.get(pk=10)
        nb_ordonners = playlistTest.liste.count()
        ordonnerTest = Ordonner.objects.filter(playlist=playlistTest).first()
        with transaction.atomic():
            with self.assertRaisesMessage(LockedError, message):
                ordonnerTest.delete()
        # Vérification que le nombre de `Diffusable` dans la `Playlist`
        # n'a pas changé
        self.assertEqual(playlistTest.liste.count(),
                         nb_ordonners)
