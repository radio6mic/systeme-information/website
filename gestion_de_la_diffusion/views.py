# -*- coding: utf-8 -*-

"""
Définition des vues applicatives
"""

from django.http import HttpResponse


def index(request):
    """
    Vue principale de l'application.
    """
    if request.method == 'GET':
        return HttpResponse("Vous êtes bien sur la vue de la"
                            "gestion de la diffusion.")
