# -*- coding: utf-8 -*-

"""
Définition des URLs de routage de l'application.
"""

from django.conf.urls import url
from . import views


urlpatterns = [
    url(r"^$", views.index, name="index"),
]
