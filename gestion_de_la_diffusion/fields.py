# -*- coding: utf-8 -*-

"""
Définition des champs de modèle pour la gestion de la diffusion.
"""

import logging
import audiotools
from django.core import checks
from django.db.models.fields import files
from . import forms
from . import sounds

LOG = logging.getLogger(__name__)


class SoundFieldFile(sounds.SoundFile, files.FieldFile):
    """
    Abstraction qui fournit l'accès au système de fichier.
    """
    def save(self, *args, **kwargs):
        """
        Surcharge de la méthode save() pour usage ultérieur.
        """
        super().save(*args, **kwargs)

    def delete(self, save=True, keep_file=False):
        """
        Effectue la suppression du fichier dans le système de fichiers.
        keep_file: booléen, permet de contourner cette destruction.
        """
        # Nettoie le cache des données associées
        # au fichier ayant déjà été ouvert
        if hasattr(self, "_duree_cache"):
            del self._duree_cache
        if hasattr(self, "_metadata_cache"):
            del self._metadata_cache
        if not keep_file:
            super().delete(save)


class SoundField(files.FileField):
    """
    Type de champs qui représente dans la base de donnée le fichier sonore.
    """
    attr_class = SoundFieldFile
    descriptor_class = files.FileDescriptor
    description = "Sound"
    available_formats = ["{nom} (.{ext})".format(nom=typ.DESCRIPTION,
                                                 ext=typ.NAME)
                         for typ in audiotools.AVAILABLE_TYPES
                         if typ.supports_to_pcm()]
    default_error_messages = {
            "invalid_sound": "Chargez un fichier sonore valide. "
            "Le fichier chargé n'était pas un fichier sonore ou bien "
            "le fichier était corrompu. "
            "Formats de fichiers autorisés : {liste_formats}."
            "".format(liste_formats=str(available_formats).strip("]["))}

    def __init__(self, verbose_name=None, name=None, **kwargs):
        super().__init__(verbose_name=verbose_name, name=name, **kwargs)

    def check(self, **kwargs):
        errors = super().check(**kwargs)
        errors.extend(self._check_audiotools_library_installed())
        return errors

    def _check_audiotools_library_installed(self):
        try:
            import audiotools
        except ImportError:
            return [
                checks.Error(
                    "Cannot use SoundField because audiotools"
                    "is not installed.",
                    hint=("Get audiotools at https://pypi.python.org/pypi/"
                          "audiotools or run command "
                          "'pip install fmoo-audiotools'."),
                    obj=self,
                )
            ]
        else:
            return []

    def get_internal_type(self):
        return "FileField"

    def contribute_to_class(self, cls, name, **kwargs):
        """
        Surcharge de la méthode assurant l'association entre la classe
        `SoundField` et son gestionnaire `SoundFile`.
        Pour utilisation future.
        """
        super().contribute_to_class(cls, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {"form_class": forms.SoundField}
        defaults.update(kwargs)
        return super().formfield(**defaults)
