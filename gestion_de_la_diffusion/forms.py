# -*- coding: utf-8 -*-

"""
Définition du modèle de données pour la gestion des musiques.
"""

import os
import tempfile
from io import BytesIO
from django import forms
from django.core.exceptions import ValidationError
import logging
import audiotools
from website import settings

LOG = logging.getLogger(__name__)


class SoundField(forms.FileField):
    """
    Classe `SoundField` disponible dans les formulaires.
    """
    def to_python(self, data):
        """
        Vérifie que les données du champ de fichier d'upload contient bien
        du son (n'importe quoi que la bilbiothèque 'audiotools' gère).
        """
        file_handler = tempfile.NamedTemporaryFile()
        f = super().to_python(data)
        if f is None:
            return None

        if hasattr(data, 'temporary_file_path'):
            file = data.temporary_file_path()
        else:
            if hasattr(data, 'read'):
                file_content = BytesIO(data.read())
            else:
                file_content = BytesIO(data['content'])
            file_handler.write(file_content.getvalue())
            file = file_handler.name
            LOG.debug("Fichier temporaire du SoundField {!r} : {!r}"
                      .format(str(data),
                              file))
        try:
            audiotools.open(file)
            LOG.debug("Le fichier {nom!r} est bien un fichier son"
                      .format(nom=file))
        except audiotools.UnsupportedFile as exc:
            raise ValidationError(
                    self.error_messages['invalid_sound'],
                    code='invalid_sound',) from exc
        finally:
            file_handler.close()

        # Conformation du fichier
        self.conformation(file)

        if hasattr(f, 'seek') and callable(f.seek):
            f.seek(0)
        return f

    def conformation(self, file_path):
        """
        Mise en conformité du fichier sonore.
        """
        LOG.debug("# CONFORMATION")
        # Création d'un fichier temporaire dans le même répertoire
        # que le fichier à conformer
        fichier_destination = tempfile.NamedTemporaryFile(
                dir=os.path.dirname(file_path),
                delete=False)
        LOG.debug("## Fichier temporaire de travail {!r}"
                  .format(fichier_destination.name))
        source_audio_file = audiotools.open(file_path)
        if (settings.AUDIO_CONFIG['bits_per_sample'] ==
                source_audio_file.bits_per_sample()
            and settings.AUDIO_CONFIG['channels'] ==
                source_audio_file.channels()
            and settings.AUDIO_CONFIG['sample_rate'] ==
                source_audio_file.sample_rate()):
            audio_convertisseur = source_audio_file.to_pcm()
        else:
            if source_audio_file.supports_to_pcm():
                LOG.info("## Création du convertisseur au format '{} Hz, "
                         "{} chan, {} bits'".format(
                                 settings.AUDIO_CONFIG['sample_rate'],
                                 settings.AUDIO_CONFIG['channels'],
                                 settings.AUDIO_CONFIG['bits_per_sample']))
                audio_convertisseur = audiotools.PCMConverter(
                    source_audio_file.to_pcm(),
                    sample_rate=settings.AUDIO_CONFIG['sample_rate'],
                    channels=settings.AUDIO_CONFIG['channels'],
                    channel_mask=settings.AUDIO_CONFIG['channel_mask'],
                    bits_per_sample=settings.AUDIO_CONFIG['bits_per_sample'])
            else:
                LOG.error("Le décodeur {decodeur!r} pour ce format {format!r} "
                          "n'est pas installé sur le système"
                          "".format(decodeur=source_audio_file.DESCRIPTION,
                                    format=source_audio_file.NAME))
                raise audiotools.DecodingError("Aucun décodeur disponible")

        LOG.info("## Sélection du type de conteneur de sortie")
        LOG.info("   Type: {!r}".format(settings.AUDIO_CONFIG['type']))
        LOG.info("   Comprimer le fichier en {type!r} dans {file!r}".format(
                file=fichier_destination.name,
                type=settings.AUDIO_CONFIG['type']))
        if settings.AUDIO_CONFIG['type'] == 'flac':
            conteneur = getattr(audiotools, 'FlacAudio')
        else:  # settings.AUDIO_CONFIG['type'] == 'mp3'
            conteneur = getattr(audiotools, 'MP3Audio')
        conteneur.from_pcm(fichier_destination.name,
                           audio_convertisseur,
                           compression='insane')
        os.rename(fichier_destination.name,
                  file_path)
