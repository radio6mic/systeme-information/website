# -*- coding: utf-8 -*-
"""
Définition des classes utilisées dans l'application.
"""
import datetime


class Occurrence():
    """
    Une `Occurrence` d'une `Programmation` est un intervalle de temps
    pendant lequel un `Diffusable` est diffusé lors d'une `Programmation`.
    """
    def __init__(self, debut=None, fin=None):
        self._debut = debut
        self._fin = fin

    def __repr__(self):
        return "<Occurrence : ({debut} ; {fin})".format(debut=self._debut,
                                                        fin=self._fin)

    def __str__(self):
        return "({debut} ; {fin})".format(debut=self._debut,
                                          fin=self._fin)

    def __eq__(self, occ):
        return (self.debut == occ.debut) and (self.fin == occ.fin)

    @property
    def debut(self):
        return self._debut

    @debut.setter
    def debut(self, value):
        if type(value) is datetime.datetime:
            self._debut = value
        else:
            raise TypeError("Le début d'une Occurrence doit être "
                            "de type datime.datetime")

    @property
    def fin(self):
        return self._fin

    @fin.setter
    def fin(self, value):
        if type(value) is datetime.datetime:
            self._fin = value
        else:
            raise TypeError("La fin d'une Occurrence doit être "
                            "de type datime.datetime")

    def duree(self):
        try:
            return self._fin - self._debut
        except TypeError:
            return None

    def intersection(self, occ):
        pass

    def est_vide(self):
        try:
            if self._debut >= self._fin:
                return True
            return False
        except TypeError:
            return True
