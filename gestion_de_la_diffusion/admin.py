# -*- coding: utf-8 -*-

"""
Définition de la configuration du site d'administration
pour la gestion de la diffusion.
"""

from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.html import escape, format_html
from website.settings import DELAI_AVANT_DIFFUSION
from django.contrib import admin, messages
from django.contrib.admin import helpers
from django.contrib.admin.models import LogEntry
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from .models import (Playlist,
                     Diffusable,
                     Ordonner,
                     Editeur,
                     Licence,
                     Provenance,
                     Medium,
                     Frequence,
                     Programmation)
from .exceptions import LockedError
from django import forms


class OrdonnerForm(forms.ModelForm):
    """
    Formulaire pour OrdonnerPourPlaylistInline et OrdonnerPourDiffusableInline.
    Permet de ne pas avoir accès à la valeur 0 pour la position d'un
    `Diffusable` dans une `Playlist`.
    """
    position = forms.IntegerField(
            min_value=1,
            help_text="Position du diffusable dans la liste de "
                      "lecture. Les positions doivent être toutes "
                      "différentes dans une même liste de lecture.")

    class Meta:
        model = Ordonner
        fields = ("playlist",
                  "diffusable")


class OrdonnerPourPlaylistInline(admin.TabularInline):
    """
    Définition des attributs d'affichage Inline
    de la classe `Ordonner` pour les `Playlist`.
    """
    model = Ordonner
    form = OrdonnerForm
    verbose_name = "diffusable"
    verbose_name_plural = "contenu de la liste de lecture"
    fk_name = "playlist"
    fields = ["diffusable", "get_duree", "position"]
    readonly_fields = ["get_duree"]
    extra = 0
    autocomplete_fields = ["diffusable"]
    ordering = ["position"]

    def get_duree(self, instance):
        """
        Retourne la durée du `Diffusable` enregistré dans un `Ordonner`.
        """
        return instance.diffusable.duree

    get_duree.short_description = "durée"


class PlaylistAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Playlist`.
    """
    fieldsets = [(None,
                  {"fields": ("titre",
                              "duree",
                              "date_publication",
                              ("licence", "editeur"),
                              "description")}),
                 ("DATES DE PROGRAMMATION",
                  {"fields": ("programmations_liste_html",),
                   "classes": ("collapse",),
                   "description": "Dates de programmation de la liste de "
                                  "lecture ou des listes de lectures la "
                                  "contenant."})]
    inlines = [OrdonnerPourPlaylistInline]
    list_display = ("titre",
                    "duree",
                    "est_programme")
    readonly_fields = ["duree",
                       "programmations_liste_html"]
    search_fields = ["titre",
                     "description"]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Permet de n'avoir que l'`Editeur` "Lycée André Malraux"
        comme `Editeur` possible d'une `Playlist`.
        """
        if db_field.name == "editeur":
            kwargs["queryset"] = Editeur.objects.filter(
                                            nom="Lycée André Malraux")
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_changeform_initial_data(self, request):
        """
        Utilise par défaut l'`Editeur` "Lycée André Malraux" comme `Editeur`
        d'une nouvelle `Playlist`.
        """
        initial = super().get_changeform_initial_data(request)
        initial["editeur"] = "1"
        return initial

    def change_view(self, request, object_id, form_url="", extra_context=None):
        """
        Gestion de `LockedError` si on essaye de modifier une `Playlist`
        qui est programmée : un message d'erreur est envoyé à l'utilisateur
        indiquant les `Programmation` verrouillant la `Playlist`.
        """
        try:
            return super().change_view(request,
                                       object_id,
                                       form_url,
                                       extra_context)
        except LockedError as erreur:
            message = escape(erreur.message)
            for obj in erreur.related_objects:
                message += "<br /><strong> {related_obj} </strong>" \
                           .format(related_obj=escape(obj))
            self.message_user(request, mark_safe(message), messages.ERROR)
            opts = self.model._meta
            return_url = reverse("admin:{app}_{model}_change".format(
                                                     app=opts.app_label,
                                                     model=opts.model_name),
                                 args=(object_id,),
                                 current_app=self.admin_site.name)
            return HttpResponseRedirect(return_url)


class DiffusableAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Diffusable`.
    """
    fieldsets = [(None,
                  {"fields": ("titre",
                              "duree",
                              "nature",
                              "date_publication",
                              ("licence", "editeur"))}),
                 ("DATES DE PROGRAMMATION",
                  {"fields": ("programmations_liste_html",),
                   "classes": ("collapse",),
                   "description": "Dates de programmation du diffusable ou "
                                  "des listes de lectures le contenant."}),
                 ("LISTES DE LECTURES",
                  {"fields": ("playlists_liste_html",),
                   "classes": ("collapse",),
                   "description": "Ce diffusable fait partie des listes "
                                  "de lectures suivantes."})]
    list_display = ("titre",
                    "duree",
                    "nature",
                    "date_publication",
                    "licence",
                    "est_programme")
    readonly_fields = ["est_programme",
                       "nature",
                       "duree",
                       "programmations_liste_html",
                       "playlists_liste_html"]
    list_filter = ["date_publication",
                   "licence"]
    date_hierarchy = "date_publication"
    search_fields = ["titre"]
    autocomplete_fields = ["editeur",
                           "licence"]

    def change_view(self, request, object_id, form_url="", extra_context=None):
        """
        Gestion de `LockedError` si on essaye de modifier un `Diffusable`
        qui est programmé : un message d'erreur est envoyé à l'utilisateur
        indiquant les `Programmation` verrouillant le `Diffusable`.
        """
        try:
            return super().change_view(request,
                                       object_id,
                                       form_url,
                                       extra_context)
        except LockedError as erreur:
            message = escape(erreur.message)
            for obj in erreur.related_objects:
                message += "<br /><strong> {related_obj} </strong>" \
                           .format(related_obj=escape(obj))
            self.message_user(request, mark_safe(message), messages.ERROR)
            opts = self.model._meta
            return_url = reverse("admin:{app}_{model}_change".format(
                                                     app=opts.app_label,
                                                     model=opts.model_name),
                                 args=(object_id,),
                                 current_app=self.admin_site.name)
            return HttpResponseRedirect(return_url)


class LicenceAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Licence`.
    """
    list_display = ("acronyme",
                    "nom",
                    "url")
    search_fields = ["acronyme",
                     "texte",
                     "url"]


class EditeurAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Editeur`.
    """
    list_display = ("nom",
                    "url")
    search_fields = ["nom",
                     "url"]


class ProvenanceAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Provenance`.
    """
    list_display = ("description",
                    "url",
                    "medium")
    search_fields = ["description",
                     "url"]
    autocomplete_fields = ["medium"]


class MediumAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Medium`.
    """
    search_fields = ["nom"]


class ProgrammationAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Programmation`.
    """
    fieldsets = [(None,
                  {"fields": (("debut", "fin"),
                              "frequence",
                              "diffusable",
                              "programmateur")}),
                 ("OCCURENCES DE LA PROGRAMMATION",
                  {"fields": ("get_duree",
                              "occurrences_liste_html",),
                   "classes": ("collapse",)})]
    list_display = ["diffusable",
                    "debut",
                    "fin",
                    "frequence",
                    "programmateur",
                    "get_duree",
                    "occurrences_liste_html"]
    ordering = ["debut"]
    search_fields = ["diffusable__titre"]
    readonly_fields = ["get_duree",
                       "occurrences_liste_html"]
    autocomplete_fields = ["diffusable",
                           "programmateur"]

    def get_duree(self, instance):
        """
        Retourne la durée du `Diffusable` programmé.
        """
        return instance.diffusable.duree

    get_duree.short_description = "durée d'une occurrence"

    def get_changeform_initial_data(self, request):
        """
        Utilise par défaut le nom de l'utilisateur comme programmateur
        d'une nouvelle `Programmation`.
        """
        initial = super().get_changeform_initial_data(request)
        initial["programmateur"] = request.user
        return initial

    def delete_view(self, request, object_id, extra_context=None):
        """
        Si l'utilisateur essaye de supprimer une `Programmation` ayant déjà
        débuté (une `LockedError` est lancée), le message d'erreur est envoyé
        à l'utilisateur sur la page de suppression.
        """
        try:
            return super().delete_view(request, object_id, extra_context)
        except LockedError as erreur:
            prog = Programmation.objects.get(pk=object_id)
            message = "<strong> {nom} </strong> n'a pas été " \
                      "supprimée.".format(nom=escape(prog))
            message += "<br />"
            message += escape(erreur.message)
            self.message_user(request, mark_safe(message), messages.ERROR)
            opts = self.model._meta
            return_url = reverse("admin:{app}_{model}_change".format(
                                                     app=opts.app_label,
                                                     model=opts.model_name),
                                 args=(object_id,),
                                 current_app=self.admin_site.name)
            return HttpResponseRedirect(return_url)

    def response_action(self, request, queryset):
        """
        Si l'utilisateur essaye de supprimer un ensemble de `Programmation`
        contenant au moins une `Programmation` protégée
        (une ProtectedError est lancée), le message d'erreur est envoyé
        à l'utilisateur sur la page de suppression avec le nom des
        `Programmation` protégées.
        """
        try:
            return super().response_action(request, queryset)
        except LockedError as erreur:
            message = erreur.message
            # Récupère les id des `Programmation` sélectionnées
            progs_id = request.POST.getlist(helpers.ACTION_CHECKBOX_NAME)
            # Récupère les `Programmation` protégées
            limite_debut = timezone.now() + DELAI_AVANT_DIFFUSION
            progs = Programmation.objects.filter(
                            pk__in=progs_id).filter(
                                    debut__lte=limite_debut)
            # Personnalise le message d'erreur à envoyer à l'utilisateur
            # avec le nom des `Programmation` non supprimables
            if progs.count() == 1:
                message += "<br /> La programmation suivante " \
                           "ne peut pas être supprimée :<br /><strong>" \
                           " {prog} </strong>".format(prog=escape(progs[0]))
            else:
                message += "<br /> Les programmations suivantes " \
                           "ne peuvent pas être supprimées :"
                for prog in progs:
                    message += "<br /><strong> {prog} </strong>" \
                               .format(prog=escape(prog))
            self.message_user(request, mark_safe(message), messages.ERROR)
            opts = self.model._meta
            return_url = reverse("admin:{app}_{model}_changelist".format(
                                                     app=opts.app_label,
                                                     model=opts.model_name),
                                 current_app=self.admin_site.name)
            return HttpResponseRedirect(return_url)


class LogEntryAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration de la classe `LogEntry`.
    Permet juste de visualiser l'historique. Pas d'ajout, de modification
    ou de suppression possible.
    """
    list_display = ["action_time",
                    "user",
                    "action_str",
                    "content_type",
                    "object_link",
                    "__str__"]
    readonly_fields = ["content_type",
                       "user",
                       "action_time",
                       "object_id",
                       "object_link",
                       "object_repr",
                       "action_flag",
                       "action_str",
                       "change_message"]
    list_display_links = None
    list_filter = ["content_type"]
    ordering = ["-action_time", "user", "content_type"]
    search_fields = ["user__username", "object_repr"]

    def action_str(self, instance):
        """
        Retourne le type d'action de `Suppression` : Création, Modification
        ou Suppression
        """
        if instance.is_addition():
            return "Création"
        elif instance.is_change():
            return "Modification"
        elif instance.is_deletion():
            return "Suppression"
        else:
            return str(instance.action_flag)

    action_str.short_description = "action"

    def object_link(self, instance):
        """
        Affiche le nom de l'objet de `LogEntry` avec un lien vers sa page
        d'édition s'il existe toujours.
        """
        try:
            instance.get_edited_object()
            return format_html("<a href='{url}' title='Accéder à la "
                               "page d&apos;édition de l&apos;objet'> "
                               "{obj_str}</a>",
                               url=instance.get_admin_url(),
                               obj_str=instance.object_repr)
        except ObjectDoesNotExist:
            return format_html("{obj_str}",
                               obj_str=instance.object_repr)
        else:
            return "-"

    object_link.short_description = "objet"

    def has_delete_permission(self, request, obj=None):
        """
        Permet d'interdire de supprimer un `LogEntry` dans l'admin.
        """
        return False

    def has_add_permission(self, request):
        """
        Permet d'interdire d'ajouter un `LogEntry` dans l'admin.
        """
        return False

    def has_change_permission(self, request, obj=None):
        """
        Permet d'interdire de modifier un `LogEntry` dans l'admin.
        """
        return False

    def has_view_permission(self, request, obj=None):
        """
        Permet d'afficher les `LogEntry` dans l'admin pour consultation.
        """
        return True

    def get_actions(self, request):
        """
        Supprime l'action par défaut "Supprimer les objets sélectionnés".
        """
        actions = super().get_actions(request)
        if "delete_selected" in actions:
            del actions["delete_selected"]
        return actions


admin.site.register(Diffusable, DiffusableAdmin)
admin.site.register(Playlist, PlaylistAdmin)
admin.site.register(Licence, LicenceAdmin)
admin.site.register(Editeur, EditeurAdmin)
admin.site.register(Provenance, ProvenanceAdmin)
admin.site.register(Medium, MediumAdmin)
admin.site.register(Frequence)
admin.site.register(Programmation, ProgrammationAdmin)
admin.site.register(LogEntry, LogEntryAdmin)
