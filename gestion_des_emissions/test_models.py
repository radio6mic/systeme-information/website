# -*- coding: utf-8 -*-

"""
Module de test des modèles de la gestion des émissions.
"""

import os
import datetime
import tempfile
import django.test
import django.db.utils
from PIL import Image
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.db.models.deletion import ProtectedError
from .models import (MotClef,
                     RelationEpisodeMotClef,
                     SourceExterne,
                     RelationEpisodeSourceExterne,
                     Emission,
                     Episode,
                     Role,
                     Intervention,
                     Intervenant)
from gestion_de_la_diffusion.models import (Diffusable,
                                            Editeur,
                                            Licence)
from website import settings

# Nombre d'insertions et de suppressions d'objets à réaliser
# lors des tests 'test_objet_insertion_suppression'
NB_INSERTIONS = 5


class RoleTest(django.test.TestCase):
    """
    Test des objets de type `Role`.
    """
    fixtures = ["Role",
                "Group"]
    prefixeTest = "RoleTest" + next(tempfile._get_candidate_names())

    def test_Role_doublon_creation(self):
        """
        S'assure que l'on ne peut pas ajouter un `Role` déjà existant.
        """
        role = Role.objects.first()
        with self.assertRaises(IntegrityError):
            Role.objects.create(name=role.name)

    def test_Role_doublon_modification(self):
        """
        S'assure que l'on ne peut pas modifier le nom d'un `Role`
        en utilisant un nom de `Role` existant.
        """
        role = Role.objects.first()
        roleTest = Role.objects.last()
        with self.assertRaises(IntegrityError):
            roleTest.name = role.name
            roleTest.save()

    def test_Role_existence(self):
        """
        S'assure que les `Role` fournis existent dans
        la base et sont des `Role`.
        """
        roles = Role.objects.all()
        for i in range(3):
            roleTest = roles[i]
            with self.subTest(role=roleTest):
                self.assertIsInstance(Role.objects.get(name=roleTest.name),
                                      Role)

    def test_Role_inexistence(self):
        """
        S'assure qu'une exception est soulevée lorsqu'un `Role` n'existe pas.
        """
        with self.assertRaises(Role.DoesNotExist):
            Role.objects.get(name=self.prefixeTest)

    def test_Role_insertion_suppression(self):
        """
        S'assure qu'insertions et suppressions de `Role` fonctionnent.
        Réalise l'insertion de NB_INSERTIONS `Role`.
        Réalise une suppression des NB_INSERTIONS `Role` insérés.
        """
        nb_roles = Role.objects.all().count()
        roles = [Role(name="{nom}-{suffixe}".format(nom=self.prefixeTest,
                                                    suffixe=str(i)))
                 for i in range(NB_INSERTIONS)]
        for roleTest in roles:
            with self.subTest(role=roleTest):
                roleTest.save()
                roleTest.refresh_from_db()
                self.assertIsNotNone(roleTest.pk)
                roleTest.delete()
        self.assertEqual(Role.objects.all().count(),
                         nb_roles)


class IntervenantTest(django.test.TestCase):
    """
    Test des objets de type Intervenant.
    """
    fixtures = ["Intervenant",
                "Group",
                "User"]

    def test_Intervenant_str(self):
        """
        S'assure que la méthode str() de Intervenant fonctionne comme voulu.
        """
        # Intervenant dont les nom et prenom ne sont pas renseignés
        intervenantTest = Intervenant.objects.get(pk=15)
        self.assertEqual(str(intervenantTest),
                         "vincent")
        # Intervenant dont les nom et prénom sont renseignés
        intervenantTest = Intervenant.objects.get(pk=16)
        self.assertEqual(str(intervenantTest),
                         "Vincent LE TEXIER")


class InterventionTest(django.test.TestCase):
    """
    Test des objets de type `Intervention`.
    """
    fixtures = ["Diffusable",
                "ObjetSonore",
                "Licence",
                "Editeur",
                "Episode",
                "Emission",
                "Role",
                "Group",
                "User",
                "Intervenant",
                "Intervention"]
    # Titre de l'`Episode` utilisé dans les tests, fait partie de emissionTest,
    # possède 8 `Intervention`
    episodeTest_titre = "Conseils aux STS1 pour PPE3"
    nb_interventions_episodeTest = 8
    # Nom de l'`Emission` de episodeTest
    emissionTest_nom = "Transmission"
    # Nom d'un `Intervenant` de episodeTest, il fait partie de 4
    # `Intervention' pour cet `Episode`
    intervenantTest_name = "david.g"
    intervenant_inutile_name = "le-texier.v"
    nb_interventions_intervenantTest = 5
    # Nom d'un `Role` de intervenantTest pour episodeTest
    # Ce role est ulilisé 7 fois dans les fixtures
    roleTest_name = "Animateur"
    role_inutile_name = "Réalisateur"
    nb_interventions_roleTest = 7

    def setUp(self):
        """
        Definit les episodeTest, emissionTest, intervenantTest
        et roleTest utilisés dans chaque test.
        """
        self.emissionTest = Emission.objects.get(nom=self.emissionTest_nom)
        self.episodeTest = Episode.objects.get(titre=self.episodeTest_titre,
                                               emission=self.emissionTest)
        self.intervenantTest = Intervenant.objects.get(
                                        username=self.intervenantTest_name)
        self.roleTest = Role.objects.get(name=self.roleTest_name)
        self.interventionTest = Intervention.objects.get(
                                            episode=self.episodeTest,
                                            intervenant=self.intervenantTest,
                                            role=self.roleTest)

    def test_Intervention_str(self):
        """
        S'assure que la méthode str() de `Intervention`
        fonctionne comme voulue.
        """
        self.assertEqual(str(self.interventionTest),
                         "Grégory DAVID est Animateur pour "
                         "Conseils aux STS1 pour PPE3")

    def test_Intervention_Episode_related(self):
        """
        S'assure que l'`Intervention` fait partie de l"ensemble
        des `Intervention` de l'`Episode` auquel elle est associée.
        """
        self.assertEqual(self.episodeTest.intervention_set.all().count(),
                         self.nb_interventions_episodeTest)
        self.assertIn(self.interventionTest,
                      self.episodeTest.intervention_set.all())

    def test_Episode_suppression(self):
        """
        S'assure que lorsqu'on supprime un `Episode`, toutes les `Intervention`
        associées à cet `Episode` sont supprimées aussi.
        """
        nb_episodes_avant = Episode.objects.all().count()
        nb_interventions_avant = Intervention.objects.all().count()
        # Supprime episodeTest
        self.episodeTest.delete(keep_ObjetSonore=True)
        # Vérifie que l'`Episode` a bien été supprimé
        nb_episodes_apres = Episode.objects.all().count()
        self.assertEqual(nb_episodes_avant,
                         nb_episodes_apres + 1)
        with self.assertRaises(Episode.DoesNotExist):
            Episode.objects.get(titre=self.episodeTest_titre,
                                emission=self.emissionTest)
        # Vérifie que les `Intervention` ont bien été supprimées
        nb_interventions_apres = Intervention.objects.all().count()
        self.assertEqual(
                nb_interventions_avant,
                nb_interventions_apres + self.nb_interventions_episodeTest)
        interventions = Intervention.objects.filter(
                                episode__titre=self.episodeTest_titre,
                                episode__emission__nom=self.emissionTest_nom)
        self.assertEqual(interventions.count(),
                         0)

    def test_Intervention_Intervenant_related(self):
        """
        S'assure que l'`Intervention` fait partie de l'ensemble des
        `Intervention` de l'`Intervenant` auquel elle est associée.
        """
        self.assertEqual(self.intervenantTest.intervention_set.all().count(),
                         self.nb_interventions_intervenantTest)
        self.assertIn(self.interventionTest,
                      self.intervenantTest.intervention_set.all())

    def test_Intervenant_suppression(self):
        """
        S'assure qu'on peut supprimer un `Intervenant` non utilisé
        mais qu'on ne peut pas supprimer un `Intervenant` utilisé
        par un `Episode`.
        S'assure que lorsqu'on supprime un `Intervenant`, toutes les
        `Intervention` contenant cet `Intervenant` sont supprimées aussi.
        """
        nb_intervenants_avant = Intervenant.objects.all().count()
        nb_interventions_avant = Intervention.objects.all().count()
        # Supprime un `Intervenant` non utilisé
        Intervenant.objects.get(
                username=self.intervenant_inutile_name).delete()
        # Vérifie qu'on a bien 1 `Intervenant` de moins
        # et autant d'`Intervention` qu'au début
        nb_intervenants_apres = Intervenant.objects.all().count()
        self.assertEqual(nb_intervenants_avant,
                         nb_intervenants_apres + 1)
        nb_interventions_apres = Intervention.objects.all().count()
        self.assertEqual(nb_interventions_avant,
                         nb_interventions_apres)
        with self.assertRaises(Intervenant.DoesNotExist):
            Intervenant.objects.get(username=self.intervenant_inutile_name)
        # Supprime un `Intervenant` utilisé par des `Episode`
        with self.assertRaises(ProtectedError):
            self.intervenantTest.delete()
        nb_intervenants_apres = Intervenant.objects.all().count()
        self.assertEqual(nb_intervenants_avant,
                         nb_intervenants_apres + 1)
        nb_interventions_apres = Intervention.objects.all().count()
        self.assertEqual(nb_interventions_avant,
                         nb_interventions_apres)

    def test_Intervention_Role_related(self):
        """
        S'assure que l'`Intervention` fait partie de l'ensemble des
        `Intervention` du l'`Role` auquel elle est associée.
        """
        self.assertEqual(self.roleTest.intervention_set.all().count(),
                         self.nb_interventions_roleTest)
        self.assertIn(self.interventionTest,
                      self.roleTest.intervention_set.all())

    def test_Role_suppression(self):
        """
        S'assure qu'on peut supprimer un `Intervenant` non utilisé
        mais qu'on ne peut pas supprimer un `Intervenant` utilisé
        par un `Episode`.
        S'assure que lorsqu'on supprime un `Intervenant`, toutes les
        `Intervention` contenant cet `Intervenant` sont supprimées aussi.
        """
        nb_roles_avant = Role.objects.all().count()
        nb_interventions_avant = Intervention.objects.all().count()
        # Supprime un `Role` non utilisé
        Role.objects.get(name=self.role_inutile_name).delete()
        # Vérifie qu'on a bien 1 `Role` de moins
        # et autant d'`Intervention` qu'au début
        nb_roles_apres = Role.objects.all().count()
        self.assertEqual(nb_roles_avant,
                         nb_roles_apres + 1)
        nb_interventions_apres = Intervention.objects.all().count()
        self.assertEqual(nb_interventions_avant,
                         nb_interventions_apres)
        with self.assertRaises(Role.DoesNotExist):
            Role.objects.get(name=self.role_inutile_name)
        # Supprime un `Role` utilisé par des `Episode`
        with self.assertRaises(ProtectedError):
            self.roleTest.delete()
        nb_roles_apres = Role.objects.all().count()
        self.assertEqual(nb_roles_avant,
                         nb_roles_apres + 1)
        nb_interventions_apres = Intervention.objects.all().count()
        self.assertEqual(nb_interventions_avant,
                         nb_interventions_apres)

    def test_Intervention_doublon(self):
        """
        S'assure qu'on ne peut pas associer deux fois le même
        `Intervenant' dans le même `Role` à un `Episode`.
        """
        with self.assertRaises(IntegrityError):
            # Association de intervenantTest dans le roleTest à episodeTest
            Intervention.objects.create(
                    episode=self.episodeTest,
                    intervenant=self.intervenantTest,
                    role=self.roleTest)


class EmissionTest(django.test.TestCase):
    """
    Test des objets de type `Emission`.
    """
    fixtures = ["Emission",
                "Group"]
    prefixeTest = "EmissionTest" + next(tempfile._get_candidate_names())
    emissionTest_nom = "Transmission"
    emissionTest_annee = 2017

    def test_Emission_str(self):
        """
        S'assure que la méthode str() de `Emission` fonctionne comme voulue.
        """
        emissionTest = Emission.objects.get(
                nom=self.emissionTest_nom,
                annee_scolaire=self.emissionTest_annee)
        self.assertEqual(str(emissionTest),
                         "{nom} ({annee1}-{annee2})".format(
                                 nom=self.emissionTest_nom,
                                 annee1=self.emissionTest_annee,
                                 annee2=self.emissionTest_annee + 1))

    def test_Emission_doublon_creation(self):
        """
        S'assure qu'on ne peut pas ajouter une `Emission` déjà existante.
        """
        with self.assertRaisesMessage(ValidationError,
                                      "existe déjà sur la période"):
            Emission.objects.create(nom=self.emissionTest_nom,
                                    annee_scolaire=self.emissionTest_annee)

    def test_Emission_doublon_modification(self):
        """
        S'assure qu'on ne peut pas modifier une `Emission`
        avec un nom d'`Emission` existant pour la même année
        (le champ année n'est pas éditable et ne peut pas être modifié).
        """
        emission = Emission.objects.create(
                                nom=self.prefixeTest,
                                annee_scolaire=self.emissionTest_annee)
        with self.assertRaisesMessage(ValidationError,
                                      "existe déjà sur la période"):
            emission.nom = self.emissionTest_nom
            emission.save()

    def test_Emission_existence(self):
        """
        S'assure que les `Emission` fournies existent dans
        la base et sont des `Emission`.
        """
        self.assertIsInstance(
                Emission.objects.get(nom=self.emissionTest_nom,
                                     annee_scolaire=self.emissionTest_annee),
                Emission)

    def test_Emission_inexistence(self):
        """
        S'assure qu'une exception est soulevée lorsqu'une
        `Emission` n'existe pas.
        """
        with self.assertRaises(Emission.DoesNotExist):
            Emission.objects.get(nom=self.prefixeTest)

    def test_Emission_insertion_suppression(self):
        """
        S'assure qu'insertions et suppressions d'`Emission` fonctionnent.
        Réalise l'insertion de NB_INSERTIONS `Emission`.
        Réalise une suppression des NB_INSERTIONS `Emissions` insérées.
        """
        nb_emissions = Emission.objects.all().count()
        emissions = [Emission(nom="{nom}-{suffixe}"
                              .format(nom=self.prefixeTest,
                                      suffixe=str(i)))
                     for i in range(NB_INSERTIONS)]
        for emissionTest in emissions:
            with self.subTest(emission=emissionTest):
                # l'`Emission` de la collection n'a pas de Primary Key
                # car pas enregistrée dans la base
                self.assertIsNone(emissionTest.pk)
                emissionTest.save()
                emissionTest.refresh_from_db()
                # et finalement l'`Emission` a une Primary Key
                # car sauvegardée en base
                self.assertIsNotNone(emissionTest.pk)
        emissions = Emission.objects.filter(
                nom__startswith=self.prefixeTest)
        for emissionTest in emissions:
            with self.subTest(emission=emissionTest):
                emissionTest.delete()
        # Vérifie qu'il y a bien le même nombre d'`Emission` à la fin
        # qu'au début du test
        self.assertEqual(Emission.objects.all().count(),
                         nb_emissions)


class EpisodeTest(django.test.TestCase):
    """
    Test des objets de type `Episode`.
    """
    fixtures = ["Diffusable",
                "ObjetSonore",
                "Licence",
                "Editeur",
                "Episode",
                "Emission",
                "Group",
                "SourceExterne",
                "RelationEpisodeSourceExterne",
                "MotClef",
                "RelationEpisodeMotClef"]
    prefixeTest = "EpisodeTest" + next(tempfile._get_candidate_names())
    # `Emission` contenant 2 `Episode` dont episodeTest et episodeDoublon
    emissionTest_nom = "Transmission"
    emissionTest_annee = 2017
    nb_episodes_emissionTest = 2
    # Autre `Emission' de la même année contenant episodeTest
    emissionDoublon_nom = "Numérique pour tous"
    # `Episode` faisant partie de emissionTest et emissionDoublon
    # de `SourceExterne` sourceExterneTest
    episodeTest_titre = "Conseils aux STS1 pour PPE3"
    # Autre `Episode` faisant partie de emissionTest
    episodeDoublon_titre = "Conseils aux STS2 pour PPE4"
    # sourceExterneTest est reliée à 2 `Episode` dont episodeTest
    # episodeTest utilise 2 `SourceExterne` dont sourceExterneTest
    sourceExterneTest_pk = 1
    nb_episodes_sourceExterneTest = 2
    nb_sourcesExternes_episodeTest = 2
    # `SourceExterne` inutilisée dans les fixtures
    sourceExterne_inutile_pk = 3
    # motClefTest est reliée à 2 `Episode` dont episodeTest
    # episodeTest utilise 2 `MotClef` dont motClefTest
    motClefTest_pk = 1
    nb_episodes_motClefTest = 2
    nb_motsClefs_episodeTest = 2
    # `MotClef` inutilisée dans les fixtures
    motClef_inutile_pk = 3

    def setUp(self):
        """
        Definit et réinitialise à chaque test les eipsodeTest et
        emissionTest utilisés dans chaque test.
        """
        self.emissionTest = Emission.objects.get(
                                    nom=self.emissionTest_nom,
                                    annee_scolaire=self.emissionTest_annee)
        self.episodeTest = Episode.objects.get(titre=self.episodeTest_titre,
                                               emission=self.emissionTest)
        self.sourceExterneTest = SourceExterne.objects.get(
                                                pk=self.sourceExterneTest_pk)
        self.motClefTest = MotClef.objects.get(pk=self.motClefTest_pk)

    def test_Episode_doublon_creation(self):
        """
        S'assure que l'on ne peut pas ajouter un `Episode` déjà existant
        dans la même `Emission`.
        """
        nb_episodes_avant = Diffusable.objects.filter(
                                        titre=self.episodeTest_titre).count()
        with self.assertRaises(ValidationError):
            Episode.objects.create(
                titre=self.episodeTest_titre,
                date_publication=datetime.date.today(),
                emission=self.emissionTest,
                editeur=Editeur.objects.first(),
                licence=Licence.objects.first())
        nb_episodes_apres = Diffusable.objects.filter(
                                        titre=self.episodeTest_titre).count()
        self.assertEqual(nb_episodes_avant, nb_episodes_apres)

    def test_Episode_doublon_modification_nom(self):
        """
        S'assure que l'on ne peut pas modifier un `Episode` en donnant
        le titre d'un `Episode` déjà existant dans la même `Emission`.
        """
        with self.assertRaises(ValidationError):
            self.episodeTest.titre = self.episodeDoublon_titre
            self.episodeTest.save()

    def test_Episode_doublon_modification_emission(self):
        """
        S'assure que l'on ne peut pas modifier un `Episode`
        en l'associant à une `Emission` possédant un
        `Episode` du même nom.
        """
        with self.assertRaises(ValidationError):
            self.episodeTest.emission = Emission.objects.get(
                                        nom=self.emissionDoublon_nom,
                                        annee_scolaire=self.emissionTest_annee)
            self.episodeTest.save()

    def test_Episode_existence(self):
        """
        S'assure que les `Episode` fournis existent dans
        la base et sont des `Episode`.
        """
        episodes = Episode.objects.all()
        for episodeTest in episodes:
            with self.subTest(episode=episodeTest):
                self.assertIsInstance(Episode.objects.filter(
                                                titre=episodeTest).first(),
                                      Episode)

    def test_Episode_inexistence(self):
        """
        S'assure qu'une exception est soulevée
        lorsqu'un `Episode` n'existe pas.
        """
        with self.assertRaises(Episode.DoesNotExist):
            Episode.objects.get(titre=self.prefixeTest)

    def test_Episode_insertion_suppression(self):
        """
        S'assure qu'insertions et suppressions d'`Episode` fonctionnent.
        Réalise l'insertion de NB_INSERTIONS `Episode` ayant pour `Emission`
        emissionTest.
        Effectue une modification de l'`Emission` associé pour la moitié des
        `Episode` insérés.
        Réalise enfin une suppression des NB_INSERTIONS `Episode` insérés.
        """
        nb_episodes = Episode.objects.all().count()
        episodes = [Episode(titre="{titre}-{suffixe}"
                                  .format(titre=self.prefixeTest,
                                          suffixe=str(i)),
                            date_publication=datetime.date.today(),
                            emission=self.emissionTest,
                            editeur=Editeur.objects.first(),
                            licence=Licence.objects.first())
                    for i in range(NB_INSERTIONS)]
        for episodeTest in episodes:
            with self.subTest(episode=episodeTest):
                # l'`Episode` courant de la collection n'a pas de Primary Key
                # car pas enregistré en base
                self.assertIsNone(episodeTest.pk)
                episodeTest.save()
                episodeTest.refresh_from_db()
                # et finalement l'`Episode` a une Primary Key
                # car sauvegardé en base
                self.assertIsNotNone(episodeTest.pk)
                if episodeTest.pk % 2 == 0:
                    episodeTest.licence = Licence.objects.last()
                    episodeTest.save()
                    episodeTest.refresh_from_db()
                    self.assertEqual(episodeTest.licence.acronyme,
                                     Licence.objects.last().acronyme)
        episodes = Episode.objects.filter(titre__startswith=self.prefixeTest)
        for episodeTest in episodes:
            with self.subTest(episode=episodeTest):
                episodeTest.delete()
        # Vérifie qu'il y a bien le même nombre de tuples à la fin
        # qu'au début du test
        self.assertEqual(Episode.objects.all().count(),
                         nb_episodes)

    def test_Episode_Emission_related(self):
        """
        S'assure que l'`Episode` fait partie de l'ensemble des `Episode`
        de l'`Emission` auquel il est associé.
        """
        self.assertEqual(len(self.emissionTest.episode_set_emission.all()),
                         self.nb_episodes_emissionTest)
        self.assertIn(self.episodeTest,
                      self.emissionTest.episode_set_emission.all())

    def test_Emission_suppression(self):
        """
        Vérifier qu'on ne peut pas supprimer une `Emission`
        possédant des `Episode`.
        """
        nb_emissions_avant = Emission.objects.all().count()
        nb_episodes_avant = Episode.objects.all().count()
        # Supprime une `Emission` contenant des `Episode`
        with self.assertRaises(ProtectedError):
            self.emissionTest.delete()
        # Vérifie que l'`Emission` n'a pas été supprimée
        nb_emissions_apres = Emission.objects.all().count()
        self.assertEqual(nb_emissions_avant,
                         nb_emissions_apres)
        nb_episodes_apres = Episode.objects.all().count()
        self.assertEqual(nb_episodes_avant,
                         nb_episodes_apres)

    def test_Episode_SourceExterne_related(self):
        """
        S'assure que l'`Episode` fait partie de l'ensemble des `Episode`
        de la `SourceExterne` auquel il est associé.
        """
        self.assertEqual(len(self.sourceExterneTest.episode_set.all()),
                         self.nb_episodes_sourceExterneTest)
        self.assertIn(self.episodeTest,
                      self.sourceExterneTest.episode_set.all())

    def test_SourceExterne_Episode_related(self):
        """
        S'assure que la `SourceExterne` fait partie de l'ensemble des
        `SourceExterne` de l'`Episode` auquel elle est associée.
        """
        self.assertEqual(len(self.episodeTest.sources_externes.all()),
                         self.nb_sourcesExternes_episodeTest)
        self.assertIn(self.sourceExterneTest,
                      self.episodeTest.sources_externes.all())

    def test_SourceExterne_suppression(self):
        """
        S'assure qu'on peut supprimer une `SourceExterne` non utilisée
        mais qu'on ne peut pas supprimer une `SourceExterne` utilisée
        par un `Episode`.
        """
        nb_sourcesExternes_avant = SourceExterne.objects.all().count()
        nb_episodes_avant = Episode.objects.all().count()
        # Supprime une `SourceExterne` non utilisée
        SourceExterne.objects.get(pk=self.sourceExterne_inutile_pk).delete()
        # Vérifie qu'on a bien 1 `SourceExterne` de moins
        # et autant d'`Episode` qu'au début
        nb_sourcesExternes_apres = SourceExterne.objects.all().count()
        self.assertEqual(nb_sourcesExternes_avant,
                         nb_sourcesExternes_apres + 1)
        nb_episodes_apres = Episode.objects.all().count()
        self.assertEqual(nb_episodes_avant,
                         nb_episodes_apres)
        with self.assertRaises(SourceExterne.DoesNotExist):
            SourceExterne.objects.get(pk=self.sourceExterne_inutile_pk)
        # Supprime une `SourceExterne` utilisée par des `Episode`
        with self.assertRaises(ProtectedError):
            self.sourceExterneTest.delete()
        # Vérifie qu'on a bien 1 `SourceExterne` de moins
        # et autant d'`Episode` qu'au début
        nb_sourcesExternes_apres = SourceExterne.objects.all().count()
        self.assertEqual(nb_sourcesExternes_avant,
                         nb_sourcesExternes_apres + 1)
        nb_episodes_apres = Episode.objects.all().count()
        self.assertEqual(nb_episodes_avant,
                         nb_episodes_apres)

    def test_SourceExterne_Episode_suppression(self):
        """
        S'assure que lors de la suppression d'un `Episode` les
        `SouceExterne' utilisées ne sont pas supprimées mais que
        les `RelationEpisodeSourceExterne` le sont.
        """
        nb_sourcesExternes_avant = SourceExterne.objects.all().count()
        nb_episodes_avant = Episode.objects.all().count()
        nb_relations_avant = RelationEpisodeSourceExterne.objects.all().count()
        # Supprime episodeTest utilisant 2 `SourceExterne`
        self.episodeTest.delete()
        nb_sourcesExternes_apres = SourceExterne.objects.all().count()
        nb_episodes_apres = Episode.objects.all().count()
        nb_relations_apres = RelationEpisodeSourceExterne.objects.all().count()
        self.assertEqual(nb_sourcesExternes_avant,
                         nb_sourcesExternes_apres)
        self.assertEqual(nb_episodes_avant,
                         nb_episodes_apres + 1)
        self.assertEqual(
                nb_relations_avant,
                nb_relations_apres + self.nb_episodes_sourceExterneTest)

    def test_SourceExterne_Episode_doublon(self):
        """
        S'assure qu'on ne peut pas associer deux fois la même
        `SouceExterne' à un `Episode`.
        """
        with self.assertRaises(IntegrityError):
            # Association de sourceExterneTest à episodeTest
            RelationEpisodeSourceExterne.objects.create(
                    episode=self.episodeTest,
                    source_externe=self.sourceExterneTest)

    def test_Episode_MotClef_related(self):
        """
        S'assure que l'`Episode` fait partie de l'ensemble des `Episode`
        du `MotClef` auquel il est associé.
        """
        self.assertEqual(len(self.motClefTest.episode_set.all()),
                         self.nb_episodes_motClefTest)
        self.assertIn(self.episodeTest,
                      self.motClefTest.episode_set.all())

    def test_MotClef_Episode_related(self):
        """
        S'assure que le `MotClef` fait partie de l'ensemble des
        `MotClef` de l'`Episode` auquel il est associé.
        """
        self.assertEqual(len(self.episodeTest.mots_clefs.all()),
                         self.nb_motsClefs_episodeTest)
        self.assertIn(self.motClefTest,
                      self.episodeTest.mots_clefs.all())

    def test_MotClef_suppression(self):
        """
        S'assure qu'on peut supprimer un `MotClef` non utilisé
        mais qu'on ne peut pas supprimer un `MotClef` utilisé
        par un `Episode`.
        """
        nb_motsClefs_avant = MotClef.objects.all().count()
        nb_episodes_avant = Episode.objects.all().count()
        # Supprime un `MotClef` non utilisé
        MotClef.objects.get(pk=self.motClef_inutile_pk).delete()
        # Vérifie qu'on a bien 1 `MotClef` de moins
        # et autant d'`Episode` qu'au début
        nb_motsClefs_apres = MotClef.objects.all().count()
        self.assertEqual(nb_motsClefs_avant,
                         nb_motsClefs_apres + 1)
        nb_episodes_apres = Episode.objects.all().count()
        self.assertEqual(nb_episodes_avant,
                         nb_episodes_apres)
        with self.assertRaises(MotClef.DoesNotExist):
            MotClef.objects.get(pk=self.motClef_inutile_pk)
        # Supprime un `MotClef` utilisé par des `Episode`
        with self.assertRaises(ProtectedError):
            self.motClefTest.delete()
        # Vérifie qu'on a bien 1 `MotClef` de moins
        # et autant d'`Episode` qu'au début
        nb_motsClefs_apres = MotClef.objects.all().count()
        self.assertEqual(nb_motsClefs_avant,
                         nb_motsClefs_apres + 1)
        nb_episodes_apres = Episode.objects.all().count()
        self.assertEqual(nb_episodes_avant,
                         nb_episodes_apres)

    def test_MotClef_Episode_suppression(self):
        """
        S'assure que lors de la suppression d'un `Episode` les
        `MotClef' utilisés ne sont pas supprimés mais que
        les `RelationEpisodeMotClef` le sont.
        """
        nb_motsClefs_avant = MotClef.objects.all().count()
        nb_episodes_avant = Episode.objects.all().count()
        nb_relations_avant = RelationEpisodeMotClef.objects.all().count()
        # Supprime episodeTest utilisant 2 `MotClef`
        self.episodeTest.delete()
        nb_motsClefs_apres = MotClef.objects.all().count()
        nb_episodes_apres = Episode.objects.all().count()
        nb_relations_apres = RelationEpisodeMotClef.objects.all().count()
        self.assertEqual(nb_motsClefs_avant,
                         nb_motsClefs_apres)
        self.assertEqual(nb_episodes_avant,
                         nb_episodes_apres + 1)
        self.assertEqual(
                nb_relations_avant,
                nb_relations_apres + self.nb_episodes_motClefTest)

    def test_MotClef_Episode_doublon(self):
        """
        S'assure qu'on ne peut pas associer deux fois le même
        `MotClef' à un `Episode`.
        """
        with self.assertRaises(IntegrityError):
            # Association de motClefTest à episodeTest
            RelationEpisodeMotClef.objects.create(
                    episode=self.episodeTest,
                    mot_clef=self.motClefTest)


class ImageEmissionTest(django.test.TestCase):
    """
    Tests de la gestion des images pour les `Emission`.
    """
    # Préfixe pour tous les nom de fichiers test
    prefixeTest = next(tempfile._get_candidate_names())
    emissionTest_nom = "EmissionTest" + prefixeTest
    # Tailles, modes et formats testés (il faut au moins 3 choix minimum
    # par atribut pour la fonction "test_nom_fichier_image")
    sizesTest = [(200, 500), (1500, 500), (500, 1500), (1500, 1500)]
    modesTest = ["1", "L", "RGB"]
    formatsTest = ["JPEG", "PNG", "TIFF"]
    # Tailles, mode et format attendus des images après traitement
    expected_size_max = settings.IMAGE_CONFIG["max_size"]
    expected_mode = settings.IMAGE_CONFIG["mode"]
    expected_format = settings.IMAGE_CONFIG["format"]
    # Dossier où sont stockées les images
    dossier_image = os.path.join(settings.MEDIA_ROOT,
                                 settings.IMAGE_DIR,
                                 "Emission")
    # Dossier temporaire pour fabriquer les images
    dossier_tmp = os.path.join(settings.MEDIA_ROOT,
                               prefixeTest)

    @classmethod
    def setUpClass(cls):
        """
        Initialisation de la classe. Création du dossier temporaire.
        """
        super().setUpClass()
        os.makedirs(cls.dossier_tmp, exist_ok=True)

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage du dossier MEDIA_ROOT de tous les fichiers de test.
        """
        # Supprime tous les fichiers image créés dans les tests
        liste_fichiers_image = os.listdir(cls.dossier_image)
        for nom_fichier in liste_fichiers_image:
            if nom_fichier.find(cls.emissionTest_nom) != -1:
                fichier_path = os.path.join(cls.dossier_image, nom_fichier)
                os.remove(fichier_path)
        # Supprime le dossier temporaire après l'avoir éventuellement vidé
        liste_fichiers_tmp = os.listdir(cls.dossier_tmp)
        for nom_fichier in liste_fichiers_tmp:
            fichier_path = os.path.join(cls.dossier_tmp, nom_fichier)
            os.remove(fichier_path)
        os.rmdir(cls.dossier_tmp)
        super().tearDownClass()

    def test_traitement_image(self):
        """
        Tests de la fonction traitement_image() pour les `Emission`:
        Vérifie que le fichier image est correctement nommé.
        Vérifie que le mode est toujours settings.IMAGE_CONFIG["mode"].
        Vérifie que les dimensions finales sont inférieures
        à settings.IMAGE_CONFIG["max_size"]
        et que le ratio est bien conservé.
        """
        # On teste traitement_image() pour toutes les tailles, modes
        # et formats prévus
        for s in range(len(self.sizesTest)):
            for m in range(len(self.modesTest)):
                for f in range(len(self.formatsTest)):
                    # Création d'une image noire avec les attributs désirés
                    imagePIL = Image.new(self.modesTest[m],
                                         self.sizesTest[s])
                    image_path = os.path.join(
                                        self.dossier_tmp,
                                        next(tempfile._get_candidate_names()))
                    # Enregistrement de l'image au format désiré
                    imagePIL.save(image_path, format=self.formatsTest[f])
                    # Création d'une `Emission` utilisant l'image
                    emissionTest = Emission.objects.create(
                                        nom="{nom}-{num_size}-{num_mode}"
                                            "-{num_format}"
                                            .format(nom=self.emissionTest_nom,
                                                    num_size=s,
                                                    num_mode=m,
                                                    num_format=f),
                                        image=image_path)
                    # Ouverture le l'image après son traitement
                    image_emissionTest = Image.open(emissionTest.image.file)
                    # Véification du mode, du format et des dimensions
                    self.assertEqual(image_emissionTest.mode,
                                     self.expected_mode)
                    self.assertEqual(image_emissionTest.format,
                                     self.expected_format)
                    self.assertLessEqual(image_emissionTest.width,
                                         self.expected_size_max[0])
                    self.assertLessEqual(image_emissionTest.height,
                                         self.expected_size_max[1])
                    # Vérification que le ratio de l'image est conservé
                    ratioTest = self.sizesTest[s][0] / self.sizesTest[s][1]
                    largeur = image_emissionTest.width
                    hauteur = image_emissionTest.height
                    ratio_image = largeur / hauteur
                    self.assertEqual(round(ratioTest, 1),
                                     round(ratio_image, 1))
                    # Vérification que le dossier tmp est vide
                    self.assertEqual(len(os.listdir(self.dossier_tmp)),
                                     0)

    def test_nom_fichier_image(self):
        """
        Vérifie que le fichier image est bien géré lors d'une création
        ou une modification d'une `Emission` (vérification existence ou non
        du fichier et vérification qu'il est toujours bien nommé).
        """
        def nouveau_fichier_image(num):
            # Création d'une image noire avec les attributs définis par
            # le paramètre num
            imagePIL = Image.new(self.modesTest[num],
                                 self.sizesTest[num])
            image_path = os.path.join(self.dossier_tmp,
                                      next(tempfile._get_candidate_names()))
            # Enregistrement de l'image au format désiré
            imagePIL.save(image_path, format=self.formatsTest[num])
            return image_path

        # Création d'une `Emission` utilisant une image
        num = 0
        emissionTest = Emission.objects.create(
                                nom="{nom}-{num}-{num}-{num}"
                                    .format(nom=self.emissionTest_nom,
                                            num=num),
                                image=nouveau_fichier_image(num))
        # Vérification que le fichier image existe et que le fichier
        # initial a bien été supprimé
        expected_name = os.path.join(
                self.dossier_image,
                "{nom}.{extension}"
                .format(nom=str(emissionTest),
                        extension=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_tmp)),
                         0)
        # Modification de l'image de l'`Emission`
        num += 1
        emissionTest.image = nouveau_fichier_image(num)
        emissionTest.save()
        # Vérification que l'image a bien été remplacée (ratio différent)
        # que le fichier image existe et que le fichier initial a été supprimé
        image_emissionTest = Image.open(emissionTest.image.file)
        ratio_image = image_emissionTest.width / image_emissionTest.height
        expected_ratio = self.sizesTest[num][0] / self.sizesTest[num][1]
        self.assertEqual(round(ratio_image, 1),
                         round(expected_ratio, 1))
        expected_name = os.path.join(
                self.dossier_image,
                "{nom}.{extension}"
                .format(nom=str(emissionTest),
                        extension=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_tmp)),
                         0)
        # Modification du nom de l'`Emission`
        nouveau_emissionTest_nom = self.emissionTest_nom + "-test"
        emissionTest.nom = nouveau_emissionTest_nom
        emissionTest.save()
        # Vérification que le nom du fichier a bien changé
        # et que l'ancien fichier image n'existe plus
        self.assertFalse(os.path.isfile(expected_name))
        expected_name = os.path.join(
                self.dossier_image,
                "{nom}.{extension}"
                .format(nom=str(emissionTest),
                        extension=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        # Suppression de l'image de l'`Emission`
        emissionTest.image = ""
        emissionTest.save()
        # Vérification que le fichier image a bien été supprimé
        self.assertFalse(os.path.isfile(expected_name))
        # Ajout d'une nouvelle image pour l'`Emission`
        num += 1
        emissionTest.image = nouveau_fichier_image(num)
        emissionTest.save()
        # Vérification que le fichier image existe et que le fichier
        # initial a bien été supprimé
        self.assertTrue(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_tmp)),
                         0)

    def test_suppression_Emission(self):
        """
        Vérifie que le fichier image est bien supprimé du système
        de stockage lors de la suppression d'une `Emission`.
        """
        # Création d'une image noire avec les attributs désirés
        imagePIL = Image.new(self.modesTest[0],
                             self.sizesTest[0])
        image_path = os.path.join(
                            self.dossier_tmp,
                            next(tempfile._get_candidate_names()))
        # Enregistrement de l'image au format désiré
        imagePIL.save(image_path, format=self.formatsTest[0])
        # Création d'une `Emission` utilisant l'image
        emissionTest = Emission.objects.create(
                nom="{nom}-{suffixe}".format(nom=self.emissionTest_nom,
                                             suffixe=self.prefixeTest),
                image=image_path)
        # Vérification que le fichier image existe
        expected_name = os.path.join(
                self.dossier_image,
                "{nom}.{extension}"
                .format(nom=str(emissionTest),
                        extension=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        nombre_fichier_image = len(os.listdir(self.dossier_image))
        emissionTest.delete()
        # Vérification que le fichier image a été supprimé
        self.assertFalse(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_image)),
                         nombre_fichier_image - 1)


class ImageEpisodeTest(django.test.TestCase):
    """
    Tests de la gestion des images pour les `Episode`.
    """
    fixtures = ["Licence",
                "Editeur",
                "Emission"]
    # Références des fixtures utilisées dans les tests
    emissionTest_nom = "Transmission"
    emissionTest_annee = 2017
    # Préfixe pour tous les nom de fichiers test
    prefixeTest = next(tempfile._get_candidate_names())
    episodeTest_titre = "EpisodeTest" + prefixeTest
    # Tailles, modes et formats testés (il faut au moins 3 choix minimum
    # par atribut pour la fonction "test_nom_fichier_image")
    sizesTest = [(200, 500), (1500, 500), (500, 1500), (1500, 1500)]
    modesTest = ["1", "L", "RGB"]
    formatsTest = ["JPEG", "PNG", "TIFF"]
    # Tailles, mode et format attendus des images après traitement
    expected_size_max = settings.IMAGE_CONFIG["max_size"]
    expected_mode = settings.IMAGE_CONFIG["mode"]
    expected_format = settings.IMAGE_CONFIG["format"]
    # Dossier où sont stockées les images
    dossier_image = os.path.join(settings.MEDIA_ROOT,
                                 settings.IMAGE_DIR,
                                 "Emission")
    # Dossier temporaire pour fabriquer les images
    dossier_tmp = os.path.join(settings.MEDIA_ROOT,
                               prefixeTest)

    @classmethod
    def setUpClass(cls):
        """
        Initialisation de la classe. Création du dossier temporaire.
        """
        super().setUpClass()
        os.makedirs(cls.dossier_tmp, exist_ok=True)

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage du dossier MEDIA_ROOT de tous les fichiers de test.
        """
        # Supprime tous les fichiers image créés dans les tests
        liste_fichiers_image = os.listdir(cls.dossier_image)
        for nom_fichier in liste_fichiers_image:
            if nom_fichier.find(cls.episodeTest_titre) != -1:
                fichier_path = os.path.join(cls.dossier_image, nom_fichier)
                os.remove(fichier_path)
        # Supprime le dossier temporaire après l'avoir éventuellement vidé
        liste_fichiers_tmp = os.listdir(cls.dossier_tmp)
        for nom_fichier in liste_fichiers_tmp:
            fichier_path = os.path.join(cls.dossier_tmp, nom_fichier)
            os.remove(fichier_path)
        os.rmdir(cls.dossier_tmp)
        super().tearDownClass()

    def setUp(self):
        """
        Definit les emissionTest utilisés dans chaque test.
        """
        self.emissionTest = Emission.objects.get(
                                    nom=self.emissionTest_nom,
                                    annee_scolaire=self.emissionTest_annee)

    def test_traitement_image(self):
        """
        Tests de la fonction traitement_image() pour les `Episode`:
        Vérifie que le fichier image est correctement nommé.
        Vérifie que le mode est toujours settings.IMAGE_CONFIG["mode"].
        Vérifie que les dimensions finales sont inférieures
        à settings.IMAGE_CONFIG["max_size"]
        et que le ratio est bien conservé.
        """
        # On teste traitement_image() pour toutes les tailles, modes
        # et formats prévus
        for s in range(len(self.sizesTest)):
            for m in range(len(self.modesTest)):
                for f in range(len(self.formatsTest)):
                    # Création d'une image noire avec les attributs désirés
                    imagePIL = Image.new(self.modesTest[m],
                                         self.sizesTest[s])
                    image_path = os.path.join(
                                        self.dossier_tmp,
                                        next(tempfile._get_candidate_names()))
                    # Enregistrement de l'image au format désiré
                    imagePIL.save(image_path, format=self.formatsTest[f])
                    # Création d'un `Episode` utilisant l'image
                    episodeTest = Episode.objects.create(
                                titre="{titre}-{num_size}-{num_mode}"
                                      "-{num_format}"
                                      .format(titre=self.episodeTest_titre,
                                              num_size=s,
                                              num_mode=m,
                                              num_format=f),
                                date_publication=datetime.date.today(),
                                emission=self.emissionTest,
                                editeur=Editeur.objects.first(),
                                licence=Licence.objects.first(),
                                image=image_path)
                    # Ouverture le l'image après son traitement
                    image_episodeTest = Image.open(episodeTest.image.file)
                    # Véification du mode, du format et des dimensions
                    self.assertEqual(image_episodeTest.mode,
                                     self.expected_mode)
                    self.assertEqual(image_episodeTest.format,
                                     self.expected_format)
                    self.assertLessEqual(image_episodeTest.width,
                                         self.expected_size_max[0])
                    self.assertLessEqual(image_episodeTest.height,
                                         self.expected_size_max[1])
                    # Vérification que le ratio de l'image est conservé
                    ratioTest = self.sizesTest[s][0] / self.sizesTest[s][1]
                    largeur = image_episodeTest.width
                    hauteur = image_episodeTest.height
                    ratio_image = largeur / hauteur
                    self.assertEqual(round(ratioTest, 1),
                                     round(ratio_image, 1))
                    # Vérification que le dossier tmp est vide
                    self.assertEqual(len(os.listdir(self.dossier_tmp)),
                                     0)

    def test_nom_fichier_image(self):
        """
        Vérifie que le fichier image est bien géré lors d'une création
        ou une modification d'un `Episode` (vérification existence ou non
        du fichier et vérification qu'il est toujours bien nommé).
        """
        def nouveau_fichier_image(num):
            # Création d'une image noire avec les attributs définis par
            # le paramètre num
            imagePIL = Image.new(self.modesTest[num],
                                 self.sizesTest[num])
            image_path = os.path.join(self.dossier_tmp,
                                      next(tempfile._get_candidate_names()))
            # Enregistrement de l'image au format désiré
            imagePIL.save(image_path, format=self.formatsTest[num])
            return image_path

        # Création d'un `Episode` utilisant une image
        num = 0
        episodeTest = Episode.objects.create(
                                titre="{titre}-{num}-{num}-{num}"
                                .format(titre=self.episodeTest_titre,
                                        num=num),
                                date_publication=datetime.date.today(),
                                emission=self.emissionTest,
                                editeur=Editeur.objects.first(),
                                licence=Licence.objects.first(),
                                image=nouveau_fichier_image(num))
        # Vérification que le fichier image existe et que le fichier
        # initial a bien été supprimé
        expected_name = os.path.join(
                self.dossier_image,
                "{nom_emission}-{nom_episode}.{extension}"
                .format(nom_emission=str(self.emissionTest),
                        nom_episode=episodeTest.titre,
                        extension=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_tmp)),
                         0)
        # Modification de l'image de l'`Episode`
        num += 1
        episodeTest.image = nouveau_fichier_image(num)
        episodeTest.save()
        # Vérification que l'image a bien été remplacée (ratio différent)
        # que le fichier image existe et que le fichier initial a été supprimé
        image_episodeTest = Image.open(episodeTest.image.file)
        ratio_image = image_episodeTest.width / image_episodeTest.height
        expected_ratio = self.sizesTest[num][0] / self.sizesTest[num][1]
        self.assertEqual(round(ratio_image, 1),
                         round(expected_ratio, 1))
        expected_name = os.path.join(
                self.dossier_image,
                "{nom_emission}-{nom_episode}.{extension}"
                .format(nom_emission=str(self.emissionTest),
                        nom_episode=episodeTest.titre,
                        extension=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_tmp)),
                         0)
        # Modification du nom de l'`Episode`
        episodeTest.titre = self.episodeTest_titre + "-test"
        episodeTest.save()
        # Vérification que le nom du fichier a bien changé
        # et que l'ancien fichier image n'existe plus
        self.assertFalse(os.path.isfile(expected_name))
        expected_name = os.path.join(
                self.dossier_image,
                "{nom_emission}-{nom_episode}.{extension}"
                .format(nom_emission=str(self.emissionTest),
                        nom_episode=episodeTest.titre,
                        extension=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        # Suppression de l'image de l'`Episode`
        episodeTest.image = ""
        episodeTest.save()
        # Vérification que le fichier image a bien été supprimé
        self.assertFalse(os.path.isfile(expected_name))
        # Ajout d'une nouvelle image pour l'`Episode`
        num += 1
        episodeTest.image = nouveau_fichier_image(num)
        episodeTest.save()
        # Vérification que le fichier image existe et que le fichier
        # initial a bien été supprimé
        self.assertTrue(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_tmp)),
                         0)

    def test_suppression_Emission(self):
        """
        Vérifie que le fichier image est bien supprimé du système
        de stockage lors de la suppression d'un `Episode`.
        """
        # Création d'une image noire avec les attributs désirés
        imagePIL = Image.new(self.modesTest[0],
                             self.sizesTest[0])
        image_path = os.path.join(
                            self.dossier_tmp,
                            next(tempfile._get_candidate_names()))
        # Enregistrement de l'image au format désiré
        imagePIL.save(image_path, format=self.formatsTest[0])
        # Création d'un `Episode` avec l'image
        episodeTest = Episode.objects.create(
                            titre="{nom}-{suffixe}"
                                  .format(nom=self.emissionTest_nom,
                                          suffixe=self.prefixeTest),
                            date_publication=datetime.date.today(),
                            emission=self.emissionTest,
                            editeur=Editeur.objects.first(),
                            licence=Licence.objects.first(),
                            image=image_path)
        # Vérification que le fichier image existe
        expected_name = os.path.join(
                self.dossier_image,
                "{nom_episode}-{nom_emission}.{extension}"
                .format(nom_episode=str(episodeTest.emission),
                        nom_emission=episodeTest.titre,
                        extension=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        nombre_fichier_image = len(os.listdir(self.dossier_image))
        episodeTest.delete()
        # Vérification que le fichier image a été supprimé
        self.assertFalse(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_image)),
                         nombre_fichier_image - 1)
