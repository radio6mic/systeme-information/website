# -*- coding: utf-8 -*-

"""
Définition de la configuration de l'application `gestion_des_emissions`.
"""

from django.apps import AppConfig


class GestionDesEmissionsConfig(AppConfig):
    """
    Configuration de l'application 'gestion_des_emissions'
    """
    name = "gestion_des_emissions"
    verbose_name = "gestion des émissions"
