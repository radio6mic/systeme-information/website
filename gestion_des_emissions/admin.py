# -*- coding: utf-8 -*-

"""
Définition de la configuration du site d'administration
pour la gestion des émissions.
"""

from django.contrib import admin, messages
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.utils.safestring import mark_safe
from django.utils.html import escape, format_html, format_html_join
from django.urls import reverse, resolve
from django.http import HttpResponseRedirect
from .models import (MotClef,
                     Intervenant,
                     Emission,
                     Episode,
                     SourceExterne,
                     Role,
                     Intervention)
from gestion_de_la_diffusion.models import Editeur
from gestion_de_la_diffusion.exceptions import LockedError
from website import settings


class InterventionPourIntervenantInline(admin.TabularInline):
    """
    Définition des attributs d'affichage Inline
    de la classe `Intervention` pour les `Intervenant`.
    """
    model = Intervention
    autocomplete_fields = ["episode"]
    extra = 0


class IntervenantAdmin(UserAdmin):
    """
    Définition des attributs d'administration
    de la classe `Intervenant`.
    """
    search_fields = ["first_name",
                     "last_name",
                     "username"]
    inlines = [InterventionPourIntervenantInline]


class SourceExterneAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `SourceExterne`.
    """
    fieldsets = [(None,
                  {"fields": ("titre",
                              "auteur",
                              "date_publication",
                              "editeur",
                              "url")}),
                 ("ÉPISODES",
                  {"fields": ("episodes_liste_html",),
                   "classes": ("collapse",),
                   "description": "Épisodes utilisant cette ressource."})]
    readonly_fields = ["episodes_liste_html"]
    ordering = ["-date_publication"]
    search_fields = ["titre",
                     "auteur"]

    def episodes_liste_html(self, instance):
        """
        Retourne le code html pour afficher la liste des `Episode` d'une
        `SourceExterne` avec un lien permettant d'accéder à la page de
        modification de chaque `Episode`.
        """
        episodes = instance.episode_set.all()
        liste_html = format_html_join(
                "\n",
                "<a href='{}'> <strong> {} </strong> </a> dans l'émission "
                "<strong>{}</strong>\n<br/>",
                (((reverse("admin:{app}_{model}_change"
                           .format(app=episode._meta.app_label,
                                   model=episode._meta.model_name),
                           args=[episode.pk])),
                  episode.titre,
                  episode.emission)
                 for episode in episodes))
        return format_html("\n{}\n",
                           liste_html)

    episodes_liste_html.short_description = "épisode"


class MotClefAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `MotClef`.
    """
    search_fields = ["mot_clef"]


class MotClefPourEpisodeInline(admin.TabularInline):
    """
    Définition des attributs d'affichage Inline
    de la classe `MotClef`.
    """
    model = Episode.mots_clefs.through
    classes = ["collapse"]
    verbose_name = "mot-clé"
    verbose_name_plural = "mots-clés"
    autocomplete_fields = ["mot_clef"]
    extra = 0


class SourceExternePourEpisodeInline(admin.TabularInline):
    """
    Définition des attributs d'affichage Inline
    de la classe `SourceExterne`.
    """
    model = Episode.sources_externes.through
    classes = ["collapse"]
    verbose_name = "source externe"
    verbose_name_plural = "sources externes"
    autocomplete_fields = ["source_externe"]
    extra = 0


class InterventionPourEpisodeInline(admin.TabularInline):
    """
    Définition des attributs d'affichage Inline
    de la classe `Intervention` pour les `Episode`.
    """
    model = Intervention
    autocomplete_fields = ["intervenant"]
    extra = 0


class EpisodeAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Episode`.
    """
    fieldsets = [(None,
                  {"fields": ("titre",
                              "emission",
                              "fichier",
                              "duree",
                              "display_image_html",
                              "image",
                              "date_publication",
                              "licence",
                              "editeur")}),
                 ("DATES DE PROGRAMMATION",
                  {"fields": ("programmations_liste_html",),
                   "classes": ("collapse",),
                   "description": "Dates de programmation de l'épisode ou "
                                  "des listes de lectures le contenant."}),
                 ("LISTES DE LECTURES",
                  {"fields": ("playlists_liste_html",),
                   "classes": ("collapse",),
                   "description": "Cet épisode fait partie des listes "
                                  "de lectures suivantes."}),
                 ("DESCRIPTION",
                  {"fields": ("description",),
                   "classes": ("collapse",)})]
    readonly_fields = ["duree",
                       "display_image_html",
                       "est_programme",
                       "programmations_liste_html",
                       "playlists_liste_html"]
    list_display = ("titre",
                    "emission",
                    "duree",
                    "date_publication",
                    "licence",
                    "fichier_disponible",
                    "est_programme")
    list_filter = ["date_publication",
                   "emission",
                   "licence"]
    date_hierarchy = "date_publication"
    search_fields = ["titre",
                     "emission__nom"]
    inlines = [MotClefPourEpisodeInline,
               SourceExternePourEpisodeInline,
               InterventionPourEpisodeInline]
    autocomplete_fields = ["emission"]

    def playlists_liste_html(self, instance):
        """
        Retourne le code html de la liste des `Playlist` contenant
        l'`Episode` avec un lien permettant d'accéder à la page de
        modification de chaque `Playlist`.
        """
        ordonners = instance.ordonner_set_diffusable.all()
        liste_html = format_html_join(
                "\n",
                "<a href='{}'> <strong> {} </strong> </a>  en position "
                "<strong>{}</strong>\n<br/>",
                (((reverse("admin:{app}_{model}_change"
                           .format(app=ordonner.playlist._meta.app_label,
                                   model=ordonner.playlist._meta.model_name),
                           args=[ordonner.playlist.pk])),
                  ordonner.playlist,
                  ordonner.position)
                 for ordonner in ordonners))
        return format_html("\n{}\n",
                           liste_html)

    playlists_liste_html.short_description = "listes de lecture"

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        Permet de n'avoir que l'`Editeur` "Lycée André Malraux"
        comme `Editeur` possible d'un `Episode`.
        """
        if db_field.name == "editeur":
            kwargs["queryset"] = Editeur.objects.filter(
                                    nom__icontains=settings.EDITEUR_INTERNE)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_changeform_initial_data(self, request):
        """
        Utilise par défaut l'`Editeur` EDITEUR_INTERNE comme `Editeur`
        d'un nouvel `Episode`.
        """
        initial = super().get_changeform_initial_data(request)
        editeurs = Editeur.objects.filter(
                            nom__icontains=settings.EDITEUR_INTERNE)
        if editeurs.exists():
            initial["editeur"] = editeurs.first().pk
        return initial

    def change_view(self, request, object_id, form_url="", extra_context=None):
        """
        Gestion de `LockedError` si on essaye de modifier un `Episode`
        qui est programmé : un message d'erreur est envoyé à l'utilisateur
        indiquant les `Programmation` verrouillant l'`Episode`.
        """
        try:
            return super().change_view(request,
                                       object_id,
                                       form_url,
                                       extra_context)
        except LockedError as erreur:
            message = escape(erreur.message)
            for obj in erreur.related_objects:
                message += "<br /><strong> {related_obj} </strong>" \
                           .format(related_obj=escape(obj))
            self.message_user(request, mark_safe(message), messages.ERROR)
            opts = self.model._meta
            return_url = reverse("admin:{app}_{model}_change".format(
                                                     app=opts.app_label,
                                                     model=opts.model_name),
                                 args=(object_id,),
                                 current_app=self.admin_site.name)
            return HttpResponseRedirect(return_url)

    def response_add(self, request, obj):
        """
        Gère la redirection de la page lors de l'enregistrement.
        Si on a accédé à la page à partir d'une `Emission' :
        - Si on enregistre, on revient à la page de l'`Emission`
          après l'enregistrement de l'`Episode`.
        - Si on enregistre et continue, on reste sur la même vue.
        - Si on enregistre et ajoute un nouveau, le champ de l'`Emission`
          est pré-rempli dans la vue d'ajout.
        """
        opts = self.model._meta
        emission_pk = None
        if "emission" in request.GET:
            emission_pk = request.GET["emission"]
        if emission_pk:
            message = format_html("L'épisode <strong> {titre} </strong> "
                                  "à été ajouté avec succès.",
                                  titre=str(obj))
            if "_addanother" in request.POST:
                message += escape(" Vous pouvez en ajouter un nouveau.")
                url_retour = reverse("admin:{app}_{model}_add".format(
                                                     app=opts.app_label,
                                                     model=opts.model_name),
                                     current_app=self.admin_site.name)
                url_retour += "?emission={pk}".format(pk=emission_pk)
            elif "_continue" in request.POST:
                message += escape(" Vous pouvez continuer les modifications.")
                url_retour = reverse("admin:{app}_{model}_change".format(
                                                     app=opts.app_label,
                                                     model=opts.model_name),
                                     args=(obj.pk,),
                                     current_app=self.admin_site.name)
                url_retour += "?emission={pk}".format(pk=emission_pk)
            else:
                url_retour = reverse("admin:{app}_emission_change".format(
                                                         app=opts.app_label),
                                     args=(emission_pk,),
                                     current_app=self.admin_site.name)
            self.message_user(request, message, messages.SUCCESS)
            return HttpResponseRedirect(url_retour)
        return super().response_add(request, obj)

    def response_change(self, request, obj):
        """
        Gère la redirection de la page lors de l'enregistrement.
        Si on a accédé à la page à partir d'une `Emission' :
        - Si on enregistre, on revient à la page de l'`Emission`
          après l'enregistrement de l'`Episode`.
        - Si on enregistre et continue, on reste sur la même vue.
        - Si on enregistre et ajoute un nouveau, le champ de l'`Emission`
          est pré-rempli dans la vue d'ajout.
        """
        opts = self.model._meta
        emission_pk = None
        if "emission" in request.GET:
            emission_pk = request.GET["emission"]
        if emission_pk:
            message = format_html("L'épisode <strong> {titre} </strong> "
                                  "à été modifié avec succès.",
                                  titre=str(obj))
            if "_addanother" in request.POST:
                message += escape(" Vous pouvez en ajouter un nouveau.")
                url_retour = reverse("admin:{app}_{model}_add".format(
                                                     app=opts.app_label,
                                                     model=opts.model_name),
                                     current_app=self.admin_site.name)
                url_retour += "?emission={pk}".format(pk=emission_pk)
            elif "_continue" in request.POST:
                message += escape(" Vous pouvez continuer les modifications.")
                url_retour = request.path
                url_retour += "?emission={pk}".format(pk=emission_pk)
            else:
                url_retour = reverse("admin:{app}_emission_change".format(
                                                         app=opts.app_label),
                                     args=(emission_pk,),
                                     current_app=self.admin_site.name)
            self.message_user(request, message, messages.SUCCESS)
            return HttpResponseRedirect(url_retour)
        return super().response_change(request, obj)


class EpisodePourEmissionInline(admin.TabularInline):
    """
    Définition des attributs d'affichage Inline
    de la classe `Episode` pour les `Emission`.
    """
    model = Episode
    fields = ["champ_inline", "est_programme"]
    readonly_fields = ["champ_inline", "est_programme"]
    verbose_name = ""
    verbose_name_plural = "épisodes faisant partie de cette émission"
    emission_pk = None
    extra = 0

    def get_extra(self, request, obj=None, **kwargs):
        """
        Méthode permettant de définir le nombre de formulaires vides ajoutés
        par défaut.
        Permet ici en plus de récupérer le pk de l'`Emission`.
        """
        # Retourne tous les paramètres pouvant être obtenus à partir
        # le l'url de la vue
        infos = resolve(request.path_info)
        # Teste si l'`Emission` a été enregistrée en base
        if "object_id" in infos.kwargs:
            self.emission_pk = infos.kwargs["object_id"]
            self.max_num = None  # Affiche tous les sous-formulaires
        else:
            self.max_num = 0  # N'affiche aucun sous-formulaires
        extra = 0  # N'affiche aucun nouveau sous-formulaire vide
        return extra

    def champ_inline(self, instance):
        """
        Affiche le nom de l'`Episode` s'il existe.
        Affiche un lien pour créer un nouvel `Episode` avec le
        champ `Emission` prérempli sinon.
        Ajoute les paramètres emission (id de l'`Emission`) pour pouvoir
        retourner sur la page de modification de l'`Emission` après avoir
        enregistré l'`Episode`.
        """
        opts = self.model._meta
        if instance.pk is None:
            url_cible = reverse("admin:{app}_{model}_add".format(
                                                     app=opts.app_label,
                                                     model=opts.model_name),
                                current_app=self.admin_site.name)
            valeur = format_html(
                        "\n<a href='{url_cible}?emission={pk}'>"
                        "Ajouter un épisode</a>\n",
                        url_cible=url_cible,
                        pk=self.emission_pk)
        else:
            url_cible = reverse("admin:{app}_{model}_change".format(
                                                     app=opts.app_label,
                                                     model=opts.model_name),
                                args=(instance.pk,),
                                current_app=self.admin_site.name)
            valeur = format_html(
                        "\n<a href='{url_cible}?emission={pk}'"
                        " title='Modifier l&apos;épisode'> <strong> {titre} "
                        "</strong> </a>  (durée : {duree})\n",
                        url_cible=url_cible,
                        pk=self.emission_pk,
                        titre=str(instance),
                        duree=instance.duree)
        return valeur

    champ_inline.short_description = "épisodes"


class EmissionAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Emission`.
    """
    fieldsets = [(None,
                  {"fields": ("nom",
                              "duree",
                              "display_image_html",
                              "image")})]
    readonly_fields = ["duree",
                       "display_image_html"]
    list_display = ("nom",
                    "duree")
    search_fields = ["nom"]
    inlines = [EpisodePourEmissionInline]

    def add_view(self, request, form_url="", extra_context=None):
        """
        Ajoute un message dans les pages d'ajouts d'`Emission` disant
        d'enregistrer l'`Emission` avant de pouvoir ajouter des `Episode`.
        """
        message = "Pour pouvoir ajouter des épisodes sur cette page, " \
                  "il faut d'abord enregistrer la nouvelle émission " \
                  "(bouton 'Enregistrer et continuer les modifications')"
        self.message_user(request, message, messages.INFO)
        return super().add_view(request, form_url, extra_context)


admin.site.register(Intervenant, IntervenantAdmin)
admin.site.register(Episode, EpisodeAdmin)
admin.site.register(Emission, EmissionAdmin)
admin.site.register(SourceExterne, SourceExterneAdmin)
admin.site.register(MotClef, MotClefAdmin)
admin.site.register(Role, GroupAdmin)
