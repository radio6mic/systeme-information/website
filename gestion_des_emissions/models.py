# -*- coding: utf-8 -*-

"""
Définition du modèle de données pour la gestion des émissions.
"""

import datetime
import logging
from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User, Group
from gestion_de_la_diffusion.models import (Diffusable,
                                            ObjetSonore,
                                            ObjetImage)
from website import settings

LOG = logging.getLogger(__name__)


class MotClef(models.Model):
    """
    Un `MotClef` est un mot utilisé pour indexer un `Episode` permettant de le
    retrouver lors d'une recherche.
    """
    mot_clef = models.CharField(
            "mot-clé",
            unique=True,
            max_length=40,
            help_text="Mot-clé utilisé pour décrire un fichier sonore.")

    def __str__(self):
        return "{mot}".format(mot=self.mot_clef)

    class Meta:
        """
        Meta données de la classe `MotClef`.
        """
        verbose_name = "mot-clé"
        verbose_name_plural = "mots-clés"


class SourceExterne(models.Model):
    """
    Une `SourceExterne` représente un élément extérieur au système
    d'information utilisé pour alimenter le contenu d'un `Episode`.
    Pas de restriction au niveau de l'unicité des champs.
    """
    titre = models.CharField(
            max_length=255,
            help_text="Titre ou courte description de la ressource.")
    auteur = models.CharField(
            "auteur(s)",
            max_length=500,
            help_text="Auteur(s) de la ressource. En cas de plusieurs auteurs"
                      ", tous les noms sont à écrire dans ce champ.")
    date_publication = models.DateField(
            "date de publication")
    editeur = models.CharField(
            "éditeur",
            max_length=500,
            blank=True,
            null=True,
            help_text="(Facultatif) Editeur de la ressource.")
    url = models.URLField(
            "lien Internet",
            blank=True,
            null=True,
            help_text="(Facultatif) Lien Internet valide de la ressource.")

    def __str__(self):
        return "{titre} par {auteur}".format(titre=self.titre,
                                             auteur=self.auteur)

    class Meta:
        """
        Meta données de la classe `SourceExterne`.
        """
        verbose_name = "source externe"
        verbose_name_plural = "sources externes"


class Emission(ObjetImage):
    """
    Conteneur d'`Episode`, tel un `Album` contient des `Piste`.
    Le nom de l'`Emission` alimentera le champ ID3v2 : TALB.
    """
    nom = models.CharField(
            max_length=255,
            help_text="Nom de l'émission (Renseignera "
                      "le champ TALB  dans le Tag ID3v2 du fichier audio)")
    annee_scolaire = models.IntegerField(
            "année scolaire",
            default=(datetime.datetime.today()
                     - datetime.timedelta(days=211)).year,
            editable=False,
            help_text="Champ non éditable valorisé automatiquement avec "
                      "la valeur de l'année scolaire courante.")

    @property
    def duree(self):
        """
        Retourne la somme des durées des `Episode` associés dont les fichiers
        sont disponibles sur le système de fichiers.
        """
        duree = settings.DUREE_NULLE
        if self.episode_set_emission.exists():
            for episode in self.episode_set_emission.all():
                if episode.fichier_disponible() is True:
                    duree += episode.duree
        return duree

    def __str__(self):
        return "{nom} ({annee1}-{annee2})".format(
                nom=self.nom,
                annee1=self.annee_scolaire,
                annee2=self.annee_scolaire + 1)

    def validation(self, exclude=None):
        """
        Vérifie la contrainte d'unicité commune entre nom et annee_scolaire.
        Cette validation est nécessaire ici car unique_together
        ne vérifie pas l'unicité dans ce cas.
        """
        try:
            liste_doublons = Emission.objects.filter(
                                            nom=self.nom,
                                            annee_scolaire=self.annee_scolaire)
            # Il faut vérifier si l'`Emission `existe déjà ou si c'est
            # une nouvelle
            # Si elle existe déjà, il faut pouvoir modifier les champs
            if self.pk is None:
                enregistrement_existe = liste_doublons.exists()
            else:
                enregistrement_existe = liste_doublons.exclude(
                                                        pk=self.pk).exists()
            if enregistrement_existe:
                message = "Une émission nommée '{nom}' " \
                          "existe déjà sur la période {annee1}-{annee2}" \
                          .format(nom=self.nom,
                                  annee1=self.annee_scolaire,
                                  annee2=self.annee_scolaire + 1)
                raise ValidationError(message)
        except Emission.DoesNotExist:
            return

    def save(self, *args, **kwargs):
        self.validation()
        self.traitement_image()
        super().save(*args, **kwargs)

    class Meta:
        """
        Meta données de la classe `Emission`.
        """
        verbose_name = "émission"


class Episode(Diffusable, ObjetSonore, ObjetImage):
    """
    Un `Episode` représente un enregistrement produit et diffusable.
    """
    emission = models.ForeignKey(
            Emission,
            on_delete=models.PROTECT,
            verbose_name="émission",
            related_name="episode_set_emission",
            help_text="Émission à laquelle l'épisode est "
                      "associé de manière unique (Renseigne "
                      "le champ TALB  dans le Tag ID3v2 du fichier audio)")
    sources_externes = models.ManyToManyField(
            SourceExterne,
            through="RelationEpisodeSourceExterne",
            verbose_name="source externe",
            help_text="(Facultatif) Ressources utilisées dans l'épisode")
    description = models.TextField(
            blank=True,
            null=True,
            help_text="(Facultatif) Texte présentant le fichier audio."
                      " Sera affiché sur la page web lors de la diffusion"
                      " et sera utilisé lors de recherches de fichiers audio"
                      " dans la base de données.")
    mots_clefs = models.ManyToManyField(
            MotClef,
            through="RelationEpisodeMotClef",
            verbose_name="mots-clés",
            help_text="(Facultatif) Mots-clés décrivant le fichier audio."
                      " Seront utilisés lors de recherches de fichiers audio"
                      " dans la base de données.")

    def validation(self, exclude=None):
        """
        Dans une même `Emission`, il ne peut pas y avoir deux `Episode`
        avec le même titre.
        """
        try:
            liste_doublons = Episode.objects.filter(emission=self.emission,
                                                    titre=self.titre)
            # Il faut vérifier si l'`Episode` existe déjà ou si
            # c'est un nouveau
            # S'il existe déjà, il faut pouvoir modifier les autres champs
            if self.pk is None:
                enregistrement_existe = liste_doublons.exists()
            else:
                enregistrement_existe = liste_doublons.exclude(
                                                        pk=self.pk).exists()
            if enregistrement_existe:
                message = "Un épisode {titre} dans l'émission {nom} existe " \
                          "déjà".format(titre=self.titre,
                                        nom=self.emission.nom)
                raise ValidationError(message)
        except Emission.DoesNotExist:
            return

    def save(self, *args, **kwargs):
        self.validation()
        self.traitement_image()
        super().save(*args, **kwargs)

    def delete(self, *args, keep_ObjetSonore=False, **kwargs):
        """
        TODO: faire en sorte que l'algorithme n'existe que dans le
        parent ObjetSonore.

        keep_ObjetSonore: booléen permettant de ne pas appeler la
        destruction de l'`ObjetSonore` associé.
        """
        if not keep_ObjetSonore:
            super().delete(self, *args, **kwargs)
        else:
            Diffusable.delete(self, *args, **kwargs)

    class Meta:
        """
        Meta données de la classe `Episode`.
        """
        verbose_name = "épisode"


class Intervenant(User):
    """
    Un `Intervenant` est un utilisateur spécifique qui intervient
    dans un `Episode`.
    """
    def __str__(self):
        nom = self.get_full_name()
        if nom == "":
            nom = self.username
        return nom

    class Meta:
        """
        Meta données de la classe Intervenant
        """
        verbose_name = "intervenant"


class Role(Group):
    """
    Le `Role` détermine la nature de la relation d'`Intervention`.
    """

    class Meta:
        """
        Meta données de la classe `Role`.
        """
        verbose_name = "rôle"


class Intervention(models.Model):
    """
    Une `Intervention` représente la relation entre
    un `User` et un `Episode`.
    """
    episode = models.ForeignKey(
            Episode,
            on_delete=models.CASCADE)
    intervenant = models.ForeignKey(
            Intervenant,
            on_delete=models.PROTECT)
    role = models.ForeignKey(
            Role,
            on_delete=models.PROTECT,
            verbose_name="rôle")

    def __str__(self):
        return "{intervenant} est {role} pour {episode}".format(
                intervenant=str(self.intervenant),
                role=str(self.role),
                episode=str(self.episode))

    class Meta:
        """
        Meta données de la classe `Intervention`.
        """
        unique_together = ("episode", "intervenant", "role")


class RelationEpisodeSourceExterne(models.Model):
    """
    Classe intermédiaire pour la relation ManyToMany entre les `Episode`
    et les `SourceExterne`.
    """
    episode = models.ForeignKey(
            Episode,
            on_delete=models.CASCADE,
            verbose_name="épisode")
    source_externe = models.ForeignKey(
            SourceExterne,
            on_delete=models.PROTECT,
            verbose_name="source externe",
            help_text="Référence à une ressource utilisée ou "
                      "citée dans l'épisode.")

    class Meta:
        """
        Meta données de la classe `RelationEpisodeSourceExterne`.
        """
        unique_together = ("episode", "source_externe")


class RelationEpisodeMotClef(models.Model):
    """
    Classe intermédiaire pour la relation ManyToMany entre les `Episode`
    et les `MotClef`.
    """
    episode = models.ForeignKey(
            Episode,
            on_delete=models.CASCADE,
            verbose_name="épisode")
    mot_clef = models.ForeignKey(
            MotClef,
            on_delete=models.PROTECT,
            verbose_name="mot-clé",
            help_text="Mots-clés décrivant le fichier audio."
                      " Seront utilisés lors de recherches de fichiers audio"
                      " dans la base de données.")

    class Meta:
        """
        Meta données de la classe `RelationEpisodeMotClef`.
        """
        unique_together = ("episode", "mot_clef")
