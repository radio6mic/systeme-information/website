# -*- coding: utf-8 -*-

"""
Définition de la configuration de l'application `gestion_des_musiques`.
"""

from django.apps import AppConfig


class GestionDesMusiquesConfig(AppConfig):
    """
    Configuration de l'application `gestion_des_musiques`
    """
    name = 'gestion_des_musiques'
    verbose_name = "gestion du contenu musical"
