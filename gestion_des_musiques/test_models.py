# -*- coding: utf-8 -*-

"""
Module de test des modèles de l'application gestion_des_musiques.
"""

import os
import datetime
import tempfile
import django.test
from django.db.utils import IntegrityError
from PIL import Image
from django.db import transaction
from django.db.models.deletion import ProtectedError
from .models import (Album,
                     Genre,
                     Artiste,
                     Piste)
from gestion_de_la_diffusion.models import (Diffusable,
                                            Editeur,
                                            Licence,
                                            Provenance)
from website import settings

# Nombre d'insertions et de suppressions d'objets à réaliser
# lors des tests 'test_objet_insertion_suppression'
NB_INSERTIONS = 5


class GenreTest(django.test.TestCase):
    """
    Test des objets de type `Genre`.
    """
    fixtures = ["Genre"]
    prefixeTest = "GenreTest" + next(tempfile._get_candidate_names())

    def test_Genre_doublon(self):
        """
        S'assure que l'on ne peut pas ajouter un `Genre` déjà existant.
        """
        GenreTest = Genre.objects.first()
        with self.assertRaises(IntegrityError):
            Genre.objects.create(nom=GenreTest.nom)

    def test_Genre_existence(self):
        """
        S'assure que les `Genre` fournis existent dans
        la base et sont des `Genre`.
        """
        genres = Genre.objects.all()
        # tests sur les 3 premiers `Genre`
        for i in range(3):
            genreTest = genres[i]
            with self.subTest(genre=genreTest):
                self.assertIsInstance(Genre.objects.get(nom=genreTest.nom),
                                      Genre)

    def test_Genre_inexistence(self):
        """
        S'assure qu'une exception est soulevée lorsqu'un `Genre` n'existe pas.
        """
        with self.assertRaises(Genre.DoesNotExist):
            Genre.objects.get(nom=self.prefixeTest)

    def test_Genre_insertion_suppression(self):
        """
        S'assure qu'insertions et suppressions de `Genre` fonctionnent.
        Réalise l'insertion de NB_INSERTIONS `Genre`.
        Modification de la moitiée des `Genre` avec un nom altéré.
        Réalise enfin une suppression des NB_INSERTIONS `Genre` insérés.
        """
        nb_genres = Genre.objects.all().count()
        nb_noms_modif = 0
        genres = [Genre(nom="{nom}-{suffixe}".format(nom=self.prefixeTest,
                                                     suffixe=str(i)))
                  for i in range(NB_INSERTIONS)]
        for genreTest in genres:
            with self.subTest(genre=genreTest):
                # le `Genre` courant de la collection n'a pas de Primary Key
                # car pas enregistré en base
                self.assertIsNone(genreTest.pk)
                genreTest.save()
                genreTest.refresh_from_db()
                # et finalement le `Genre` a une Primary Key
                # car sauvegardé en base
                self.assertIsNotNone(genreTest.pk)
                if genreTest.pk % 2 == 0:
                    nb_noms_modif += 1
                    genreTest.nom = "{nom}-{nom}-{suffixe}".format(
                                            nom=self.prefixeTest,
                                            suffixe=str(genreTest.pk))
                    genreTest.save()
        # Vérification que les modifications de nom ont été prises en compte
        genres = Genre.objects.filter(
                nom__startswith="{nom}-{nom}".format(nom=self.prefixeTest))
        self.assertEqual(genres.count(),
                         nb_noms_modif)
        genres = Genre.objects.filter(nom__startswith=self.prefixeTest)
        for genreTest in genres:
            with self.subTest(genre=genreTest):
                genreTest.delete()
        # Vérification qu'il y a bien le même nombre de `Genre` à la fin
        # qu'au début du test
        self.assertEqual(Genre.objects.all().count(),
                         nb_genres)


class ArtisteTest(django.test.TestCase):
    """
    Test des objets de type `Artiste`.
    """
    fixtures = ["Artiste"]
    prefixeTest = "ArtisteTest" + next(tempfile._get_candidate_names())

    def test_Artiste_doublon(self):
        """
        S'assure que l'on ne peut pas ajouter un `Artiste` déjà existant
        """
        artisteTest = Artiste.objects.first()
        with self.assertRaises(IntegrityError):
            Artiste.objects.create(nom=artisteTest.nom)

    def test_Artiste_existence(self):
        """
        S'assure que les `Artiste` fournis existent dans
        la base et sont des `Artiste`.
        """
        artisteTest = Artiste.objects.first()
        self.assertIsInstance(artisteTest,
                              Artiste)

    def test_Artiste_inexistence(self):
        """
        S'assure qu'une exception est soulevée lorsqu'un
        `Artiste` n'existe pas.
        """
        with self.assertRaises(Artiste.DoesNotExist):
            Artiste.objects.get(nom=self.prefixeTest)

    def test_Artiste_insertion_suppression(self):
        """
        S'assure qu'insertions et suppressions d'`Artiste` fonctionnent.
        Réalise l'insertion de NB_INSERTIONS `Artistes`.
        Modifie de la moitiée des `Artiste` avec un nom altéré.
        Réalise enfin ;la suppression des NB_INSERTIONS `Artiste` insérés.
        """
        nb_artistes = Artiste.objects.all().count()
        nb_noms_modif = 0
        artistes = [Artiste(nom="{nom}-{suffixe}"
                            .format(nom=self.prefixeTest,
                                    suffixe=str(i)))
                    for i in range(NB_INSERTIONS)]
        for artisteTest in artistes:
            with self.subTest(artiste=artisteTest):
                # l'`Artiste` courant de la collection n'a pas de Primary Key
                # car pas enregistré en base
                self.assertIsNone(artisteTest.pk)
                artisteTest.save()
                artisteTest.refresh_from_db()
                # et finalement l'`Artiste` a une Primary Key
                # car sauvegardé en base
                self.assertIsNotNone(artisteTest.pk)
                if artisteTest.pk % 2 == 0:
                    nb_noms_modif += 1
                    artisteTest.nom = "{nom}-{nom}-{suffixe}".format(
                                            nom=self.prefixeTest,
                                            suffixe=str(artisteTest.pk))
                    artisteTest.save()
        # Vérification que les modifications de nom ont été prises en compte
        artistes = Artiste.objects.filter(
                nom__startswith="{nom}-{nom}".format(nom=self.prefixeTest))
        self.assertEqual(artistes.count(),
                         nb_noms_modif)
        artistes = Artiste.objects.filter(nom__startswith=self.prefixeTest)
        for artisteTest in artistes:
            with self.subTest(artiste=artisteTest):
                artisteTest.delete()
        # Vérifie qu'il y a bien le même nombre d'`Artiste` à la fin
        # qu'au début du test
        self.assertEqual(Artiste.objects.all().count(),
                         nb_artistes)


class AlbumTest(django.test.TestCase):
    """
    Test des objets de type `Album`.
    """
    fixtures = ["Album"]
    prefixeTest = "AlbumTest" + next(tempfile._get_candidate_names())

    def test_Album_doublon(self):
        """
        S'assure que l'on ne peut pas ajouter un `Album` déjà existant.
        """
        albumTest = Album.objects.first()
        with self.assertRaises(IntegrityError):
            Album.objects.create(nom=albumTest.nom)

    def test_Album_existence(self):
        """
        S'assure que les `Album` fournis existent dans la base
        et sont des `Album`.
        """
        albumTest = Album.objects.first()
        self.assertIsInstance(albumTest,
                              Album)

    def test_Album_inexistence(self):
        """
        S'assure qu'une exception est soulevée lorsqu'un `Album` n'existe pas.
        """
        with self.assertRaises(Album.DoesNotExist):
            Album.objects.get(nom=self.prefixeTest)

    def test_Album_insertion_suppression(self):
        """
        S'assure qu'insertions et suppressions d'`Album` fonctionnent.
        Réalise l'insertion de NB_INSERTIONS `Album`.
        Effectue une modification du nom pour la moitié des
        `Album` insérés.
        Réalise enfin la suppression des NB_INSERTIONS `Album` insérés.
        """
        nb_albums = Album.objects.all().count()
        nb_noms_modif = 0
        albums = [Album(nom="{nom}-{suffixe}".format(nom=self.prefixeTest,
                                                     suffixe=str(i)))
                  for i in range(NB_INSERTIONS)]
        for albumTest in albums:
            with self.subTest(album=albumTest):
                # l'album courant de la collection n'a pas de Primary Key
                # car pas enregistré base
                self.assertIsNone(albumTest.pk)
                albumTest.save()
                albumTest.refresh_from_db()
                # et finalement l'album a une Primary Key
                # car sauvegardé en base
                self.assertIsNotNone(albumTest.pk)
                if albumTest.pk % 2 == 0:
                    nb_noms_modif += 1
                    albumTest.nom = "{nom}-{nom}-{suffixe}".format(
                                            nom=self.prefixeTest,
                                            suffixe=str(albumTest.pk))
                    albumTest.save()
        # Vérification que les modifications de nom ont été prises en compte
        albums = Album.objects.filter(
                nom__startswith="{nom}-{nom}".format(nom=self.prefixeTest))
        self.assertEqual(albums.count(),
                         nb_noms_modif)
        albums = Album.objects.filter(nom__startswith=self.prefixeTest)
        for albumTest in albums:
            with self.subTest(album=albumTest):
                albumTest.delete()
        # Vérification qu'il y a bien le même nombre d'`Album` à la fin
        # qu'au début du test
        self.assertEqual(Album.objects.all().count(),
                         nb_albums)


class PisteTest(django.test.TestCase):
    """
    Test des objets de type `Piste`.
    """
    fixtures = ["Album",
                "Artiste",
                "Diffusable",
                "Editeur",
                "Genre",
                "Licence",
                "ObjetSonore",
                "Piste",
                "Medium",
                "Provenance"]

    prefixeTest = "PisteTest" + next(tempfile._get_candidate_names())
    # Titre le la `Piste` utilisée dans les tests
    # (position 1 dans albumTest, d'artiste artisteTest,
    # de provenance provenanceTest)
    pisteTest_titre = "vapeur"
    positionTest = 1
    # Nom de l'`Album` de la pisteTest comportant 6 `Piste` en tout
    albumTest_nom = "Aven"
    position_libre = 100
    nb_pistes_albumTest = 6
    # `Artiste` présent et utilisé 6 fois dans les fixtures
    artisteTest_nom = "groolot"
    nb_pistes_artisteTest = 6
    # `Artiste` présent mais inutilisé dans les fixtures
    artiste_inutile_nom = "david bowie"
    # `Provenance` présente et utilisée 6 fois dans les fixtures
    provenanceTest_description = "Site officiel de Groolot"
    nb_pistes_provenanceTest = 6
    # `Provenance` présente mais inutilisée dans les fixtures
    provenance_inutile_pk = 2
    # `Genre` présent et utilisé 6 fois dans les fixtures
    genreTest_pk = 52
    nb_pistes_genreTest = 6
    # `Genre` présent mais inutilisé dans les fixtures
    genre_inutile_pk = 1

    def setUp(self):
        """
        Definit et réinitialise à chaque test les pisteTest, albumTest,
        artisteTest, provenanceTest et genreTest utilisés dans les tests.
        """
        self.pisteTest = Piste.objects.get(titre=self.pisteTest_titre)
        self.albumTest = Album.objects.get(nom=self.albumTest_nom)
        self.artisteTest = Artiste.objects.get(nom=self.artisteTest_nom)
        self.provenanceTest = Provenance.objects.get(
                                description=self.provenanceTest_description)
        self.genreTest = Genre.objects.get(pk=self.genreTest_pk)

    def test_Piste_doublon(self):
        """
        S'assure que l'on ne peut pas ajouter une `Piste` déjà existante
        On va vérifier qu'en position 1 de l'`Album` "Aven" on ne
        peut pas ajouter une `Piste`.
        """
        nb_titres_avant = Diffusable.objects.filter(
                                        titre=self.prefixeTest).count()
        with self.assertRaises(IntegrityError):
            with transaction.atomic():
                Piste.objects.create(
                        titre=self.prefixeTest,
                        date_publication=datetime.date.today(),
                        position=self.positionTest,
                        genre=Genre.objects.first(),
                        album=self.albumTest,
                        artiste=Artiste.objects.first(),
                        provenance=Provenance.objects.first(),
                        editeur=Editeur.objects.first(),
                        licence=Licence.objects.first())
        nb_titres_apres = Diffusable.objects.filter(
                                        titre=self.prefixeTest).count()
        # Vérification qu'aucun `Diffusable` n'a été ajouté
        self.assertEqual(nb_titres_avant,
                         nb_titres_apres)

    def test_Piste_existence(self):
        """
        S'assure que les `Piste` fournies existent dans
        la base et sont des `Piste`.
        """
        pistes = Piste.objects.all()
        # tests sur les 3 premières `Piste`
        for i in range(3):
            pisteTest = pistes[i]
            with self.subTest(piste=pisteTest):
                self.assertIsInstance(Piste.objects.get(titre=pisteTest.titre),
                                      Piste)

    def test_Piste_inexistence(self):
        """
        S'assure qu'une exception est soulevée lorsqu'une `Piste` n'existe pas.
        """
        with self.assertRaises(Piste.DoesNotExist):
            Piste.objects.get(titre=self.prefixeTest)

    def test_Piste_insertion_suppression(self):
        """
        S'assure qu'insertions et suppressions de `Piste` fonctionnent.
        Réalise l'insertion de NB_INSERTIONS `Piste` pour l'`Album` "Aven"
        et de position 100, 101, 102 etc...
        Effectue une modification de l'`Album` associé pour la moitié des
        `Piste` insérées.
        Réalise enfin la suppression des NB_INSERTIONS `Piste` insérées.
        """
        nb_pistes = Piste.objects.all().count()
        nb_noms_modif = 0
        pistes = [Piste(titre="{titre}-{suffixe}"
                              .format(titre=self.prefixeTest,
                                      suffixe=str(i)),
                        date_publication=datetime.date.today(),
                        position=self.position_libre + i,
                        genre=Genre.objects.first(),
                        album=self.albumTest,
                        artiste=Artiste.objects.first(),
                        provenance=Provenance.objects.first(),
                        editeur=Editeur.objects.first(),
                        licence=Licence.objects.first())
                  for i in range(NB_INSERTIONS)]
        # Création d'un nouvel `Album`
        albumTest1 = Album(nom="AlbumTest-{suffixe}"
                               .format(suffixe=self.prefixeTest))
        albumTest1.save()
        genreTest = Genre.objects.last()
        for pisteTest in pistes:
            with self.subTest(piste=pisteTest):
                # la `Piste` courante de la collection n'a pas de Primary Key
                # car pas enregistrée en base
                self.assertIsNone(pisteTest.pk)
                pisteTest.save()
                pisteTest.refresh_from_db()
                # et finalement la `Piste` a une Primary Key
                # car sauvegardée en base
                self.assertIsNotNone(pisteTest.pk)
                if pisteTest.pk % 2 == 0:
                    nb_noms_modif += 1
                    pisteTest.album = albumTest1
                    pisteTest.genre = genreTest
                    pisteTest.save()
                    pisteTest.refresh_from_db()
                    self.assertEqual(pisteTest.album.nom,
                                     albumTest1.nom)
                    self.assertEqual(pisteTest.genre.nom,
                                     genreTest.nom)
        # Vérification que les modifications ont été prises en compte
        pistes = Piste.objects.filter(album__nom=albumTest1.nom,
                                      genre__nom=genreTest.nom)
        self.assertEqual(pistes.count(),
                         nb_noms_modif)
        pistes = Piste.objects.filter(titre__startswith=self.prefixeTest)
        for pisteTest in pistes:
            with self.subTest(piste=pisteTest):
                pisteTest.delete()
        albumTest1.delete()
        # Vérification qu'il y a bien le même nombre de `Piste` à la fin
        # qu'au début du test
        self.assertEqual(Piste.objects.all().count(),
                         nb_pistes)

    def test_Piste_Album_related(self):
        """
        S'assure qu'une `Piste` fait partie de l'ensemble des `Piste`
        de l'`Album` auquel elle est associée.
        """
        self.assertEqual(self.albumTest.piste_set.all().count(),
                         self.nb_pistes_albumTest)
        self.assertIn(self.pisteTest,
                      self.albumTest.piste_set.all())

    def test_Album_suppression(self):
        """
        S'assure qu'on ne peut pas supprimer un `Album` ayant
        des `Piste` associées.
        """
        nb_albums_avant = Album.objects.all().count()
        nb_pistes_avant = Piste.objects.all().count()
        # Echec de suppression d'un `Album` contenant des `Piste`
        with self.assertRaises(ProtectedError):
            self.albumTest.delete()
        nb_albums_apres = Album.objects.all().count()
        self.assertEqual(nb_albums_avant,
                         nb_albums_apres)
        # Vérifie que les `Piste` n'ont pas été supprimées
        nb_pistes_apres = Piste.objects.all().count()
        self.assertEqual(nb_pistes_avant,
                         nb_pistes_apres)

    def test_Piste_Artiste_related(self):
        """
        S'assure qu'une `Piste` fait partie de l'ensemble des `Piste`
        de l'`Artiste` auquel elle est associée.
        """
        self.assertEqual(self.artisteTest.piste_set.all().count(),
                         self.nb_pistes_artisteTest)
        self.assertIn(self.pisteTest,
                      self.artisteTest.piste_set.all())

    def test_Artiste_suppression(self):
        """
        S'assure qu'on peut supprimer un `Artiste` non utilisé,
        mais qu'on ne peut pas supprimer un `Artiste` utilisé par une `Piste`.
        """
        nb_artistes_avant = Artiste.objects.all().count()
        nb_pistes_avant = Piste.objects.all().count()
        # Supprime un `Artiste` non utilisé
        Artiste.objects.get(nom=self.artiste_inutile_nom).delete()
        # Vérifie qu'on a bien 1 `Artiste` de moins
        # et autant de `Piste` qu'au début
        nb_artistes_apres = Artiste.objects.all().count()
        self.assertEqual(nb_artistes_avant,
                         nb_artistes_apres + 1)
        nb_pistes_apres = Piste.objects.all().count()
        self.assertEqual(nb_pistes_avant,
                         nb_pistes_apres)
        # Supprime un `Artiste` utilisé par des `Piste`
        with self.assertRaises(ProtectedError):
            self.artisteTest.delete()
        # Vérifie qu'on a toujours 1 `Artiste` de moins
        # et autant de `Piste` qu'au début
        nb_artistes_apres = Artiste.objects.all().count()
        self.assertEqual(nb_artistes_avant,
                         nb_artistes_apres + 1)
        nb_pistes_apres = Piste.objects.all().count()
        self.assertEqual(nb_pistes_avant,
                         nb_pistes_apres)

    def test_Piste_Provenance_related(self):
        """
        S'assure qu'une `Piste` fait partie de l'ensemble des `Piste`
        de la `Provenance` auquel elle est associée.
        """
        self.assertEqual(self.provenanceTest.piste_set.all().count(),
                         self.nb_pistes_provenanceTest)
        self.assertIn(self.pisteTest,
                      self.provenanceTest.piste_set.all())

    def test_Provenance_suppression(self):
        """
        S'assure qu'on peut supprimer une `Provenance` non utilisée
        mais qu'on ne peut pas supprimer une `Provenance`
        utilisée par une `Piste`.
        """
        nb_provenances_avant = Provenance.objects.all().count()
        nb_pistes_avant = Piste.objects.all().count()
        # Supprime une `Provenance` non utilisée
        Provenance.objects.get(pk=self.provenance_inutile_pk).delete()
        # Vérifie qu'on a bien 1 `Provenance` de moins
        # et autant de `Piste` qu'au début
        nb_provenances_apres = Provenance.objects.all().count()
        self.assertEqual(nb_provenances_avant,
                         nb_provenances_apres + 1)
        nb_pistes_apres = Piste.objects.all().count()
        self.assertEqual(nb_pistes_avant,
                         nb_pistes_apres)
        with self.assertRaises(Provenance.DoesNotExist):
            Provenance.objects.get(description=self.prefixeTest)
        # Supprime une `Provenance` utilisée par des `Piste`
        with self.assertRaises(ProtectedError):
            self.provenanceTest.delete()
        # Vérifie qu'on a bien 1 `Provenance` de moins
        # et autant de `Piste` qu'au début
        nb_provenances_apres = Provenance.objects.all().count()
        self.assertEqual(nb_provenances_avant,
                         nb_provenances_apres + 1)
        nb_pistes_apres = Piste.objects.all().count()
        self.assertEqual(nb_pistes_avant,
                         nb_pistes_apres)

    def test_Piste_Genre_related(self):
        """
        S'assure qu'une `Piste` fait partie de l'ensemble des `Piste`
        du `Genre` auquel elle est associée.
        """
        self.assertEqual(self.genreTest.piste_set.all().count(),
                         self.nb_pistes_genreTest)
        self.assertIn(self.pisteTest,
                      self.genreTest.piste_set.all())

    def test_Genre_suppression(self):
        """
        S'assure qu'on peut supprimer un `Genre` non utilisé,
        mais qu'on ne peut pas supprimer un `Genre` utilisé par une `Piste`.
        """
        nb_genres_avant = Genre.objects.all().count()
        nb_pistes_avant = Piste.objects.all().count()
        # Supprime un `Genre` non utilisé
        Genre.objects.get(pk=self.genre_inutile_pk).delete()
        # Vérifie qu'on a bien 1 `Genre` de moins et
        # autant de `Piste` qu'au début
        nb_genres_apres = Genre.objects.all().count()
        self.assertEqual(nb_genres_avant,
                         nb_genres_apres + 1)
        nb_pistes_apres = Piste.objects.all().count()
        self.assertEqual(nb_pistes_avant,
                         nb_pistes_apres)
        with self.assertRaises(Genre.DoesNotExist):
            Genre.objects.get(pk=self.genre_inutile_pk)
        # Supprime un `Genre` utilisé par des `Piste`
        with self.assertRaises(ProtectedError):
            self.genreTest.delete()
        # Vérifie qu'on a bien toujours 1 `Genre` de moins
        # et autant de `Piste` qu'au début
        nb_genres_apres = Genre.objects.all().count()
        self.assertEqual(nb_genres_avant,
                         nb_genres_apres + 1)
        nb_pistes_apres = Piste.objects.all().count()
        self.assertEqual(nb_pistes_avant,
                         nb_pistes_apres)


class ImageAlbumTest(django.test.TestCase):
    """
    Tests de la gestion des images pour les `Album`.
    """
    # Préfixe pour tous les nom de fichiers test
    prefixeTest = next(tempfile._get_candidate_names())
    albumTest_nom = "AlbumTest" + prefixeTest
    # Tailles, modes et formats testés (il faut au moins 3 choix minimum
    # par atribut pour la fonction "test_nom_fichier_image")
    sizesTest = [(200, 500), (1500, 500), (500, 1500), (1500, 1500)]
    modesTest = ["1", "L", "RGB"]
    formatsTest = ["JPEG", "PNG", "TIFF"]
    # Tailles, mode et format attendus des images après traitement
    expected_size_max = settings.IMAGE_CONFIG["max_size"]
    expected_mode = settings.IMAGE_CONFIG["mode"]
    expected_format = settings.IMAGE_CONFIG["format"]
    # Dossier où sont stockées les images
    dossier_image = os.path.join(settings.MEDIA_ROOT,
                                 settings.IMAGE_DIR,
                                 "Album")
    # Dossier temporaire pour fabriquer les images
    dossier_tmp = os.path.join(settings.MEDIA_ROOT,
                               prefixeTest)

    @classmethod
    def setUpClass(cls):
        """
        Initialisation de la classe. Création du dossier temporaire.
        """
        super().setUpClass()
        os.makedirs(cls.dossier_tmp, exist_ok=True)

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage du dossier MEDIA_ROOT de tous les fichiers de test.
        """
        # Supprime tous les fichiers image créés dans les tests
        liste_fichiers_image = os.listdir(cls.dossier_image)
        for nom_fichier in liste_fichiers_image:
            if nom_fichier.find(cls.albumTest_nom) != -1:
                fichier_path = os.path.join(cls.dossier_image, nom_fichier)
                os.remove(fichier_path)
        # Supprime le dossier temporaire après l'avoir éventuellement vidé
        liste_fichiers_tmp = os.listdir(cls.dossier_tmp)
        for nom_fichier in liste_fichiers_tmp:
            fichier_path = os.path.join(cls.dossier_tmp, nom_fichier)
            os.remove(fichier_path)
        os.rmdir(cls.dossier_tmp)
        super().tearDownClass()

    def test_traitement_image(self):
        """
        Tests de la fonction traitement_image() pour les `Album`:
        Vérifie que le fichier image est correctement nommé.
        Vérifie que le mode est toujours settings.IMAGE_CONFIG["mode"].
        Vérifie que les dimensions finales sont inférieures
        à settings.IMAGE_CONFIG["max_size"]
        et que le ratio est bien conservé
        """
        # On teste traitement_image() pour toutes les tailles, modes
        # et formats prévus
        for s in range(len(self.sizesTest)):
            for m in range(len(self.modesTest)):
                for f in range(len(self.formatsTest)):
                    # Création d'une image noire avec les attributs désirés
                    imagePIL = Image.new(self.modesTest[m],
                                         self.sizesTest[s])
                    image_path = os.path.join(
                                        self.dossier_tmp,
                                        next(tempfile._get_candidate_names()))
                    # Enregistrement de l'image au format désiré
                    imagePIL.save(image_path, format=self.formatsTest[f])
                    # Création d'un `Album` utilisant l'image
                    albumTest = Album.objects.create(
                                    nom="{nom}-{num_size}-{num_mode}"
                                        "-{num_format}"
                                        .format(nom=self.albumTest_nom,
                                                num_size=s,
                                                num_mode=m,
                                                num_format=f),
                                    image=image_path)
                    # Ouverture le l'image après son traitement
                    image_albumTest = Image.open(albumTest.image.file)
                    # Véification du mode, du format et des dimensions
                    self.assertEqual(image_albumTest.mode,
                                     self.expected_mode)
                    self.assertEqual(image_albumTest.format,
                                     self.expected_format)
                    self.assertLessEqual(image_albumTest.width,
                                         self.expected_size_max[0])
                    self.assertLessEqual(image_albumTest.height,
                                         self.expected_size_max[1])
                    # Vérification que le ratio de l'image est conservé
                    ratioTest = self.sizesTest[s][0] / self.sizesTest[s][1]
                    largeur = image_albumTest.width
                    hauteur = image_albumTest.height
                    ratio_image = largeur / hauteur
                    self.assertEqual(round(ratioTest, 1),
                                     round(ratio_image, 1))
                    # Vérification que le dossier tmp est vide
                    self.assertEqual(len(os.listdir(self.dossier_tmp)),
                                     0)

    def test_nom_fichier_image(self):
        """
        Vérifie que le fichier image est bien géré lors d'une création
        ou une modification d'un `Album` (vérification existence ou non
        du fichier et vérification qu'il est toujours bien nommé).
        """
        def nouveau_fichier_image(num):
            # Création d'une image noire avec les attributs définis par
            # le paramètre num
            imagePIL = Image.new(self.modesTest[num],
                                 self.sizesTest[num])
            image_path = os.path.join(self.dossier_tmp,
                                      next(tempfile._get_candidate_names()))
            # Enregistrement de l'image au format désiré
            imagePIL.save(image_path, format=self.formatsTest[num])
            return image_path

        # Création d'un `Album` utilisant une image
        num = 0
        albumTest = Album.objects.create(
                            nom="{nom}-{num}-{num}-{num}"
                                .format(nom=self.albumTest_nom,
                                        num=num),
                            image=nouveau_fichier_image(num))
        # Vérification que le fichier image existe et que le fichier
        # initial a bien été supprimé
        expected_name = os.path.join(
                self.dossier_image,
                "{nom}.{extension}"
                .format(nom=albumTest.nom,
                        extension=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_tmp)),
                         0)
        # Modification de l'image de l'`Album`
        num += 1
        albumTest.image = nouveau_fichier_image(num)
        albumTest.save()
        # Vérification que l'image a bien été remplacée (ratio différent)
        # que le fichier image existe et que le fichier initial a été supprimé
        image_albumTest = Image.open(albumTest.image.file)
        ratio_image = image_albumTest.width / image_albumTest.height
        expected_ratio = self.sizesTest[num][0] / self.sizesTest[num][1]
        self.assertEqual(round(ratio_image, 1),
                         round(expected_ratio, 1))
        expected_name = os.path.join(
                self.dossier_image,
                "{nom}.{ext}".format(nom=albumTest.nom,
                                     ext=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_tmp)),
                         0)
        # Modification du nom de l'`Album`
        nouveau_albumTest_nom = self.albumTest_nom + "-test"
        albumTest.nom = nouveau_albumTest_nom
        albumTest.save()
        # Vérification que le nom du fichier a bien changé
        # et que l'ancien fichier image n'existe plus
        self.assertFalse(os.path.isfile(expected_name))
        expected_name = os.path.join(
                self.dossier_image,
                "{nom}.{ext}".format(nom=albumTest.nom,
                                     ext=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        # Suppression de l'image de l'`Album`
        albumTest.image = ""
        albumTest.save()
        # Vérification que le fichier image a bien été supprimé
        self.assertFalse(os.path.isfile(expected_name))
        # Ajout d'une nouvelle image pour l'`Album`
        num += 1
        albumTest.image = nouveau_fichier_image(num)
        albumTest.save()
        # Vérification que le fichier image existe et que le fichier
        # initial a bien été supprimé
        self.assertTrue(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_tmp)),
                         0)

    def test_suppression_Album(self):
        """
        Vérifie que le fichier image est bien supprimé du système
        de stockage lors de la suppression d'un `Album`.
        """
        # Création d'une image noire avec les attributs désirés
        imagePIL = Image.new(self.modesTest[0],
                             self.sizesTest[0])
        image_path = os.path.join(
                            self.dossier_tmp,
                            next(tempfile._get_candidate_names()))
        # Enregistrement de l'image au format désiré
        imagePIL.save(image_path, format=self.formatsTest[0])
        # Création d'un `Album` utilisant l'image
        albumTest = Album.objects.create(
                        nom="{nom}-{suffixe}".format(nom=self.albumTest_nom,
                                                     suffixe=self.prefixeTest),
                        image=image_path)
        # Vérification que le fichier image existe
        expected_name = os.path.join(
                self.dossier_image,
                "{nom}.{ext}".format(nom=albumTest.nom,
                                     ext=settings.IMAGE_CONFIG["extension"]))
        self.assertTrue(os.path.isfile(expected_name))
        nombre_fichier_image = len(os.listdir(self.dossier_image))
        albumTest.delete()
        # Vérification que le fichier image a été supprimé
        self.assertFalse(os.path.isfile(expected_name))
        self.assertEqual(len(os.listdir(self.dossier_image)),
                         nombre_fichier_image - 1)
