# -*- coding: utf-8 -*-

"""
Définition du modèle de données pour la gestion des musiques.
"""

from django import forms
from .models import Piste


class PisteForm(forms.ModelForm):
    """
    Modèle de formulaire permettant de n'autoriser que des nombres entiers
    de 1 à 99 pour la position d'une `Piste` dans un `Album`.
    """
    position = forms.IntegerField(
        min_value=1,
        max_value=99,
        label="position",
        help_text="Position de la piste dans l'album. "
                  "Nombre entier compris entre 1 et 99")

    class Meta:
        model = Piste
        fields = ("fichier",
                  "titre",
                  "date_publication",
                  "licence",
                  "editeur",
                  "position",
                  "artiste",
                  "album",
                  "genre",
                  "provenance")
